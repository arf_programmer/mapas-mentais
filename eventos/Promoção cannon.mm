<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1390329924762" ID="ID_27948119" MODIFIED="1390331227654" TEXT="Promo&#xe7;&#xe3;o cannon">
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1390329939405" ID="ID_330800685" MODIFIED="1390331227614" POSITION="right" TEXT="regulamento">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1390329944556" ID="ID_848946139" MODIFIED="1390331227624" TEXT="inscricao">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1390329989535" ID="ID_1634281564" MODIFIED="1390331227624" TEXT="at&#xe9; dia 22/01">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1390329952039" ID="ID_1431449169" MODIFIED="1390331227634" TEXT="envio de docs">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1390330036966" ID="ID_1264748737" MODIFIED="1390331227634" TEXT="3 fotografias">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1390330090708" ID="ID_164397273" MODIFIED="1390331227634" TEXT="no max 5 mb"/>
</node>
<node COLOR="#990000" CREATED="1390330101958" ID="ID_1793771988" MODIFIED="1390331227634" TEXT="texto ">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1390330104152" ID="ID_1900644381" MODIFIED="1390331227644" TEXT="2500 carac"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1390330365236" ID="ID_1355768164" MODIFIED="1390331227644" TEXT="etapas de julgamento">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1390330371828" ID="ID_757693162" MODIFIED="1390331227644" TEXT="comissao julgadora">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1390330377099" ID="ID_168120814" MODIFIED="1390331227644" TEXT="vota&#xe7;&#xe3;o popular">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1390330417807" ID="ID_133332957" MODIFIED="1390331227654" TEXT="resultado">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1390330421057" ID="ID_699257059" MODIFIED="1390331227654" TEXT="no dia 10/02/14">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1390330896706" ID="ID_1713644055" MODIFIED="1390331227654" TEXT="premio prescreve">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1390330901328" ID="ID_615269576" MODIFIED="1390331227654" TEXT="180 dias"/>
</node>
</node>
</node>
</node>
</map>
