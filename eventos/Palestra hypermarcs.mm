<map version="0.9.0" NAME="Palestra hypermarcs" DATE="1384901202000" FILE="f42be685-bc18-49dd-be49-5bcdaaf97cbb.mm" MAPID="crh/FZ6tRZmIhUtWA0C3ew==" BGCOLOR="#F2F3F7">
<node ID="ID_5167100382" TEXT="Palestra hypermarcs" COLOR="#000034" TSGSTYLE="NodeMasterGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_6380516371" TEXT="Estrutura" POSITION="right" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2645451488" TEXT="Rn" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5481418607" TEXT="Projetos" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<arrowlink ARROW_ID="ID_8845373674" DESTINATION="ID_4445788587" STARTINCLINATION="-141;7;" ENDINCLINATION="-136;-52;" STARTARROW="None" ENDARROW="Default" />
<arrowlink ARROW_ID="ID_4580307100" DESTINATION="ID_4178811463" STARTINCLINATION="167;-173;" ENDINCLINATION="158;-102;" STARTARROW="None" ENDARROW="Default" />
<node ID="ID_8062530834" TEXT="Execu&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6282305657" TEXT="Sap" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_3558344003" TEXT="Bi" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3144437525" TEXT="Sistemas n&#227;o sap" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="help" />
</node>
<node ID="ID_3472483117" TEXT="Mobile" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_4178811463" TEXT="Servi&#231;os de usu&#225;rios" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5371652647" TEXT="Suporte a aplica&#231;&#245;es" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8286002468" TEXT="Equipe de suporte" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7867541030" TEXT="Sistema core" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1182117381" TEXT="Sap" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6672325023" TEXT="Gest&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6107167680" TEXT="Software alem&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8201710460" TEXT="Componentes" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1870842087" TEXT="Produ&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1267035440" TEXT="Vendas" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_6238473434" TEXT="&#193;rea jur&#237;dica" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8272515305" TEXT="Sistema da &#225;rea" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_6235457767" TEXT="Central de servi&#231;os" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0462253301" TEXT="Muita solicita&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6688035274" TEXT="Via web" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_4088206146" TEXT="Ativos" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1530154586" TEXT="Microinform&#225;tica" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1666326232" TEXT="Central de servi&#231;os" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6306681853" TEXT="Governan&#231;a de ti" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7342032713" TEXT="Controle" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6108105205" TEXT="Organizar os processos" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1467002282" TEXT="Suporte para as outras &#225;reas" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7504835470" TEXT="&#193;reas" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1247845744" TEXT="Escrit&#243;rio de projetos" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2541632774" TEXT="Processos de ti" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8756165864" TEXT="Riscos de ti" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1018722040" TEXT="Continuanidade de neg&#243;cio" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7247624218" TEXT="Seguran&#231;a de informa&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5205368214" TEXT="Grande investimento" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_4445788587" TEXT="Infraestrutura" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7447363768" TEXT="Tr&#234;s &#225;reas" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0744712268" TEXT="Projetos de infra" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8817633214" TEXT="Controle operacional" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4128113852" TEXT="Data center" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2045845282" TEXT="Sp" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3716041123" TEXT="Go" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2558748216" TEXT="Controla as opera&#231;&#245;es" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8752057256" TEXT="Garantindo controle" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_1574530265" TEXT="Suporte a infra" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8042015105" TEXT="Recebe projetos" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
</node>
</map>
