<map version="0.9.0" NAME="Inova&#231;&#227;o" DATE="1383947947383" FILE="Inova&#231;&#227;o.mm" MAPID="jReCS8FUQH6BZG6EB9L96Q==" BGCOLOR="#F2F3F7">
<node ID="ID_0578310543" TEXT="Inova&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeMasterGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_0378885017" TEXT="Palestrante" POSITION="right" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<richcontent TYPE="NOTE">
<pre>Meetup.com/brnewtech</pre>
</richcontent>
<node ID="ID_1224003264" TEXT="Diogo" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4686060654" TEXT="Bedy" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3113688782" TEXT="Empres&#225;rios" POSITION="left" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6423685562" TEXT="Face" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0200056050" TEXT="Inst gran" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6378666108" TEXT="Fundador" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3612417586" TEXT="Brasileiro" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4308424065" TEXT="Mike krieger" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
<node ID="ID_4234373462" TEXT="Vale do sil&#237;cio" POSITION="right" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2225421824" TEXT="Ecossistema" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_3352476266" TEXT="Bom para novas id&#233;ias" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7422877253" TEXT="Fatores" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6170821166" TEXT="Cultura" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7515532214" TEXT="Capital" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2504627826" TEXT="Pesquisa" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4780445528" TEXT="Talento" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1355013004" TEXT="Empresas" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_4014325872" TEXT="Caracter&#237;sticas da cultura" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_3036420466" TEXT="" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_4530628820" TEXT="Ecossistema do Diogo" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2733773661" TEXT="Fundos encontrados" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8748353863" TEXT="Empresas de financiamento de risco" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5723576258" TEXT="Com mais de 5 milh&#245;es" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_3326658154" TEXT="Usado" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5424052068" TEXT="Motos" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8520815662" TEXT="Terreno" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6307544055" TEXT="Milhas para troca de viagens" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
</map>
