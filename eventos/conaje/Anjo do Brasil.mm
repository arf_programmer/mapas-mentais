<map version="0.9.0" NAME="Anjo do Brasil" DATE="1383920578000" FILE="Anjo do Brasil.mm" MAPID="LnzKuEOvSX+pf0BZnIrLDA==" BGCOLOR="#F2F3F7">
<node ID="ID_1175057822" TEXT="Anjo do Brasil" COLOR="#000034" TSGSTYLE="NodeMasterGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_1614537188" TEXT="Caracteristicas" POSITION="right" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_3544787718" TEXT="Ajudar o Brasil" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5731300180" TEXT="Ligado com starup" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_3573335833" TEXT="Necessitara " COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5430524720" TEXT="Capital" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4273765220" TEXT="Fontes de capital" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8413302768" TEXT="Conhecer a necessidade" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0173862867" TEXT="Escolher melhor" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2880713007" TEXT="Para fase" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4858403368" TEXT="Tipo de neg&#243;cio" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3508864674" TEXT="Exemplos" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2463124586" TEXT="Anjo" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1781735313" TEXT="N&#227;o &#233; santo" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8077424448" TEXT="Espera retorno financeiro" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2252855413" TEXT="Investidor" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_3100114238" TEXT="Ajuda com" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0031200180" TEXT="Gest&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3740215634" TEXT="Networking" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7158880236" TEXT="Capital" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8847260746" TEXT="Escada de investimentos" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node ID="ID_6147673126" TEXT="In&#237;cio de capital" POSITION="left" COLOR="#000034" TSGSTYLE="NodeBasicGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8314681348" TEXT="Subven&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5605478321" TEXT="Ligado com bolsas" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_7230034153" TEXT="Crowdfuning" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1057417735" TEXT="O povo levanta o empreendedor" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1041757218" TEXT="Consegue levantar uma base de mercado" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1165084275" TEXT="Aceleradores" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_3846665260" TEXT="Programas de meses" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3515562114" TEXT="Ajuda a levar o produto para mercado" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8022620604" TEXT="Investe capital" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0253321245" TEXT="Recebe uma parte dos lucros" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_0555825864" TEXT="Incubadora" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6684520581" TEXT="Ligadas a universidade" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2175123165" TEXT="Acesso a conhecimento" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1138837366" TEXT="N&#227;o investe capital" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_7164652735" TEXT="Hora do anjo" POSITION="right" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2888533504" TEXT="Valores" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5855488470" TEXT="At&#233; 600 mil por projeto" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3238771512" TEXT="V&#225;rios investidor" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0435227436" TEXT="Cada um com 150 mil" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6864716620" TEXT="Ou 50 mil" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_7325774510" TEXT="Investimento total para o projeto" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4124411284" TEXT="Investimento perto" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="messagebox_warning" />
</node>
</node>
<node ID="ID_0165363134" TEXT="Perfil do neg&#243;cio" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8568488074" TEXT="Projeto novo" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5437855126" TEXT="Traz inova&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4313060644" TEXT="Inova&#231;&#227;o de modelo de neg&#243;cio" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1402605215" TEXT="Seja escal&#225;vel" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4165787305" TEXT="Ind&#250;stria &#233; escal&#225;vel" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0237083487" TEXT="Ligado a ti" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="messagebox_warning" />
</node>
</node>
<node ID="ID_3213700806" TEXT="Mercado amplo" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1812317654" TEXT="Investidor que recebe o retorno" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7246472382" TEXT="Internacionalizar" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="messagebox_warning" />
<icon BUILTIN="password" />
</node>
</node>
<node ID="ID_7866474273" TEXT="Necessidade intelectual e capital" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_3021770771" TEXT="Complemente o neg&#243;cio" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6711351084" TEXT="Empreendedor engajado" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1168348743" TEXT="Tem muitos desafios" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7235551466" TEXT="Querer fazer" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4140847441" TEXT="Motiva&#231;&#227;o grande" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_7646876041" TEXT="Setores" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7542482803" TEXT="Ti" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_7067580714" TEXT="Valor agregado" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8605664203" TEXT="Ideia de fora" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5688013662" TEXT="Implanta&#231;&#227;o de governan&#231;a" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2607102036" TEXT="Dicas" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0245617742" TEXT="Monte um time certo" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5755340021" TEXT="Diferentes &#225;reas" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6581810360" TEXT="Complemente voc&#234;" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2803072235" TEXT="Investidor &#233; s&#243;cio" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8206252537" TEXT="Pequena parcela" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4252734746" TEXT="Para motivar o empreendedor" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_1084216182" TEXT="Pesquisa o mercado" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5827558174" TEXT="Entenda as necessidades" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2322538184" TEXT="Monte um prot&#243;tipo" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0214883541" TEXT="Conhecer os contratos" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3836484716" TEXT="Preparar apresenta&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7121120074" TEXT="Apresentar pontos chaves" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8178733724" TEXT="Identificar o problema" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6327588866" TEXT="Solu&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7434167707" TEXT="Mostrar retorno" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1483142104" TEXT="Mostrar que est&#225; preparado e quer" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_8032455337" TEXT="Demora um pouco" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2138281570" TEXT="Id&#233;ias" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1546860771" TEXT="Escutar feed back" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1308575028" TEXT="Quest&#227;o da dist&#226;ncia" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5102237238" TEXT="N&#227;o &#233; obrigat&#243;rio" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5622825353" TEXT="A dist&#226;ncia" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_6068028271" TEXT="Investidor" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1450012083" TEXT="Vem de outras empresas" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4357046326" TEXT="Carreira corporativa" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5874815368" TEXT="Profissional liberal" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_8616358136" TEXT="Submiss&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4435804701" TEXT="Plataforma gush" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6854317120" TEXT="Perguntas" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7117824611" TEXT="V&#237;deo" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8818580385" TEXT="Ppt" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1872834626" TEXT="Resposta de um m&#234;s" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2538365542" TEXT="N&#227;o cobra nada" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
</map>
