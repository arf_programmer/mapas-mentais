# Repositório Público de Mapas Mentais #

A idéia surgiu pelo Alex R. Ferreira, quando concluiu sua graduação em 2014. Ele começou a utilizar vários mapas mentais para documentar aulas, eventos e planejar estudos. Ao final da graduação, observou que tinha uma rica biblioteca de mapas mentais, que podem ajudar qualquer outra pessoa a aprender ou estudar algum conteúdo.

# Manutenção do Repositório #

A manutenção é realizada pelo próprio Alex. Para ajudá-lo você pode reportar os erros de português ou erros de conteúdos pela [plataforma de Issue do repositório](www.google.com.br). Você pode agradecê-lo ou enviar feedbacks via email: arf92@live.com.

# Novos mapas Mentais #

A idéia é que o repositório seja público e se torne um biblioteca de mapas mentais, com a inserção de novos mapas mentais criados e atualizados por diferentes pessoas. Contudo, para que isto ocorra, está em fase de projeto, uma plataforma que possibilite a atualização de mapas mentais online, com registro de históricos de modificações.

# Direitos de uso #

O repositório é público, e pode ser copiado e utilizado por qualquer pessoa.