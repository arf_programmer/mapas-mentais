<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1380410446577" ID="ID_679405353" MODIFIED="1380410457689" TEXT="Gerenciadores">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1380410446581" HGAP="113" ID="ID_660904805" MODIFIED="1380410567480" POSITION="right" TEXT="Gerenciador Financeiro" VSHIFT="-78">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#00b439" CREATED="1380410446581" ID="ID_1687856443" MODIFIED="1380410457686" TEXT="todos">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="help"/>
</node>
<node COLOR="#00b439" CREATED="1380410446581" ID="ID_1339469428" MODIFIED="1380410457686" TEXT="android/pc">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#00b439" CREATED="1380410446581" ID="ID_1912809210" MODIFIED="1380410457686" TEXT="pc/net">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_cancel"/>
</node>
<node COLOR="#00b439" CREATED="1380410446581" ID="ID_408334470" MODIFIED="1380410474755" TEXT="android/net">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1380410446581" ID="ID_17163073" MODIFIED="1380410474755" TEXT="Yupee">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="xmag"/>
<icon BUILTIN="full-2"/>
<node COLOR="#111111" CREATED="1380410446581" ID="ID_1182856725" MODIFIED="1380410457686" TEXT="interface ruim">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446581" ID="ID_1199684647" MODIFIED="1380410457686" TEXT="sincroniza&#xe7;&#xe3;o boa">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446581" ID="ID_279198973" MODIFIED="1380410457686" TEXT="todas funcoes do minhas economias">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446581" ID="ID_1692755153" MODIFIED="1380410457686" TEXT="modo offiline no android">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1380410446582" ID="ID_361274903" MODIFIED="1380410474756" TEXT="Minhas Economias">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
<node COLOR="#111111" CREATED="1380410446582" ID="ID_1724823607" MODIFIED="1380410457686" TEXT="interface do android melhor">
<edge COLOR="#309eff"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#111111" CREATED="1380410446582" ID="ID_928890122" MODIFIED="1380410457687" TEXT="sincroniza&#xe7;&#xe3;o ok">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446582" ID="ID_228539200" MODIFIED="1380410457687" TEXT="offiline ok">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446582" ID="ID_1177622018" MODIFIED="1380410474757" TEXT="compatibilidade com outros softwares">
<edge COLOR="#309eff"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#111111" CREATED="1380410446582" ID="ID_841695447" MODIFIED="1380410457687" TEXT="exporta em pdf ou xls">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446582" ID="ID_1783036685" MODIFIED="1380410457687" TEXT="importa qif e outros">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1380410446582" ID="ID_384669161" MODIFIED="1380410474757" TEXT="Net">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="idea"/>
<node COLOR="#990000" CREATED="1380410446582" ID="ID_402457550" MODIFIED="1380410474758" TEXT="Organizze">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-3"/>
<node COLOR="#111111" CREATED="1380410446582" ID="ID_1098611744" MODIFIED="1380410457687" TEXT="otima interface">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446582" ID="ID_514168261" MODIFIED="1380410457687" TEXT="funcionar online no celular com versao mobile">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446582" ID="ID_181852302" MODIFIED="1380410457687" TEXT="verificar objetivos de relatorios">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446582" ID="ID_509108944" MODIFIED="1380410474759" TEXT="compatibilidade">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1380410446582" ID="ID_1392139292" MODIFIED="1380410457687" TEXT="importa somente OFX">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446582" ID="ID_210425561" MODIFIED="1380410457687" TEXT="importa agenda com XLS">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446582" ID="ID_176173489" MODIFIED="1380410457687" TEXT="exporta XLS">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1380410446582" ID="ID_795467553" MODIFIED="1380410474759" TEXT="PC">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="help"/>
<node COLOR="#990000" CREATED="1380410446582" ID="ID_303761811" MODIFIED="1380410474759" TEXT="gnuCash">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380410446582" ID="ID_1087002489" MODIFIED="1380410457687" TEXT="testar funcoes">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1380410446582" ID="ID_1889502724" MODIFIED="1380410474759" TEXT="money dance">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380410446583" ID="ID_453575243" MODIFIED="1380410457687" TEXT="testar funcoes">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1380410446583" ID="ID_97683453" MODIFIED="1380410474760" TEXT="android">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="help"/>
<node COLOR="#990000" CREATED="1380410446583" ID="ID_730350613" MODIFIED="1380410457687" TEXT="pesquisar">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1380410446577" ID="ID_74365082" MODIFIED="1380410466768" POSITION="right" TEXT="Gerenciadores de agenda">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#00b439" CREATED="1380410446577" ID="ID_1204505241" MODIFIED="1380410457680" TEXT="google agenda">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1380410446581" ID="ID_394696536" MODIFIED="1380410457685" POSITION="left" TEXT="Gerenciadores de aplicativos">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="help"/>
</node>
<node COLOR="#0033ff" CREATED="1380410446583" ID="ID_429068227" MODIFIED="1380410474761" POSITION="right" TEXT="Gerenciadores de atividades">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="help"/>
<node COLOR="#00b439" CREATED="1380410446583" ID="ID_1083296375" MODIFIED="1380410457688" TEXT="any Do">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1380410446583" ID="ID_1100341071" MODIFIED="1380410457689" TEXT="collor note">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1380410446577" ID="ID_1646468258" MODIFIED="1380410457680" POSITION="left" TEXT="Gerenciadores de documentos">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="help"/>
<node COLOR="#00b439" CREATED="1380410446577" ID="ID_1127217989" MODIFIED="1380410555135" TEXT="Mapas mentais">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1380410446577" ID="ID_1571631778" MODIFIED="1380410474720" TEXT="Mindjet">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380410446577" ID="ID_5152047863" MODIFIED="1380410457681" TEXT="Compat&#xed;vel com .mm e xmind">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446577" ID="ID_0815366054" MODIFIED="1380410457681" TEXT="Servi&#xe7;o de backup online">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446577" ID="ID_6857155434" MODIFIED="1380410457681" TEXT="&#xcd;cones compat&#xed;veis com free mind">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_5722601578" MODIFIED="1380410457681" TEXT="Interface &#xf3;tima">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_5441635527" MODIFIED="1380410457681" TEXT="V&#xe1;rias fun&#xe7;&#xf5;es">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_2638103506" MODIFIED="1380410457681" TEXT="Exporta em file, imagens e XML">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_0386166821" MODIFIED="1380410457681" TEXT="Suporta imagens somente com xmind ou mindjet">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1380410446578" ID="ID_2823583374" MODIFIED="1380410457681" TEXT="Mind memo">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1380410446578" ID="ID_1336938709" MODIFIED="1380410457681" TEXT="PDF">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380410446578" ID="ID_0586612681" MODIFIED="1380410474729" TEXT="Foxit">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_6856046206" MODIFIED="1380410457681" TEXT="Compartilha via WiFi">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_6264143735" MODIFIED="1380410457681" TEXT="Faz anota&#xe7;&#xf5;es">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_6075775807" MODIFIED="1380410457681" TEXT="Compatibilidade com dropbox">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_5142508143" MODIFIED="1380410457681" TEXT="Cria pastas">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_3672545748" MODIFIED="1380410457681" TEXT="Faz pesquisa de documentos">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_7672074125" MODIFIED="1380410457681" TEXT="Faz controle de luz">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_0050018283" MODIFIED="1380410457681" TEXT="Zoom 50%">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_5363102423" MODIFIED="1380410457681" TEXT="Visualiza&#xe7;&#xe3;o de modo texto">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_6137121450" MODIFIED="1380410457681" TEXT="Cria&#xe7;&#xe3;o de book Mark">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_3382117770" MODIFIED="1380410457681" TEXT="Usa pouca mem&#xf3;ria">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1380410446578" ID="ID_0502463517" MODIFIED="1380410457681" TEXT="Adobe">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_5888217657" MODIFIED="1380410457682" TEXT="Bloqueia o brilho da tela">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_8016041875" MODIFIED="1380410457682" TEXT="Controle de cache">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446578" ID="ID_3641727403" MODIFIED="1380410457682" TEXT="Conjunto de ferramentas">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1380410446578" ID="ID_0145142172" MODIFIED="1380410457682" TEXT="PDF utiliy">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1380410446578" ID="ID_1845309071" MODIFIED="1380410457682" TEXT="Office">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1380410446578" ID="ID_1595548692" MODIFIED="1380410457682" TEXT="openOffice">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1380410446578" ID="ID_934242344" MODIFIED="1380410457682" TEXT="googleDrive">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1380410446578" ID="ID_1356471515" MODIFIED="1380410457683" TEXT="latex">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1380410446579" ID="ID_1795831834" MODIFIED="1380410474733" TEXT="Desenho">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="info"/>
<node COLOR="#990000" CREATED="1380410446579" ID="ID_6475285355" MODIFIED="1380410474736" TEXT="Scribble pad">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_5017038713" MODIFIED="1380410457683" TEXT="Interface ruim">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_4670010281" MODIFIED="1380410457683" TEXT="Fun&#xe7;&#xf5;es b&#xe1;sicas somente">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_2867556377" MODIFIED="1380410457683" TEXT="Pincel fino e 50px">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_8173123074" MODIFIED="1380410457683" TEXT="Borracha somente 50px">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_0400176362" MODIFIED="1380410457683" TEXT="Comando Undo e redo">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1380410446579" ID="ID_7245780220" MODIFIED="1380410474737" TEXT="Home style">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_cancel"/>
<icon BUILTIN="calendar"/>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_1804028112" MODIFIED="1380410457683" TEXT="Interface s&#xf3; abre online">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1380410446579" ID="ID_6627607721" MODIFIED="1380410474740" TEXT="Desenhos para pintar">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_0626357888" MODIFIED="1380410457683" TEXT="Interface boa, mas com &#xed;cones ruins">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_6776137506" MODIFIED="1380410457683" TEXT="Pincel fino e 50px">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_0120888375" MODIFIED="1380410457683" TEXT="Pr&#xe9;-imagens desenhadas">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_4165504851" MODIFIED="1380410457683" TEXT="Cor rgb">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_3625636826" MODIFIED="1380410457683" TEXT="Fun&#xe7;&#xe3;o Undo">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1380410446579" ID="ID_3384154167" MODIFIED="1380410474742" TEXT="Drawing app">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_6252656317" MODIFIED="1380410457683" TEXT="Pincel fino e 50 px">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_1086436582" MODIFIED="1380410457683" TEXT="Cria v&#xed;deo de cria&#xe7;&#xe3;o de desenho">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_2143712457" MODIFIED="1380410457683" TEXT="Fun&#xe7;&#xe3;o Undo e rede">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_4472644180" MODIFIED="1380410457684" TEXT="Controle de pixel da borracha">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_7021536442" MODIFIED="1380410457684" TEXT="Muita propaganda">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1380410446579" ID="ID_3444182180" MODIFIED="1380410474749" TEXT="Paintter pallete">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380410446579" ID="ID_0410736036" MODIFIED="1380410457684" TEXT="Controle de tipo de pincel">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_8606864447" MODIFIED="1380410457684" TEXT="Controle de pincel completo">
<richcontent TYPE="NOTE"><pre>Transparecia, tamanho, cor, estilo da ponta, intensidade da ponta, efeito de borragem e espa&#231;o de efeito da ponta do pincel.</pre></richcontent>
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_3473013075" MODIFIED="1380410457684" TEXT="Controle Undo e redo">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_5417406880" MODIFIED="1380410457684" TEXT="Cont&#xe9;m an&#xfa;ncio na tela de desenho.">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_0704615324" MODIFIED="1380410457684" TEXT="Controle de vis&#xe3;o da p&#xe1;gina">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_7316636847" MODIFIED="1380410457684" TEXT="Importa&#xe7;&#xe3;o de imagens">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_5684030353" MODIFIED="1380410457684" TEXT="Poucos desenhos prontos para colorir">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_6140185011" MODIFIED="1380410457684" TEXT="Exporta em png">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_4522107170" MODIFIED="1380410457684" TEXT="But&#xe3;o sair esta lento">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1380410446580" ID="ID_7176608330" MODIFIED="1380410474750" TEXT="Picasso">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_7622232086" MODIFIED="1380410457684" TEXT="Muda cor do plano de fundo">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_5844675410" MODIFIED="1380410457684" TEXT="Tem diferentes pinc&#xe9;is">
<richcontent TYPE="NOTE"><pre>Spray, contorno elevado, pincel com n&#250;cleo branco.</pre></richcontent>
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_0140176724" MODIFIED="1380410474751" TEXT="Controle de tamanho">
<richcontent TYPE="NOTE"><pre>De 1px a 50px</pre></richcontent>
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_0542821662" MODIFIED="1380410457684" TEXT="Borracha">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_4387211117" MODIFIED="1380410457684" TEXT="Pincel">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_0366102048" MODIFIED="1380410457684" TEXT="Fun&#xe7;&#xe3;o Undo">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1380410446580" ID="ID_3811556784" MODIFIED="1380410474751" TEXT="How to draw">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_3816104086" MODIFIED="1380410457684" TEXT="Tamanho de pincel muito fino">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_1626175305" MODIFIED="1380410457684" TEXT="Desenhos com passo a passo para criar os desenhos">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1380410446580" ID="ID_5285551150" MODIFIED="1380410474752" TEXT="Line brush">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380410446580" ID="ID_4042258374" MODIFIED="1380410457684" TEXT="Pincel com similar ao real">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446581" ID="ID_0885554637" MODIFIED="1380410457684" TEXT="Controle de opacidade na interface principal">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446581" ID="ID_0061307375" MODIFIED="1380410457684" TEXT="Pouca fun&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380410446581" ID="ID_7128746876" MODIFIED="1380410457684" TEXT="Salva em algum tipo">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1380410446581" ID="ID_1254674087" MODIFIED="1380410457684" TEXT="Ebook">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380410446581" ID="ID_6522033686" MODIFIED="1380410457685" TEXT="Ebookdroid">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1380410446581" ID="ID_0486354272" MODIFIED="1380410457685" TEXT="Ebook reader">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1380410446583" ID="ID_1572257545" MODIFIED="1380410474761" POSITION="left" TEXT="Gerenciadores de energia">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="help"/>
<node COLOR="#00b439" CREATED="1380410446583" ID="ID_1119172219" MODIFIED="1380410457689" TEXT="bateryBooster">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1380410446583" ID="ID_1946315033" MODIFIED="1380410457689" TEXT="Juice Defender">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1380410446583" ID="ID_1022392683" MODIFIED="1380410457689" POSITION="right" TEXT="Gerenciadores de hardware">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="help"/>
</node>
<node COLOR="#0033ff" CREATED="1380410446583" ID="ID_1921978243" MODIFIED="1380410474760" POSITION="right" TEXT="Gerenciadores de notas">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="help"/>
<node COLOR="#00b439" CREATED="1380410446583" ID="ID_1217016434" MODIFIED="1380410457688" TEXT="collor note">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1380410446583" ID="ID_946165371" MODIFIED="1380410457688" TEXT="keep">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1380410446583" ID="ID_749928759" MODIFIED="1380410457688" TEXT="every notes">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1380410446581" ID="ID_1127155144" MODIFIED="1380410474752" POSITION="left" TEXT="Gerenciadores de seguran&#xe7;a">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="help"/>
<node COLOR="#00b439" CREATED="1380410446581" ID="ID_1959219179" MODIFIED="1380410457685" TEXT="avast anti thief">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="help"/>
<icon BUILTIN="full-1"/>
</node>
<node COLOR="#00b439" CREATED="1380410446581" ID="ID_1060606222" MODIFIED="1380410457685" TEXT="prey">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="help"/>
<icon BUILTIN="full-2"/>
</node>
</node>
</node>
</map>
