<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1380410713833" ID="ID_1095666521" MODIFIED="1380410822594" TEXT="Avalia&#xe7;&#xe3;o de PHP">
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1380410749275" FOLDED="true" ID="ID_925522569" MODIFIED="1380410815734" POSITION="left" TEXT="Pontos fortes">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1380410749275" ID="ID_1314755804" MODIFIED="1380410815734" TEXT="Servidor nativo de locais de hospedagem">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1380410749276" ID="ID_968233458" MODIFIED="1380410815734" TEXT="Hospedagem barata">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1380410749277" ID="ID_1051148297" MODIFIED="1380410815734" TEXT="R&#xe1;pida manuten&#xe7;&#xe3;o">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380410749277" ID="ID_500782709" MODIFIED="1380410815734" TEXT="Upload direto para o servidor">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380410749278" ID="ID_1394246025" MODIFIED="1380410777511" TEXT="Upload do arquivo .php"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1380410749270" FOLDED="true" ID="ID_870053453" MODIFIED="1380410815731" POSITION="right" TEXT="Pontos fracos">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1380410749271" ID="ID_1216769069" MODIFIED="1380410815731" TEXT="Pouca organiza&#xe7;&#xe3;o">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380410749272" ID="ID_938475390" MODIFIED="1380410815731" TEXT="Sujeito a muitos erros de programa&#xe7;&#xe3;o">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1380410749272" ID="ID_284868694" MODIFIED="1380410815731" TEXT="Conex&#xe3;o com o BD por query">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380410749273" ID="ID_1148546117" MODIFIED="1380410815731" TEXT="Falha de seguran&#xe7;a">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1380410749274" ID="ID_1524543643" MODIFIED="1380410815732" TEXT="Manuten&#xe7;&#xe3;o restrita para programador">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
</node>
</map>
