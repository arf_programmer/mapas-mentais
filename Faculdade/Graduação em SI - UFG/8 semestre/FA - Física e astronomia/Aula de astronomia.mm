<map version="0.9.0" NAME="Aula de Astronomia" DATE="1385149230792" FILE="4281a888-fcdc-4730-afa1-8d9a006f055b.mm" MAPID="aFp7+MZJTIy+gm6kcVc1gw==" BGCOLOR="#F2F3F7">
<node ID="ID_1317225091" TEXT="Aula de Astronomia" COLOR="#000000" TSGSTYLE="NodeMasterGraphic">
<font NAME="SansSerif" SIZE="20" />
<edge COLOR="#111111" />
<node ID="ID_1915398269" TEXT="Livros" POSITION="right" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#111111" />
<node ID="ID_488655887" TEXT="Astronomia e astrof&#237;sica" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#111111" />
<richcontent TYPE="NOTE">
<pre>Kepler de Souza oliveira filho
    
    
      Maria de F&#225;tima oliveira saraiva
    
    
      
    
    
      Www.if.UFRGS.br
    
    
      Departamento de astronomia</pre>
</richcontent>
</node>
</node>
<node ID="ID_7521237146" TEXT="Gerar energia" POSITION="left" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#111111" />
<node ID="ID_6745307020" TEXT="Nuclear" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#111111" />
<node ID="ID_2270876872" TEXT="Teoria de rea&#231;&#227;o at&#244;mica" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_6680261026" TEXT="Quebra por rea&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_5813681377" TEXT="Reator" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_0207133784" TEXT="Atenuadores" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1323352567" TEXT="Moderadores" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1244583278" TEXT="Neutros" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_0365753448" TEXT="Lixo" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_0247425376" TEXT="C&#233;sio 37" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_0283534773" TEXT="Emite luz azul" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_3570024074" TEXT="Metal alcalino" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_6646414870" TEXT="Caso ingerido" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_6436674738" TEXT="Perde prote&#231;&#227;o contra doen&#231;as" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_2140862084" TEXT="Diminuem a cada 30 anos 3g" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_1717624538" TEXT="Camadas de rea&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_2263834741" TEXT="Compara&#231;&#227;o" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_2708484628" TEXT="Nuclear" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_4066572606" TEXT="N&#227;o libera co2" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_6338006172" TEXT="Combust&#237;vel" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_3307617342" TEXT="Libera co2" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_3383234417" TEXT="Gasta mais" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_7354327070" TEXT="Acidentes" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_0582113170" TEXT="Caso de Chernobyl" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_3825053708" TEXT="Hist&#243;ria" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_6060548623" TEXT="Document&#225;rio" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_1101561447" TEXT="C&#233;sio em Goi&#226;nia" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<arrowlink ARROW_ID="ID_1115536386" DESTINATION="ID_0247425376" STARTINCLINATION="301;156;" ENDINCLINATION="116;-29;" STARTARROW="None" ENDARROW="Default" />
</node>
<node ID="ID_8160223622" TEXT="Casos internacionais" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_2128444668" TEXT="Combust&#237;vel" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_6456827201" TEXT="Teoria de astronomia" POSITION="left" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#111111" />
<node ID="ID_1327865641" TEXT="Astronomia grega" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#111111" />
<node ID="ID_391822632" TEXT="Modelo geoc&#234;ntrico" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_648334760" TEXT="Arist&#243;teles" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_686480566" TEXT="Di&#226;metro da terra" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_982581966" TEXT="Descobriu a dist&#226;ncia angular" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1239358079" TEXT="Alexandria" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1023360235" TEXT="Siena" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_1861807001" TEXT="Hierarquia celeste" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_857995147" TEXT="Terra" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1255245080" TEXT="planetas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_39018856" TEXT="Estrelas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1073054364" TEXT="Sol" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_286191070" TEXT="C&#233;u" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_795301996" TEXT="Est&#227;o os deuses" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_1793835442" TEXT="Teoria do movimento de elementos" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1972042547" TEXT="Elementos" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1146932952" TEXT="Ar" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_104589824" TEXT="Terra" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_982037213" TEXT="&#193;gua" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_36674713" TEXT="Fogo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1320879601" TEXT="Eter" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_469447184" TEXT="n&#227;o Sofre modifica&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_1319669551" TEXT="Exemplo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<richcontent TYPE="NOTE">
<pre>Pedra cai, pois tem &#225;gua e terra. Fuma&#231;a sobe, pois tem ar e fogo.</pre>
</richcontent>
</node>
</node>
</node>
<node ID="ID_537883170" TEXT="M&#233;todos astron&#244;micos" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_177950588" TEXT="Medir massa de um astro" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_238378151" TEXT="Dist&#226;ncia" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_360233801" TEXT="Di&#226;metro" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_607405056" TEXT="Descobrir distancia ou diametro" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1854003432" TEXT="m&#233;todo de triangula&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_181657807" TEXT="Ptolomeu" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1142189106" TEXT="Corpo n&#227;o viaja em algo vazio" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_1044469755" TEXT="Helioc&#234;ntrico" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_1297225171" TEXT="Cop&#233;rnico" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_9871287" TEXT="Unidade astron&#244;mica" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1509181725" TEXT="M&#233;todo de triangula&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1310449517" TEXT="Movimentos de rota&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_108352867" TEXT="Transla&#231;&#227;o da terra" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1375909627" TEXT="Livro" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<richcontent TYPE="NOTE">
<pre>A revolu&#231;&#227;o dos corpos celestes.
Ele inventa as &#243;rbitas</pre>
</richcontent>
</node>
<node ID="ID_5035800854" TEXT="Tirou modelos quantitativas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_6652487535" TEXT="Teoria demorou para ser aceita" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_910536879" TEXT="Kepler" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_671190440" TEXT="Lei das &#243;rbitas" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_151345842" TEXT="Velocidade da &#243;rbita" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<richcontent TYPE="NOTE">
<pre>Usada para movimento de qualquer sat&#233;lite em volta de um planeta</pre>
</richcontent>
<node ID="ID_352046750" TEXT="Aumenta mais perto do sol" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_1148285033" TEXT="Lei das &#225;reas" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_8578212821" TEXT="Descobre a massa de um planeta" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_778878229" TEXT="Descobre as dist&#226;ncias e per&#237;odos dos planetas at&#233; o sol" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_365175152" TEXT="Dist&#226;ncia entre a terra e o sol" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_33591868" TEXT="Uma unidade astron&#244;mica" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_1923075926" TEXT="V&#234;nus e o sol" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1930602885" TEXT="0,5 unidade astron&#244;mica" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_5745231268" TEXT="3 lei de Kepler" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_3680866426" TEXT="Pode dizer qual tem que ser a velocidade do sat&#233;lite geoestacion&#225;rio" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_4038807684" TEXT="F&#243;rmula no bloco de notas" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_4202185122" TEXT="Usada para projetar expedi&#231;&#227;o para marte" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_5531140472" TEXT="&#211;rbita el&#237;ptica" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_5421600806" TEXT="Quase circular" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_5778337754" TEXT="Leis universais" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_5466561742" TEXT="Vale para todo sistema" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_533420464" TEXT="Galileu" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_1493177736" TEXT="Construiu luneta" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1522959325" TEXT="Mensageiro das estrelas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<richcontent TYPE="NOTE">
<pre>Livro sobre relatos de galileu utilizando luneta</pre>
</richcontent>
</node>
<node ID="ID_1290603833" TEXT="Confirmou o modelo helioc&#234;ntrico" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1660709026" TEXT="Come&#231;ou a busca por astronomia por instrumentos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1715852172" TEXT="Descoberta de 4 maiores sat&#233;lites de J&#250;piter" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_1382389126" TEXT="Newton" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_738632684" TEXT="Leis de movimentos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_1632227746" TEXT="In&#233;rcia" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_412471859" TEXT="Constante universal gravitacional" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<richcontent TYPE="NOTE">
<pre>F=GmM/r^2</pre>
</richcontent>
</node>
</node>
<node ID="ID_866290432" TEXT="Cavendish" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1517274343" TEXT="Engenhoca" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_207171700" TEXT="Dist&#226;ncia e raio da terra" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_228304707" TEXT="Anti social" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_8843225400" TEXT="1768" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_7414436351" TEXT="Gnomo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<richcontent TYPE="NOTE">
<pre>Sombra</pre>
</richcontent>
</node>
</node>
<node ID="ID_172706641" TEXT="Calend&#225;rio" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_1610228196" TEXT="Erro de 365 dias" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_136127178" TEXT="Todo ano bissexto tem 28 dias" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1500786603" TEXT="Todo s&#233;culo m&#250;ltiplo de 400 o ano n&#227;o &#233; bissexto" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_1014336854" TEXT="Hist&#243;ria" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_413900871" TEXT="Galileu" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_945356762" TEXT="Assinou papel que dizia que iria afastar dos seus estudos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_1227994378" TEXT="Zona habit&#225;vel" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_279338192" TEXT="De v&#234;nus" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_738638568" TEXT="Cont&#233;m &#225;gua em estado l&#237;quido" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_1401253150" TEXT="Arist&#243;teles" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_382438571" TEXT="Lei" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1189188110" TEXT="Teste" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_57949220" TEXT="Hist&#243;ria" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_817036178" TEXT="Nasceu" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_930649720" TEXT="Morreu" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_777396965" TEXT="Fizeram homenagem" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1455261957" TEXT="14 de setembro de Vichy viveria" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
</node>
<node ID="ID_667277210" TEXT="Sistema solar" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#111111" />
<node ID="ID_494291613" TEXT="Modelo de exemplo" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_434403927" TEXT="Sol no if" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_644974586" TEXT="Mais longe" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_382025612" TEXT="An&#225;polis" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_887330496" TEXT="Mais perto" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1886764685" TEXT="Terra Na entrada da UFG" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_878003708" TEXT="Espa&#231;o" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_1255998711" TEXT="N&#227;o &#233; azul" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_813410339" TEXT="Escuro" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_13835246" TEXT="N&#227;o h&#225; g&#225;s" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_666392977" TEXT="N&#227;o conduz luz" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_434522054" TEXT="N&#227;o h&#225; som" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_605378940" TEXT="Astros" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_1459926164" TEXT="Urano" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_349722626" TEXT="12 sat&#233;lites" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_678985016" TEXT="Marte" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_619863417" TEXT="Scarlett" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<richcontent TYPE="NOTE">
<pre>N&#227;o &#233; certeza</pre>
</richcontent>
</node>
<node ID="ID_1054966561" TEXT="Atmosfera" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_958492222" TEXT="Mon&#243;xido de carbono" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_995823528" TEXT="Necessita de prote&#231;&#227;o para andar l&#225;" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_8373441788" TEXT="Press&#227;o atmosf&#233;rica menor" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_1046669758" TEXT="Sat&#233;lite" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1434342349" TEXT="Cont&#233;m 2 luas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_775082620" TEXT="F&#234;nix pousou" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1936333402" TEXT="P&#243;lo norte" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_1754833049" TEXT="J&#250;piter" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_203287188" TEXT="Sat&#233;lites" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_602078112" TEXT="70 sat&#233;lites" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_1901345411" TEXT="Ceres" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1034872794" TEXT="Aster&#243;ide" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1933944947" TEXT="Material" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_1255446217" TEXT="N&#237;quel e ferro" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
</node>
<node ID="ID_1660476568" TEXT="Estrelas" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_503599544" TEXT="Mais perto" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_1054532562" TEXT="Mais longe que a Lua" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_1539202811" TEXT="Sat&#233;lites" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_1731770236" TEXT="Verifica a luz da estrela" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_1700871432" TEXT="Se" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_862225699" TEXT="Luz cai" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1573023186" TEXT="Planeta passa na frente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_1405103145" TEXT="Prisma" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_663236733" TEXT="Descobre a atmosfera" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_422392628" TEXT="Gazes que cont&#233;m no planeta" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
</node>
<node ID="ID_1312492020" TEXT="Sol" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_899886919" TEXT="Di&#226;metro" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_278572775" TEXT="14km" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_530378144" TEXT="Cont&#233;m" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1628687613" TEXT="Hidrog&#234;nio" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_287796139" TEXT="Vermelho" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_295278388" TEXT="Frequ&#234;ncia" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_634053602" TEXT="Mat&#233;ria sai" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_733617281" TEXT="Tende para cor azul" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_27781924" TEXT="Mat&#233;ria sai e entra" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1793464298" TEXT="Tende para cor preta" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1174663086" TEXT="Radia&#231;&#227;o cai para metade" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_910433885" TEXT="Durante ao ano" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1581566309" TEXT="170 manchas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
</node>
</node>
<node ID="ID_7567061871" TEXT="Caracter&#237;sticas" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_6245732415" TEXT="Estrelas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_3877575342" TEXT="Temperatura superficial" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="15" />
<edge COLOR="#111111" />
<node ID="ID_5150123837" TEXT="Muda de acordo com o espectro" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
</node>
</node>
<node ID="ID_25630928" TEXT="Teoria" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1587026670" TEXT="Descobrir planetas" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_801726505" TEXT="M&#233;todo" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1663757194" TEXT="Queda de luminosidade" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<richcontent TYPE="NOTE">
<pre>Planeta passa na frente da estrela</pre>
</richcontent>
</node>
<node ID="ID_1087397037" TEXT="Oscila&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_691030871" TEXT="Compara&#231;&#227;o com o sol" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_335627209" TEXT="Atrav&#233;s da lei da gravidade" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
</node>
<node ID="ID_1043031625" TEXT="&#211;rbita" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_933870968" TEXT="1 grau= 111km" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#111111" />
</node>
<node ID="ID_521066352" TEXT="Observat&#243;rios" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#111111" />
<node ID="ID_286820887" TEXT="Observat&#243;rio europeu" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1169294611" TEXT="Www.eso.org" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_692943206" TEXT="Na China" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1071267195" TEXT="Maior telesc&#243;pio" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1303733388" TEXT="Lente de 5m de di&#226;metro" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
</node>
<node ID="ID_5837208340" TEXT="Teoria de bor" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#111111" />
<node ID="ID_0155417346" TEXT="El&#233;tron" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_5851003333" TEXT="Cont&#233;m somente um inspin" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
</node>
<node ID="ID_5831718461" TEXT="Espectro" POSITION="right" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#111111" />
<node ID="ID_7056177868" TEXT="Existem 6 frequ&#234;ncia diferentes" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#111111" />
<node ID="ID_7170564248" TEXT="Cores" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_5068135132" TEXT="Vermelho" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_8014623660" TEXT="Laranja" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1804480880" TEXT="Amarelo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_4780561241" TEXT="Verde" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1867431073" TEXT="Azul" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_5014031372" TEXT="Violeta" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_6033175166" TEXT="Cada objeto" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#111111" />
<node ID="ID_2030768565" TEXT="Reflete a luz que n&#227;o absorveu" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_5321782628" TEXT="Exemplo" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_7682106654" TEXT="Camiseta" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_3667177375" TEXT="Azul" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_5702825586" TEXT="N&#227;o absorve a cor azul" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_8024735852" TEXT="Da luz incidida sobre o objeto" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
</node>
</node>
</node>
<node ID="ID_3013263543" TEXT="Diferen&#231;a de raios" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#111111" />
<node ID="ID_3065418253" TEXT="Micro ondas" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_7042780744" TEXT="Gera onda" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_3252303456" TEXT="Agitam a &#225;gua" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1856406264" TEXT="&#193;gua tem el&#233;trons" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_8142552582" TEXT="Ex pipoca" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_6087674434" TEXT="Tem &#225;gua dentro do milho" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_2484174658" TEXT="Quando n&#227;o estoura" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1524864501" TEXT="Milho quebrado" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_4347773778" TEXT="Milho velho" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
</node>
<node ID="ID_2318668373" TEXT="Agita qualquer el&#233;tron" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_7787538208" TEXT="Onda" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_5700424586" TEXT="Tem max e min " COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_2272241634" TEXT="Max esquenta" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_1388384023" TEXT="Min fica frio" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
</node>
<node ID="ID_6532386572" TEXT="Foi maxwell" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_1055250835" TEXT="1968" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
<node ID="ID_8562614180" TEXT="Hertz" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#111111" />
<node ID="ID_2185583228" TEXT="Criou as ondas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
<node ID="ID_1565458750" TEXT="Raio gama" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_0512047646" TEXT="Quebra mol&#233;culas qu&#237;micas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_4300670161" TEXT="Mata fungos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_2283124367" TEXT="Esteriliza objetos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
<node ID="ID_6641418668" TEXT="Atrav&#233;s de radia&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#111111" />
</node>
</node>
</node>
</node>
</node>
</node>
<node ID="ID_0004405600" TEXT="Magnitudes estelares" POSITION="right" COLOR="#000000" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#111111" />
</node>
<node ID="ID_8522230771" TEXT="" POSITION="left" COLOR="#000000" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#111111" />
</node>
</node>
</map>
