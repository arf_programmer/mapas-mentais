<map version="0.9.0" NAME="Palestra do Ricardo-computa&#231;&#227;o ubiqua" DATE="1379455680653" FILE="d4fb7929-e81a-4398-b7dd-9e0d5f23a88c.mm" MAPID="420ObsZPSAaYm4sUB8R9Xg==" BGCOLOR="#F2F3F7">
<node ID="ID_0136007355" TEXT="Palestra do Ricardo-computa&#231;&#227;o ubiqua" COLOR="#000034" TSGSTYLE="NodeMasterGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_2402557802" TEXT="Defini&#231;&#245;es" POSITION="right" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1175173154" TEXT="Ubiquidade" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<richcontent TYPE="NOTE">
<pre>Estar em todos lugares</pre>
</richcontent>
</node>
<node ID="ID_5467066242" TEXT="Pervasive" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5818678808" TEXT="Atacado" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8876171212" TEXT="Grande quantidade" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4473136046" TEXT="Por pre&#231;o baixo" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3758241602" TEXT="Varejo" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2523515805" TEXT="Pequenas quantidades" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2516851405" TEXT="Entregar o que o usu&#225;rio necessita" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_4764876553" TEXT="Ub&#237;quos" POSITION="left" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_3616672462" TEXT="Exemplo" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1774205271" TEXT="Dispositivos" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6123635277" TEXT="Esta em todo lugar" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4412636240" TEXT="Mas n&#227;o esta vis&#237;vel para todos" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_8441747355" TEXT="Diferen&#231;a entre" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4212103100" TEXT="Onipresente" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4736622178" TEXT="N&#227;o &#233; utilizado por todos" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6628336416" TEXT="Devido a problemas" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_1044358467" TEXT="Exemplo de mapa" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4128468242" TEXT="Mapa de diferentes utilidades" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1607170028" TEXT="Cada mapa tem" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6854874075" TEXT="Caracterizadores" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4726438305" TEXT="Dom&#237;nio" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2353868674" TEXT="Sem&#226;ntica" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6485800110" TEXT="Prop&#243;sito" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_7148487184" TEXT="Opera&#231;&#245;es" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5353050566" TEXT="Navega&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3122535868" TEXT="Grau de detalhamento" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4376477603" TEXT="Localiza&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_3860187807" TEXT="O" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_1352237406" TEXT="Google maps" POSITION="right" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6336018453" TEXT="Atacado" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5036822230" TEXT="V&#225;rios usu&#225;rios" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5441480484" TEXT="Acessado por diferentes lugares" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3355832632" TEXT="Tudo acess&#237;vel por todos" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0153124554" TEXT="Aplica&#231;&#227;o escal&#225;vel" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_4114217781" TEXT="Base de mapas gigantescas" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7873733552" TEXT="Varejo" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5062824222" TEXT="Art project" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7345107774" TEXT="Algumas interfaces similares" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3552247176" TEXT="Elementos particulares" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4526863145" TEXT="Objetivo particular" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3236185165" TEXT="Infraestrutura &#233; a mesma" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5656884252" TEXT="Sem&#226;ntica de navega&#231;&#227;o &#233; diferente" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6311680365" TEXT="Google music " COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8450241751" TEXT="Lugares mais de perto" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3867441277" TEXT="Lugares t&#237;picos de tirar foto" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1266004304" TEXT="Valor" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3604016737" TEXT="Atenda o interesse do usu&#225;rio" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6172335044" TEXT="Evita muita informa&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_2587558277" TEXT="Trabalho dos alunos" POSITION="left" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4678561851" TEXT="Mapa para dispositivo" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6874664326" TEXT="De acordo com o lugar que o dispositivo estivesse" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8857576187" TEXT="Com servi&#231;o de descoberta" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1227560576" TEXT="Mapas usados foram criados" COLOR="#000034" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2140306600" TEXT="Espa&#231;o de profiss&#245;es" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6414437745" TEXT="UFG" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
</map>
