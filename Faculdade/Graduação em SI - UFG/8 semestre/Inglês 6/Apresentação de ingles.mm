<map version="0.9.0" NAME="Apresenta&#231;&#227;o de ingles" DATE="1380824891000" FILE="498751e7-1c05-42de-b69a-d1b5a549b7a8.mm" MAPID="oa2EVc/KSUWSIOle4GuBTg==" BGCOLOR="#F2F3F7">
<node ID="ID_1709767984" TEXT="Apresenta&#231;&#227;o de ingles" COLOR="#000000" TSGSTYLE="NodeMasterGraphic">
<font NAME="SansSerif" SIZE="20" />
<edge COLOR="#309EFF" />
<node ID="ID_1810647500" TEXT="filme" POSITION="left" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#309EFF" />
<node ID="ID_118118081" TEXT="piratas do caribe" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_871458643" TEXT="ambiente" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_347525311" TEXT="cen&#225;rio de mar" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1893441282" TEXT="lutas em terra" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_31854651" TEXT="Arquiteturas antigas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1207653506" TEXT="personagem" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1733406239" TEXT="que fazem muitas a&#231;&#245;es e aventuras" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1396738317" TEXT="tempo" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_963203644" TEXT="antigamente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1242216030" TEXT="de acordo com tipos de tecnologia" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_731191776" TEXT="producao" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_915319855" TEXT="" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1213591926" TEXT="figurino" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4012680683" TEXT="Roupas de pessoas do tempo mostrado no film" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_577889514" TEXT="genero" POSITION="left" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#309EFF" />
<node ID="ID_1602963177" TEXT="genero de film" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_484282637" TEXT="os filmes de hoollywood'" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1692373962" TEXT="n&#227;o contem somente um genero" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_106951150" TEXT="os generos existentes" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1475158921" TEXT="ex" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1673951517" TEXT="drama" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1229996572" TEXT="aventura" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1689294925" TEXT="acao" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_462985165" TEXT="terror" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_93614013" TEXT="fantasia" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_38716771" TEXT="anima&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1582380799" TEXT="caracteristicas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1422228067" TEXT="ambiente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_633993770" TEXT="personagens" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_927663605" TEXT="historia" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_586145094" TEXT="tempo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_985847240" TEXT="Diretor" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_548040987" TEXT="figurino" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_7145212711" TEXT="Oq &#233;&#10;" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5683210051" TEXT="Como &#233; utilizo" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_962125032" TEXT="minha opniao" POSITION="right" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#309EFF" />
<node ID="ID_4100761655" TEXT="Gosto do g&#234;nero" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5373536111" TEXT="Hist&#243;ria" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6076322242" TEXT="Relacionada com a hist&#243;ria da sociedade" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_8318850043" TEXT="Personagens" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6323244386" TEXT="Personagens criados" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_615627437" TEXT="outros filmes" POSITION="right" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#309EFF" />
<node ID="ID_1205362953" TEXT="Bionic Woman" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1143116262" TEXT="chuck" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1629245989" TEXT="blade" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1432809077" TEXT="sobre o genero" POSITION="right" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#309EFF" />
<node ID="ID_1513823029" TEXT="ambiente" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_291319551" TEXT="descreve o cen&#225;rio" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_471224617" TEXT="est&#225; ligado com o tempo " COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1211060429" TEXT="personagens" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_1057258611" TEXT="ligados a carcteristicas de personalidade" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1557981472" TEXT="depende do autor" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_948808310" TEXT="pode ficar melhro com um e melhor com outro" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_741233222" TEXT="tempo" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_383842843" TEXT="esta ligado com a historia" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1038899305" TEXT="tempo que est&#225; acontecendo a drama" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1519061073" TEXT="dire&#231;&#227;o" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_39639516" TEXT="os diretores podem fazer um efeito melhor " COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1570046886" TEXT="faz com que os atores fazem acoes de acordo com o efeito" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_1226094581" TEXT="figurino" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_576303753" TEXT="varia de acordo com o tempo da historia" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</map>
