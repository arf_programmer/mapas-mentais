<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1384726948534" ID="ID_0885780072" MODIFIED="1384977391944" TEXT="Aula de direito">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1384726948534" ID="53nivhdk9307gigtect4hst663" MODIFIED="1384977649812" POSITION="left" TEXT="conceitos da area">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1384726948534" ID="63qubck0la0qv8mcq7nf0v5hnq" MODIFIED="1384727247825" TEXT="Justi&#xe7;a">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948534" ID="6aqlrmpf6b4hf29bev6h13ri29" MODIFIED="1384727247825" TEXT="Est&#xe1;tua">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948534" ID="0mns9nobll1j61mlishitjlurn" MODIFIED="1384726948534" TEXT="Venda nos olhos">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948534" ID="34g5v7op89hhilr2ea61rch5ro" MODIFIED="1384726948534" TEXT="Princ&#xed;pio da igualdade">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948534" ID="4s47hiocfh9qsedpsfjut4nr5o" MODIFIED="1384726948534" TEXT="Todos s&#xe3;o iguais perante a justi&#xe7;a">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1384726948534" ID="7hjsgl5irciuivvn103tkfmu84" MODIFIED="1384727247825" TEXT="Varas dos ju&#xed;zes">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948534" ID="3c2puq4emr4h55vbev97nb31r7" MODIFIED="1384727247825" TEXT="Medido por">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948534" ID="17n51igeh69smkq62t9lt21ebi" MODIFIED="1384726948534" TEXT="Comarca">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948534" ID="3f24am55bjole31sasgfcibg7m" MODIFIED="1384727247835" TEXT="Juridi&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948534" ID="6d8s4bda5t5kbgpg73l4l9osbi" MODIFIED="1384726948534" TEXT="Infinita">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1384726948534" ID="ID_7483153751" MODIFIED="1384727247835" TEXT="Legitimidade">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948534" ID="ID_2805020724" MODIFIED="1384727247835" TEXT="N&#xed;veis">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_0521247710" MODIFIED="1384726948534" TEXT="0 a 16">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_6515402612" MODIFIED="1384726948534" TEXT="Incapaz">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_4576433716" MODIFIED="1384726948534" TEXT="Com emancipa&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_0280085773" MODIFIED="1384726948534" TEXT="Ganha capacidade">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_6116573451" MODIFIED="1384726948534" TEXT="Maior que 18">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_5302748278" MODIFIED="1384726948534" TEXT="Totalmente capaz">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_3875713118" MODIFIED="1384726948534" TEXT="Com processo de interdi&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_2505118705" MODIFIED="1384726948534" TEXT="Ele n&#xe3;o tem permiss&#xe3;o mais">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1384726948534" ID="ID_1760103132" MODIFIED="1384727247835" TEXT="Estado">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948534" ID="ID_3082775157" MODIFIED="1384977649822" TEXT="Poderes">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_8346678511" MODIFIED="1384726948534" TEXT="Judici&#xe1;rio">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_0618618815" MODIFIED="1384726948534" TEXT="Julgar casos concreto">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_7861101463" MODIFIED="1384726948534" TEXT="Atrav&#xe9;s de direitos">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_0587352103" MODIFIED="1384726948534" TEXT="Fontes de direto">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_1878205646" MODIFIED="1384726948534" TEXT="Executivo">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_3633660002" MODIFIED="1384726948534" TEXT="Administra">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_7432530220" MODIFIED="1384726948534" TEXT="Legislativo">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_1466481364" MODIFIED="1384726948534" TEXT="Faz lei">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948534" ID="ID_1052665884" MODIFIED="1384727247835" TEXT="Casas">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_0268652703" MODIFIED="1384726948534" TEXT="Para o povo">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_4833613707" MODIFIED="1384726948534" TEXT="C&#xe2;mara">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_3163361023" MODIFIED="1384726948534" TEXT="Para os senadores">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_4458473754" MODIFIED="1384726948534" TEXT="Senado">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1384726948534" ID="ID_6613717754" MODIFIED="1384727304755" TEXT="Analogia">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <pre><font size="3">&#201; a adapta&#231;&#227;o de uma situa&#231;&#227;o jur&#237;dica a outra semelhante, j&#225; objeto de cogita&#231;&#227;o ou decis&#227;o. Analogia quer dizer, semelhan&#231;a ou nivelamento.</font></pre>
  </body>
</html></richcontent>
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1384726948534" ID="13tlhcipvhft3rlefsv496c4dp" MODIFIED="1384977649826" POSITION="left" TEXT="Diferen&#xe7;a entre direito e moral">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1384726948534" ID="5ct38ov4cmv1r2pjcopeitnc9p" MODIFIED="1384727247845" TEXT="Direito tem pena escrita">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1384726948534" ID="6h5rkghog90dt423cv6sk81df8" MODIFIED="1384727247845" TEXT="Moral tem pena somente social">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948534" ID="4ebj6qr7nca8ki4aal2t1a6vt5" MODIFIED="1384727247845" TEXT="Pode ser pior que a pena do direito">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1384726948534" FOLDED="true" ID="ID_8282715755" MODIFIED="1385036066160" POSITION="right" TEXT="Direito">
<richcontent TYPE="NOTE"><pre>O direito &#233; a ci&#234;ncia social, que se preocupa com o estudo das normas que 
disciplinam a conduta do homem em sociedade, visando &#224; harmonia do conv&#237;vio e 
do bem em comum. As normas s&#227;o coercitivamente impostas pelo estado e 
garantidas por um sistema de sans&#245;es peculiares. O homem vivendo em sociedade, 
tende entrar em conflito para predom&#237;nio de seus interesses pessoais; da&#237; as 
necessidades das regras de direito, para que n&#227;o aja desordem, mas sim, 
harmonia. O direito encontra seu fundamento na pr&#243;pria natureza humana, pois &#233; 
natural, pere&#231;a que o homem tende ao bem, para o que &#233; o justo, e isto &#233; a 
finalidade do direto. O direito mant&#233;m o equil&#237;brio nas rela&#231;&#245;es humanas para 
que a sociedade se conserve e n&#227;o pere&#231;a. Finalmente, pode se afirmar, que o 
direito sempre existe onde houver sociedade, visto que n&#227;o h&#225; sociedade sem o 
direito. Os romanos, que foram os maiores jurista da antiguidade, afirmaram 
que "onde houver sociedade, a&#237; haver&#225; o direito". Conclu&#237;-que que, sociedade e 
direito, for&#231;adamente se pressup&#245;e</pre></richcontent>
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1384726948534" ID="424jcr9dsgedbe82bheq7ue1eu" MODIFIED="1384727247845" TEXT="caracteristicas">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948534" ID="22qd5g0am5qfiu6v55eo1v7egn" MODIFIED="1384727247845" TEXT="Consequ&#xea;ncia do sociedade">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1384726948534" ID="3vp5cish91h5g1sgk9alvqb6pi" MODIFIED="1384727247845" TEXT="S&#xed;mbolo">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948534" ID="3rd7iepku1cc1u1nvfs6bto2bn" MODIFIED="1384726948534" TEXT="Balan&#xe7;a">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948534" ID="61tm15c0hk42i9lue489eenkja" MODIFIED="1384726948534" TEXT="Equilibra">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1384973152454" ID="ID_2310733866" MODIFIED="1384977165080" TEXT="Mant&#xe9;m o equil&#xed;brio">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384973152454" ID="ID_7826480063" MODIFIED="1384977165080" TEXT="Das rela&#xe7;&#xf5;es humanas">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1384973152454" ID="ID_5787650735" MODIFIED="1384977649890" TEXT="Para conservar a sociedade">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384973152454" ID="ID_8430276635" MODIFIED="1384973152454" TEXT="N&#xe3;o perecer">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384973152454" ID="ID_5083768471" MODIFIED="1384973152454" TEXT="N&#xe3;o h&#xe1; sociedade sem direito">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1384973152454" ID="ID_4884601022" MODIFIED="1384977165084" TEXT="Os maiores juristas">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384973152455" ID="ID_6842310488" MODIFIED="1384977165084" TEXT="Os romanos">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1384726948534" ID="ID_1128877412" MODIFIED="1384727247845" TEXT="linha do tempo">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948534" ID="ID_6102064118" MODIFIED="1384727247845" TEXT="Nasce">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_2445711000" MODIFIED="1384726948534" TEXT="Por fato social">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_0815436147" MODIFIED="1384726948534" TEXT="Pertuba algu&#xe9;m">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948534" ID="0vk94pft1qnkeg0fg9ru3rsdbq" MODIFIED="1384726948534" TEXT="LIDE">
<richcontent TYPE="NOTE"><pre>O homem vivendo em sociedade, tende entrar em conflito para predom&#237;nio de seus interesses pessoais; da&#237; as necessidades das regras de direito, para que n&#227;o aja desordem, mas sim, harmonia.</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948534" ID="ID_2048224786" MODIFIED="1384726948534" TEXT="Conflito de interesse">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948534" ID="ID_8114836430" MODIFIED="1384727247845" TEXT="Morre">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_5860027503" MODIFIED="1384726948544" TEXT="Surge nova lei">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_6704456635" MODIFIED="1384726948544" TEXT="A constitui&#xe7;&#xe3;o revoga">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1384726948544" ID="ID_8471180147" MODIFIED="1384727247845" TEXT="Tipos">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_7413007422" MODIFIED="1384727247845" TEXT="Direito objetivo">
<richcontent TYPE="NOTE"><pre>Direito Objetivo:&#160;Objetivamente considerado, o direito &#233; um sistema de regras de conduta destinadas a obrigar o indiv&#237;duo a um comportamento eticamente coerente com a ordem ditada pela sociedade onde vive. Essa norma geral e abstrata a que o homem &#233; obrigado se chama Direito Objetivo ou Norma Agendi (norma de a&#231;&#227;o). O direito objetivo constitui o conjunto de normas que obrigam a pessoa a um comportamento consent&#226;neo(de acordo) com a ordem social. Por esse prisma, ent&#227;o, o direito objetivo quer dizer a norma de a&#231;&#227;o imposta ao homem e &#224; qual este deve submeter-se mediante coa&#231;&#227;o do estado. &#201; o que se chama Norma Agendi ou regra da a&#231;&#227;o.</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="3bv4aq0aqbss578oidtlponc3d" MODIFIED="1384726948544" TEXT="anota&#xe7;&#xe3;o minha">
<richcontent TYPE="NOTE"><pre>Direito objetivo: Objetivamente considerado, o direto &#233; um conjunto de condutas que s&#227;o destinadas ao indiv&#237;duo. Esta norma geral e abstrata a que o homem &#233; obrigado, se chama direto objetivo ou norma agendi. O direito objetivo constitui o conjunto de normas que obrigam a pessoa a um comportamento consent&#226;neo com a ordem social. Por este prisma, ent&#227;o, o objetivo quer dizer a norma de a&#231;&#227;o imposta ao homem e a qual este deve submeter-se mediante com a&#231;&#227;o do estado. &#201; o que se chama norma agendi ou regra da a&#231;&#227;o.</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="0enaa77uanp0unpajseg900ugo" MODIFIED="1384726948544" TEXT="Dever do estado">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1384973152455" ID="ID_2630851541" MODIFIED="1384977305320" TEXT="Sistemas de regras">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384973152455" ID="ID_8400177817" MODIFIED="1384977305320" TEXT="Conduta">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384973152455" ID="ID_0781430051" MODIFIED="1384977649892" TEXT="Indiv&#xed;duo dever&#xe1; seguir">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384973152455" ID="ID_0778823875" MODIFIED="1384973152455" TEXT="Comportamentos etnicamente coerentes">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384973152455" ID="ID_7464337214" MODIFIED="1384973152455" TEXT="Ordem ditada pela sociedade em que vive">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_3727845514" MODIFIED="1384727247855" TEXT="Direito subjetivo">
<richcontent TYPE="NOTE"><pre>Direito Subjetivo:&#160;&#201; o poder de a&#231;&#227;o. Diz-se ent&#227;o, que, subjetivamente, o direito &#233; a faculdade(op&#231;&#227;o) de algu&#233;m fazer ou deixar de fazer alguma coisa de acordo com a regra de a&#231;&#227;o(norma Agendi), ou seja, de acordo com a norma. A esse poder de a&#231;&#227;o, a est&#225; de faculdade de agir, denomina-se Facultas Agendi. J&#225; que o direito objetivo compreende o conjunto de normas jur&#237;dicas; o direito subjetivo seria o interesse juridicamente protegido. O direito subjetivo deriva condir&#237;eis objetivo. N&#227;o existe sem aquele. Pelo poder de a&#231;&#227;o o direito subjetivo os indiv&#237;duos fazem valer seus direitos ao sabor de seu pr&#243;prio interesse, conjugado &#224; vontade de agir, desde que para tanto estejam devidamente autorizados, portanto Direito Subjetivo quer dizer o autorizamento dado pelo Direito Objetivo. N&#227;o se pode ouvidar que o Direito Objetivo e o Direito Subjetivo s&#243; existem dento de uma sociedade. Finalmente, o Direito Objetivo s&#243; se compreende em raz&#227;o do direito subjetivo; N&#227;o h&#225; Direito Subjetivo sem Direito Objetivo ; Enfim n&#227;o h&#225; Direito Subjetivo contra Direito Objetivo; Ou seja, o Direito Objetivo comanda o Direito Subjetivo e, por conseguinte, o Direito Subjetivo Sempre est&#225; na depend&#234;ncia do Direito Objetivo.</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="3ooq8gcei6frue714ld5u5o8h4" MODIFIED="1384726948544" TEXT="Obriga&#xe7;&#xe3;o da faculta">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_1754780801" MODIFIED="1384726948544" TEXT="anotacao minha">
<richcontent TYPE="NOTE"><pre>&#201; o poder de a&#231;&#227;o. Diz-se, ent&#227;o, subjetivamente, o direito &#233; a faculdade de 
algu&#233;m fazer ou deixar de fazer alguma coisa, de acordo, com a regra de a&#231;&#227;o, 
ou seja, de acordo com a norma agendi. A este poder de a&#231;&#227;o, a esta faculdade 
de agir, denomina-se faculta agendi. J&#225; que o direito objetivo compreendi o 
conjunto de normas jur&#237;dicas; o direito subjetivo seria o interesse 
juridicamente protegido pela lei. O direito subjetivo deriva do direto 
objetivo, n&#227;o existe sem aquele.</pre></richcontent>
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384973152455" ID="ID_4712435801" MODIFIED="1384977189152" TEXT="Faculdade">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384973152455" ID="ID_8422863777" MODIFIED="1384977189153" TEXT="De fazer">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384973152456" ID="ID_6012457251" MODIFIED="1384977189153" TEXT="Ou n&#xe3;o fazer">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384973152456" ID="ID_5448637626" MODIFIED="1384977189153" TEXT="De acordo com a norma">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384973152456" ID="ID_7011574717" MODIFIED="1384977189153" TEXT="Subtendido do direito objetivo">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1384973152456" ID="ID_8551210027" MODIFIED="1384977649896" TEXT="Tipos">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384973152456" ID="ID_8160052011" MODIFIED="1384977189154" TEXT="Positivo">
<edge COLOR="#309eff"/>
<icon BUILTIN="Mail"/>
</node>
<node COLOR="#111111" CREATED="1384973152456" ID="ID_0054707208" MODIFIED="1384977189154" TEXT="Natural">
<edge COLOR="#309eff"/>
<icon BUILTIN="Mail"/>
</node>
<node COLOR="#111111" CREATED="1384973152456" ID="ID_8638704008" MODIFIED="1384977189154" TEXT="Privado">
<edge COLOR="#309eff"/>
<icon BUILTIN="Mail"/>
</node>
<node COLOR="#111111" CREATED="1384973152456" ID="ID_6525800145" MODIFIED="1384977189154" TEXT="P&#xfa;blico">
<edge COLOR="#309eff"/>
<icon BUILTIN="Mail"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_4230817042" MODIFIED="1384727247855" TEXT="Direito positivo">
<richcontent TYPE="NOTE"><pre>Direito Positivo:&#160;O Direito Positivo compreende um conjunto de regras escritas existentes num determinado pa&#237;s e numa certa &#233;poca. O Direito Positivo &#233; o Direito vigente (&#233; o tempo que a lei perdura, seu tempo de validade, vig&#234;ncia), o Direito Legislado, produzido segundo as condi&#231;&#245;es sociais de cada &#233;poca, e a t&#233;cnica legislativa adotada. O Direito Positivo &#233; o direito que existe no tempo e no espa&#231;o, na realidade na experi&#234;ncia jur&#237;dica intuitivamente percebida pelo homem e por ele existencialmente conformada. O professor Vicente Ra&#243; diz que o Direito Positivo &#233; um direito declarado, pratic&#225;vel e feito valer, materialmente, pela prote&#231;&#227;o - coer&#231;&#227;o a cargo do estado. O Direito Positivo &#233; contingente, mut&#225;vel, vari&#225;vel por que particularizado de sociedade para sociedade. (cada sociedade tem sua particularidade).
    
    
      
    
    
      Direito positivo:&#160;(nasce do direito objetivo) &#233; a norma escrita, onde o direito subjetivo encontra ref&#250;gio.</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_4648218845" MODIFIED="1384727247855" TEXT="Direito natural">
<richcontent TYPE="NOTE"><pre>Direito Natural:&#160;o Direito Natural, por seu turno, &#233; um ordenamento supremo, ideal incostante, que determina o direito positivo. Na verdade, &#233; o direito pr&#233;-existente, que converte em direito positivo ou, serve para modific&#225;-lo e tamb&#233;m para aperfei&#231;oa-lo. Era, ali&#225;s, o que dizia Lafayette "o direito natural regula o direito positivo, &#233; a norma ideal para a qual o direito positivo tende sempre melhorar e da qual tanto mais se aproxima quanto mais se aperfei&#231;oa". Considera-se o Direito Objetivo como sendo o g&#234;nero e o Direito Positivo a esp&#233;cie deste g&#234;nero.
    
    
      
    
    
      Direito natural:&#160;superior a todos os demais direitos.
    
    
      (Direito a vida, direito de ir e vir, direito de comer, direito de liberdade de express&#227;o, direito de sa&#250;de, direito de educa&#231;&#227;o... E outros). Direito elementar e fundamental do indiv&#237;duo.</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_1830103805" MODIFIED="1384726948544" TEXT="Moral">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_3830606265" MODIFIED="1384727247855" TEXT="Direito p&#xfa;blico">
<richcontent TYPE="NOTE"><pre>Direito P&#250;blico:&#160;
    
    
      O Direito P&#250;blico versa sobre o modo de ser do estado. Quando se fala em Direito P&#250;blico cuida-se dedisciplinar &#160;os interesses da coletividade. As normas de Direito P&#250;blico tratam de modo especial, de regular a atividade do estado, sua presen&#231;a, no trato com os indiv&#237;duos. Exemplo: Quando o estado, usando de sua autoridade, desapropia um bemest&#225;-se diante de um ato de direito P&#250;blico. Diga-se, ainda, que p&#250;blico ou privado tanto pode ser o direito objetivo quanto o direito subjetivo, por isso, tem-se regras de direito p&#250;blico e regras de direito privado. Da&#237; conclui-se que o Dureito P&#250;blico cuida de uma maneira geral, do modo de ser do estado sua constitui&#231;&#227;o, organiza&#231;&#227;o e funcionamento; Suas rela&#231;&#245;es com outros estados e com os indiv&#237;duos.
    
    
      
    
    
      Direito p&#250;blico:&#160;engloba o direito privado. &#201; o dever do estado. Nasce no bem viver da sociedade.
    
    
      O direito constitucional (como o estado tem que ser) e o direito administrativo ( como o estado tem que fazer) constituem o direito constitucional.
    
    
      Na briga entre os direitos privado e p&#250;blico, o direito p&#250;blico prevalece ao privado! (Direito p&#250;blico &gt; direito privado). [exceto nas seguintes situa&#231;&#245;es: direito adquirido; quest&#245;es processuais que o juiz n&#227;o tem capacidade de julgar; ato jur&#237;dico perfeito. Quando tudo fica ok, quando todas as partes s&#227;o percorridas e satisfeitas.]</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_2064045564" MODIFIED="1384726948544" TEXT="Diret&#xf3;rio constitucional">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_5250244774" MODIFIED="1384726948544" TEXT="Verifica e dizer como tem que ser">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_8700001464" MODIFIED="1384726948544" TEXT="Direito administrativo">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_5543520715" MODIFIED="1384726948544" TEXT="Rege administra&#xe7;&#xe3;o p&#xfa;blica">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_2482563717" MODIFIED="1384726948544" TEXT="Administra&#xe7;&#xe3;o p&#xfa;blica">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_7346150811" MODIFIED="1384726948544" TEXT="&#xd3;rg&#xe3;os">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_2218357220" MODIFIED="1384726948544" TEXT="Secretaria da sa&#xfa;de">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_8278662520" MODIFIED="1384726948544" TEXT="Outras secretarias">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_1176416357" MODIFIED="1384726948544" TEXT="carcteristica">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_4883184578" MODIFIED="1384726948544" TEXT="Esta sobre o direito privado">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_6401713735" MODIFIED="1384726948544" TEXT="Dever do estado">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_8840576505" MODIFIED="1384726948544" TEXT="A&#xe7;&#xf5;es do estado">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_4033511041" MODIFIED="1384726948544" TEXT="Exce&#xe7;&#xf5;es ">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_4624436056" MODIFIED="1384726948544" TEXT="Direito adquirido">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_5280846514" MODIFIED="1384726948544" TEXT="Direito adquirido anteriormente">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_6072271168" MODIFIED="1384726948544" TEXT="Coisa julgada">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_3255586742" MODIFIED="1384726948544" TEXT="Formal">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_0501305301" MODIFIED="1384726948544" TEXT="Judicial">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_1144480283" MODIFIED="1384726948544" TEXT="Quest&#xe3;o processuais do juiz">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_3816038101" MODIFIED="1384726948544" TEXT="Material">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_3051868802" MODIFIED="1384726948544" TEXT="Ato jur&#xed;dico perfeito">
<edge COLOR="#309eff"/>
<icon BUILTIN="help"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_0064240623" MODIFIED="1384727247855" TEXT="Direito privado">
<richcontent TYPE="NOTE"><pre>Direito Privado:&#160;Se o direito p&#250;blico cuida do modo de ser do estado, a evid&#234;ncia, o Direito Privado cuida dos preceitos que disciplinam as rela&#231;&#245;es jur&#237;dicas individuais; o Direito privado versa sobre o interesse dos particulares. Todavia h&#225; de se lembrar que &#224; medida que o Direito evolui, verifica-se uma evidente intervens&#227;o do direito p&#250;blico no campo do direito privado.</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_6141277215" MODIFIED="1384726948544" TEXT="Menor que o direito p&#xfa;blico">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1384726948544" ID="ID_2058026047" MODIFIED="1384977149947" TEXT="Princ&#xed;pios gerais de direito">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <pre>Deve-se entender por princ&#237;pios gerais do direito, as exig&#234;ncias do ideal de justi&#231;a a ser concretizado na aplica&#231;&#227;o do direito.</pre>
  </body>
</html></richcontent>
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1384726948544" ID="ID_2110306233" MODIFIED="1384977649898" POSITION="right" TEXT="Direito objetivo-facultas agendi">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1384726948544" ID="0fe8n5geb8l71jgaiqrg1tv0jt" MODIFIED="1384727247855" TEXT="Faculdade de agir">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1384726948544" FOLDED="true" ID="ID_5426748822" MODIFIED="1385036068680" POSITION="right" TEXT="Direito objetivo-norma agendi">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1384726948544" ID="ID_0551545318" MODIFIED="1384727247855" TEXT="Nascimento da lei">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_8671315481" MODIFIED="1384727247855" TEXT="Nasce de acordo com necessidade">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_6765843651" MODIFIED="1384726948544" TEXT="Nasce a constitui&#xe7;&#xe3;o">
<richcontent TYPE="NOTE"><pre>Em 5.10.88</pre></richcontent>
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_3383876001" MODIFIED="1384726948544" TEXT="Executor gera a pl">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1384726948544" ID="ID_0007414065" MODIFIED="1384727247855" TEXT="N&#xed;veis do c&#xf3;digo penal">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1384726948544" ID="ID_5844301166" MODIFIED="1384727247855" TEXT="Quem s&#xe3;o">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_4432603663" MODIFIED="1384727247855" TEXT="Jurisprud&#xea;ncia">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_4527240154" MODIFIED="1384727247855" TEXT="Lei">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1384726948544" ID="ID_5483720333" MODIFIED="1384727247855" TEXT="Protege o direto subjetivo">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1384726948544" ID="ID_1514133427" MODIFIED="1384977649910" POSITION="left" TEXT="Fontes de direito">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1384726948544" ID="4uqa2e8q90m2jji938vhtheqi1" MODIFIED="1384977649912" TEXT="Moral">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948544" ID="73v7fdtajiff25ablbqqq6lren" MODIFIED="1384727247855" TEXT="Regras">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="2m8gc537lf4fv7npb7m2phcfs2" MODIFIED="1384726948544" TEXT="Regra de costume">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="5t468jn77knqmb8qqp0us0fs60" MODIFIED="1384726948544" TEXT="N&#xe3;o &#xe9; obrigat&#xf3;rio">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="41cpt6g8onlolaqt8qc41kb02c" MODIFIED="1384727247855" TEXT="Regulam normas de comportamento humano">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="41dil5s78n9b8ak2aoiutncudv" MODIFIED="1384727247855" TEXT="Moral n&#xe3;o tem pena escrita">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1384726948544" ID="ID_0282583232" MODIFIED="1384727247855" TEXT="Doutrina">
<richcontent TYPE="NOTE"><pre>A doutrina como fonte do direito considera-se a opini&#227;o dos jurisdi&#231;&#245;es... Ou 
seja, dos cultores do direito, dos grandes juristas, manifestada em trabalhos 
jur&#237;dicos.

A doutrina portanto, desempenha papel relevante na aplica&#231;&#227;o do direito, desde 
da elabora&#231;&#227;o da lei at&#233; a sua interpreta&#231;&#227;o e adapta&#231;&#227;o aos casos concretos.</pre></richcontent>
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1384726948544" ID="ID_7235344840" MODIFIED="1384977236462" POSITION="right" TEXT="Lei">
<richcontent TYPE="NOTE"><pre>A lei &#233; a principal fonte de direito brasileiro, &#233; norma jur&#237;dica solenemente formulada e promulgada pelo poder competente , sobre rela&#231;&#245;es de ordem interna e de interesse geral. &#201; na verdade, uma regra e medida dos atos, pela qual somos levados a a&#231;&#227;o, ou della, impedidos. &#192; elabora&#231;&#227;o da lei, ...... , a ela inerente, uma tramita&#231;&#227;o complexa, passando por formalidades peculiares aa sua feitura, depois de cumprir todos os tr&#226;mites da organiza&#231;&#227;o, a lei deve ser promulgada, sendo a promulga&#231;&#227;o o ato pelo qual a lei se torna obrigat&#243;ria. 

Elabora&#231;&#227;o da lei: de acordo com o direito constitucional, 3 s&#227;o os poderes da rep&#250;blica</pre></richcontent>
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1384726948544" ID="ID_5324144210" MODIFIED="1384977649917" TEXT="Linha do tempo">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <pre>A iniciativa das leis, cabe a qual representante, perante a casa legislativa 
para a qual tenha o seu direito, pode o projeto de lei ser tamb&#233;m apresentado 
pelo chefe do poder executivo, bem assim, pelo supremo tribunal federal, pelos 
tribunais superiores, pelo procurador geral da rep&#250;blica e pelos cidad&#227;o, nas 
formas e ... Previstos na constitui&#231;&#227;o. O projeto de lei examinado por uma das 
casas legislativas federais, &#233; revisto pela outra. ... Para ser discutido no 
plen&#225;rio. Se o projeto n&#227;o ser aprovado, ser&#225; arquivado. Uma vez aprovado, o 
projeto vai, a assun&#231;&#227;o do poder executivo. San&#231;&#227;o &#233; o ato pelo qual, o chefe 
do poder manisfesta sua concord&#226;ncia com a lei, a san&#231;&#227;o poder&#225; ser expressa 
ou t&#225;cita. &#201; expressa quando o chefe do poder executivo assina o aut&#243;grafo da 
lei, determinando o seu&#160;&#160;.&#201; t&#225;cita quando passado o prazo legal, o chefe do 
executivo n&#227;o assinou o aut&#243;grafo, nem o p&#243;s nenhum veto. Se o executivo n&#227;o 
concordar com a lei, poder&#225; veta-lo. Promulga&#231;&#227;o &#233; o ato pelo qual, se atesta 
a exist&#234;ncia da lei, se ordena o seu cumprimento. Promulgada uma lei, deve ela 
ser publicada, a publica&#231;&#227;o serve para tornar a lei conhecida por todos. &#201; 
preceito do nosso direito que a ningu&#233;m &#233; l&#237;cito deixar de cumprir a lei 
alegando que n&#227;o a conhece. Se fosse poss&#237;vel escusar-se o indiv&#237;duo de 
cumprir a lei com a simples acusa&#231;&#227;o de ignor&#226;ncia a norma deixaria de ter 
for&#231;a e, perderia sua finalidade, o que, evidentemente, iria contrariar a 
ordem p&#250;blica.</pre>
  </body>
</html></richcontent>
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_6218446373" MODIFIED="1384727247855" TEXT="Casa inicial">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_1805527605" MODIFIED="1384726948544" TEXT="Apresenta&#xe7;&#xe3;o da pl">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_0161152354" MODIFIED="1384726948544" TEXT="Discuss&#xe3;o e vota&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_8777460704" MODIFIED="1384726948544" TEXT="Discutir constitucionalidade. ">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_8262872340" MODIFIED="1384726948544" TEXT="Se tem v&#xed;cio de constitucionais">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_3126860758" MODIFIED="1384726948544" TEXT="Se uma lei objetiva prejudicar um direito subjetivo de algu&#xe9;m">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_0745657783" MODIFIED="1384727247855" TEXT="Casa revisora">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_3408273776" MODIFIED="1384726948544" TEXT="Vota se vale veto ou n&#xe3;o">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_1205070600" MODIFIED="1384727247855" TEXT="Presidente">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_1577367001" MODIFIED="1384726948544" TEXT="Comporta">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_4833081403" MODIFIED="1384726948544" TEXT="Formas">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_0323676382" MODIFIED="1384726948544" TEXT="Passivamente">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_3816620400" MODIFIED="1384726948544" TEXT="Promulga">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_7645851378" MODIFIED="1384726948544" TEXT="Veto">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_5762470485" MODIFIED="1384726948544" TEXT="Volta para casa inicial">
<edge COLOR="#309eff"/>
<arrowlink DESTINATION="ID_0745657783" ENDARROW="Default" ENDINCLINATION="9;74;" ID="Arrow_ID_353156385" STARTARROW="None" STARTINCLINATION="0;-150;"/>
</node>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1384726948544" ID="ID_8837264801" MODIFIED="1384977238054" TEXT="Interpreta&#xe7;&#xe3;o">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <pre>A arte de interpretar as leis chama-se Hermen&#234;utica (que vem de Hermes Deus da mitologia grega).

Chama-se interpreta&#231;&#227;o a explica&#231;&#227;o do sentido de uma lei ou o esclarecimento sobre sua aplica&#231;&#227;o, e por mais clara que seja uma lei, ela precisa ser interpretada.
 
Principais formas de interpreta&#231;&#227;o da lei:
A)a interpreta&#231;&#227;o-gramatical:  consiste no exame das palavras da lei analisadas pelas regras de sintaxe,
B) interpreta&#231;&#227;o l&#243;gica: obt&#233;m-se pela compara&#231;&#227;o de uma lei com o ramo que lhe deu origem,
C) interpreta&#231;&#227;o hist&#243;rica: visa alcan&#231;ar os objetivos da lei, por meio do exame do ambiente que a determina,
D) interpreta&#231;&#227;o sistem&#225;tica: que se consegue por meio do julgamento da lei interpretada com o sistema jur&#237;dico vigente, procurando-se acomoda-l&#225; a todo ordenamento jur&#237;dico da sociedade.</pre>
  </body>
</html></richcontent>
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="messagebox_warning"/>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_0668520808" MODIFIED="1384727247865" TEXT="Hermes">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_2025657682" MODIFIED="1384726948544" TEXT="Decifrava os sinais">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_2260130030" MODIFIED="1384726948544" TEXT="Interpreta os dizeres">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948544" FOLDED="true" ID="ID_5571621356" MODIFIED="1385036077976" TEXT="Tipos">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_8061554224" MODIFIED="1384726948544" TEXT="Sistem&#xe1;tica">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_2456504746" MODIFIED="1384726948544" TEXT="Grande contexto">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_4130087181" MODIFIED="1384726948544" TEXT="Aut&#xea;ntica">
<richcontent TYPE="NOTE"><pre>Interpreta&#231;&#227;o aut&#234;ntica:&#160;que &#233; realizada pelo auto da lei, ou seja, pelo pr&#243;prio legislador. Essa interpreta&#231;&#227;o &#233; feita por uma nova lei elaborada com a finalidade de esclarecer a lei anterior.</pre></richcontent>
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_4808282750" MODIFIED="1384726948544" TEXT="Judicial">
<richcontent TYPE="NOTE"><pre>Interpreta&#231;&#227;o Judicial:&#160;que &#233; a efetuada pelos ju&#237;zes e pelos tribinais, nos julgamentos.</pre></richcontent>
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_6784343334" MODIFIED="1384726948544" TEXT="Administrativo">
<richcontent TYPE="NOTE"><pre>Interpreta&#231;&#227;o Administrativa:&#160;que &#233; a elaborada pelas autoridades administrativas por meio de circulares, portarias, instru&#231;&#245;es, respostas e consultas.</pre></richcontent>
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_0843442706" MODIFIED="1384726948544" TEXT="Doutrinal">
<richcontent TYPE="NOTE"><pre>Interpreta&#231;&#227;o Doutrinal:&#160;que &#233; o resultado do trabalho dos doutrinadores em preceres, teses, monografias e outras obras jur&#237;dicas.</pre></richcontent>
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_5623545607" MODIFIED="1384726948544" TEXT="Teses">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_5162830087" MODIFIED="1384726948544" TEXT="Monografia">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_7242174780" MODIFIED="1384726948544" TEXT="Para poder citar uma tese em um processo">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384973152458" ID="ID_6376341341" MODIFIED="1384977241077" TEXT="Gramatical">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384973152458" ID="ID_3288633141" MODIFIED="1384977241077" TEXT="Hist&#xf3;rica">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384973152458" ID="ID_2023061774" MODIFIED="1384977241077" TEXT="L&#xf3;gica">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_1881707187" MODIFIED="1385036081771" TEXT="Resultados">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_8343652485" MODIFIED="1385036088431" TEXT="Declarativa">
<richcontent TYPE="NOTE"><pre>Declarativa:&#160;Quando as palavras do legislador exprimem rigorosamente o que ele quis dizer.</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_6503833457" MODIFIED="1384726948544" TEXT="Matar &#xe9; crime">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_8512428712" MODIFIED="1384726948544" TEXT="N&#xe3;o h&#xe1; outra interpreta&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_7200471821" MODIFIED="1384726948544" TEXT="Tipos de crime">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_4756615413" MODIFIED="1384726948544" TEXT="Dolosa">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_8537732742" MODIFIED="1384726948544" TEXT="Culposos">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_1315803147" MODIFIED="1384726948544" TEXT="Neglig&#xea;ncia">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_8313136215" MODIFIED="1384726948544" TEXT="M&#xe9;dico mata por erro">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_8376465635" MODIFIED="1384726948544" TEXT="N&#xe3;o paga por morte mas pela falta de responsabilidade">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_8766444272" MODIFIED="1385036295281" TEXT="Extensiva">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <pre>Extensiva:&#160;Quando as palavras do legislador n&#227;o chegaram a ter o alcance que ele quis dar &#224; lei.</pre>
  </body>
</html>
</richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_5732035815" MODIFIED="1385036081772" TEXT="Restritiva">
<richcontent TYPE="NOTE"><pre>Restritiva:&#160;Quando as palavras do legislador foram al&#233;m do que ele quis dizer.</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_5253514083" MODIFIED="1384726948544" TEXT="Al&#xe9;m do que o legislador queria escrever">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1384726948544" ID="ID_208445826" MODIFIED="1384977649935" TEXT="caracteristicas">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_4036532323" MODIFIED="1384727247865" TEXT="Natureza">
<edge COLOR="#34b1fd"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_7403177568" MODIFIED="1384977649937" TEXT="Substantiva">
<edge COLOR="#34b1fd"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_5272353012" MODIFIED="1384726948544" TEXT="A lei em si">
<edge COLOR="#34b1fd"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_5500767453" MODIFIED="1384726948544" TEXT="Adjetiva">
<edge COLOR="#34b1fd"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_0178373363" MODIFIED="1384726948544" TEXT="Formal">
<edge COLOR="#34b1fd"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_4624685753" MODIFIED="1384727247865" TEXT="Origem">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_2822171105" MODIFIED="1384726948544" TEXT="Federal">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_4234401317" MODIFIED="1384726948544" TEXT="Vem do congresso">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_5106746821" MODIFIED="1384726948544" TEXT="Estadual">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_5135511162" MODIFIED="1384726948544" TEXT="Assembl&#xe9;ias">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_4523028688" MODIFIED="1384726948544" TEXT="Municipal">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_4631800642" MODIFIED="1384726948544" TEXT="Vereadores">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_4500476613" MODIFIED="1384727247865" TEXT="Destino">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_1384086002" MODIFIED="1384726948544" TEXT="Geral">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_3686344583" MODIFIED="1384726948544" TEXT="Especial">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_3623426527" MODIFIED="1384726948544" TEXT="Espec&#xed;fico com uma &#xe1;rea">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_2386234753" MODIFIED="1384726948544" TEXT="Leis comerciais">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_3816148120" MODIFIED="1384726948544" TEXT="N&#xe3;o ser&#xe1; utilizada para professores">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_1240780134" MODIFIED="1384726948544" TEXT="Particular">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_7647531650" MODIFIED="1384726948544" TEXT="Pens&#xe3;o que &#xe9; dada para uma pessoa">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_8873174821" MODIFIED="1384726948544" TEXT="Especial para uma pessoa">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948544" ID="ID_8033463186" MODIFIED="1384726948544" TEXT="Ou grupo">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_5822837220" MODIFIED="1384727247865" TEXT="Efeitos">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_1473306834" MODIFIED="1384726948554" TEXT="Imperativo">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_7801568242" MODIFIED="1384726948554" TEXT="Todo mundo tem direito a lei">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_7670243105" MODIFIED="1384726948554" TEXT="Proibitiva">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_7166505320" MODIFIED="1384726948554" TEXT="Ningu&#xe9;m pode ser preso sem o juiz">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_1150362624" MODIFIED="1384726948554" TEXT="Pro&#xed;be algo">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_8418670121" MODIFIED="1384726948554" TEXT="Facultativo">
<richcontent TYPE="NOTE"><pre>S&#227;o aquelas que podem ser devoradas pela livre disposi&#231;&#227;o das partes. Por exemplo, aquela que diz que o pagamento deve ser salvo em contr&#225;rio no domic&#237;lio do devedor</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_7122181860" MODIFIED="1384726948554" TEXT="Contrato">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_1317720456" MODIFIED="1384726948554" TEXT="Deixar de obrigar">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_4055423756" MODIFIED="1384726948554" TEXT="Punitiva">
<richcontent TYPE="NOTE"><pre>S&#227;o as normas que estabelecem uma pena</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_3002261785" MODIFIED="1384726948554" TEXT="Puni&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_1153370767" MODIFIED="1384726948554" TEXT="San&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_1068324670" MODIFIED="1384726948554" TEXT="Pena">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948554" ID="ID_7712501073" MODIFIED="1384727247865" TEXT="N&#xed;veis de leis">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_1376232441" MODIFIED="1384726948554" TEXT="Estado juiz">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_2371847647" MODIFIED="1384726948554" TEXT="Minist&#xe9;rio p&#xfa;blico">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_8605565381" MODIFIED="1384726948554" TEXT="N&#xed;veis">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_7417305133" MODIFIED="1384726948554" TEXT="Constitucional">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_5484202587" MODIFIED="1384726948554" TEXT="Inconstitucional">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_2880472226" MODIFIED="1384726948554" TEXT="Vigora at&#xe9; algu&#xe9;m questionar de acordo com a constitucional">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948554" ID="ID_0886567338" MODIFIED="1384727247865" TEXT="Hierarquia">
<richcontent TYPE="NOTE"><pre>As leis guardam, entre si, uma gradua&#231;&#227;o hier&#225;rquica. A mais importante das leis &#233; a constitui&#231;&#227;o, que n&#227;o pode ser contrariada por nenhuma outra norma. 
No que tange aos tr&#234;s &#226;mbitos em que se divide a na&#231;&#227;o, a lei federal prevalece sobre a lei estadual e ambas prevalecem sobre a municipal, guardadas as respectivas esferas de compet&#234;ncia. 
Efic&#225;cia-aptid&#227;o para produzir efeitos (quer dizer seu vigor e abrang&#234;ncia).</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_5784715277" MODIFIED="1384726948554" TEXT="Tipos de Hierarquia">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_2226352430" MODIFIED="1384726948554" TEXT="Complementar">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_1820670543" MODIFIED="1384726948554" TEXT="Ex">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_6753638886" MODIFIED="1384726948554" TEXT="C&#xf3;digo tribut&#xe1;rio">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_2058571406" MODIFIED="1384726948554" TEXT="Espec&#xed;fica">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_1441077584" MODIFIED="1384726948554" TEXT="Ordin&#xe1;ria">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_5035275727" MODIFIED="1384726948554" TEXT="Comuns">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_4467218583" MODIFIED="1384726948554" TEXT="Elementar">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_2001883176" MODIFIED="1384726948554" TEXT="Uma sobre p&#xf5;e a outra">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1384726948554" ID="ID_6644764255" MODIFIED="1384727247865" TEXT="Efic&#xe1;cia">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_183008507" MODIFIED="1384726948554" TEXT="ap&#xf3;s ter sido determinada">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1384726948554" ID="ID_1178791296" MODIFIED="1384726948554" TEXT="vale para todos, seguindo exece&#xe7;&#xf5;es">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1384726948544" ID="ID_7366762344" MODIFIED="1384977335548" POSITION="right" TEXT="Fontes de lei">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1384973152457" ID="ID_7674705458" MODIFIED="1384977339154" TEXT="direta">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384973152457" ID="ID_0235065231" MODIFIED="1384977339154" TEXT="Leis">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="Mail"/>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_5286223876" MODIFIED="1384977361791" TEXT="Costume">
<richcontent TYPE="NOTE"><pre>O costume tamb&#233;m constitui fonte do nosso direito. Nestas condi&#231;&#245;es, algumas 
normas h&#225; em nossa sociedade, que, embora n&#227;o escritas s&#227;o obrigat&#243;rias, tais 
normas s&#227;o ditadas pelos usos e costumes e n&#227;o podem deixar de serem 
compridas, ex uma fila. Para que um costume seja reconhecido como tal, &#233; 
preciso: 
A) que seja cont&#237;nuo;
B) que seja constante;
C) que seja moral;
D) que seja obrigat&#243;rio;</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1384973152457" ID="ID_8121041306" MODIFIED="1384977339158" TEXT="Indiretas">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_5460385014" MODIFIED="1384977363439" TEXT="Doutrina">
<richcontent TYPE="NOTE"><pre>Desempenha papel relevante na aplica&#231;&#227;o do direito, desde &#224; elabora&#231;&#227;o da lei at&#233; a sua interpreta&#231;&#227;o e adapta&#231;&#227;o apps casos concreto.</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_5128801138" MODIFIED="1384977364815" TEXT="Jurisprud&#xea;ncia">
<richcontent TYPE="NOTE"><pre>Entende-se por jurisprud&#234;ncia o conjunto de decis&#245;es dos tribunais 
manifestadas no mesmo sentido. A jurisprud&#234;ncia &#233; outra fonte indireta do 
direito e, de uma import&#226;ncia capital. &#201; ineg&#225;vel que sua contribui&#231;&#227;o para a 
pr&#243;pria modifica&#231;&#227;o de sistemas jur&#237;dicos e de aplica&#231;&#227;o da lei &#233; muito grande 
e bastante significativa.</pre></richcontent>
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1384973152457" ID="ID_8012612286" MODIFIED="1384977339161" TEXT="Explicita&#xe7;&#xe3;o ou integra&#xe7;&#xe3;o">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384973152457" ID="ID_3006187071" MODIFIED="1384977339161" TEXT="Analogia">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="Mail"/>
</node>
<node COLOR="#990000" CREATED="1384973152457" ID="ID_4068584167" MODIFIED="1384977339161" TEXT="Princ&#xed;pios gerais de direito">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="Mail"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1384726948544" ID="ID_1019238298" MODIFIED="1384977374411" POSITION="right" TEXT="exemplo de lei">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1384726948544" ID="ID_3082875562" MODIFIED="1384727247865" TEXT="Uso capi&#xe3;o">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948544" ID="ID_3831757717" MODIFIED="1384727247865" TEXT="Ganho de propriedade por mau zelo do alheio">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1384726948534" ID="ID_7424241256" MODIFIED="1384977107806" POSITION="left" TEXT="Constitui&#xe7;&#xe3;o">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1384726948534" ID="ID_1182333423" MODIFIED="1384727247865" TEXT="Stf">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384726948534" ID="ID_3464251266" MODIFIED="1384727247865" TEXT="Ministro que cuidam">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1384726948534" ID="ID_1617487762" MODIFIED="1384727247865" TEXT="Carta magna">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1384726948534" ID="ID_5446123712" MODIFIED="1384727247865" TEXT="Poder constituinte">
<richcontent TYPE="NOTE"><pre>O poder constituinte pode ser aplicado ao estabelecimento ou a revis&#227;o do texto constitucional, em sentido formal. Pode ser origin&#225;rio quando se aplica a &#224; elabora&#231;&#227;o da constitui&#231;&#227;o, fato que s&#243; ocorre por ocasi&#227;o do surgimento de um novo estado ou depois de uma revolu&#231;&#227;o. Ele pife ainda ser derivado quando dever&#225; ser exercido nas condi&#231;&#245;es previstas pela pr&#243;pria constitui&#231;&#227;o. A atual constitui&#231;&#227;o federativa do Brasil, foi promulgada pela assembleia nacional constituinte, em Bras&#237;lia no dia 05 de outubro de 88. Tem como princ&#237;pios fundamentais a uni&#227;o indissol&#250;vel dos estados e munic&#237;pios do distrito federal, constituindo-se a rep&#250;blica em estado democr&#225;tico fundado na soberania, na cidadania, na dignidade, nos valores sociais e da livre iniciativa, e no pluralismo pol&#237;tico, estabelece que todo poder emana do povo, que o exerce diretamente ou por meio de representantes eleitos. Quanto a sua elabora&#231;&#227;o, as constitui&#231;&#245;es podem ser hist&#243;ricas, dogm&#225;ticas e ortogadas. As hist&#243;ricas s&#227;o aquelas que se baseiam no direitos dos cidad&#227;os, nos usos e costumes, nas tradi&#231;&#245;es hist&#243;ricas; s&#227;o dogm&#225;ticas as constitui&#231;&#245;es ortogadas pelas assembl&#233;ias constituintes, formadas pelo povo; s&#227;o ortogadas porque constituem que o governo faz ao povo, independentemente da manifesta&#231;&#227;o direta destes por seus representantes. A nossa constitui&#231;&#227;o escrita por que resulta nos textos de leis coordenados num sistema;&#233; r&#237;gida porque caracteriza-se pela preval&#234;ncia aos da lei ordin&#225;ria e &#233; formal.</pre></richcontent>
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1384977112573" ID="ID_1859100757" MODIFIED="1384977118540" TEXT="caracteristicas">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1384973152453" ID="ID_6305180141" MODIFIED="1384977292994" POSITION="left" TEXT="Ementa">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1384973152454" ID="ID_1181373540" MODIFIED="1384977292994" TEXT="Bibliografia">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1384973152454" ID="ID_7086551615" MODIFIED="1384977292994" TEXT="Nunes">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1384973152454" ID="ID_4761020063" MODIFIED="1384973152454" TEXT="Antonio Rizzato">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1384973152454" ID="ID_1135434351" MODIFIED="1384973152454" TEXT="Manual de introdu&#xe7;&#xe3;o ao estudo do direito">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
</node>
</map>
