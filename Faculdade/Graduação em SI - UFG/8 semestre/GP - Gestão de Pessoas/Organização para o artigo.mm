<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1380394681645" ID="ID_8580873548" MODIFIED="1380417004206" TEXT="Organiza&#xe7;&#xe3;o para o artigo">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1380394681645" FOLDED="true" HGAP="21" ID="ID_4358888124" MODIFIED="1387073777586" POSITION="right" TEXT="Modelo de sess&#xf5;es" VSHIFT="21">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1380394681645" ID="ID_6480655420" MODIFIED="1380417004195" TEXT="Resumo">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1380394681645" ID="ID_1275284447" MODIFIED="1380417004195" TEXT="Introdu&#xe7;&#xe3;o">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380394681645" ID="ID_6003415111" MODIFIED="1380417004195" TEXT="Fundamenta&#xe7;&#xe3;o te&#xf3;rica">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1380394681645" ID="ID_1283803160" MODIFIED="1380417004196" TEXT="Apresenta&#xe7;&#xe3;o do problema">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1380394681645" ID="ID_3532650565" MODIFIED="1380417004196" TEXT="Experimentos">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380394681645" ID="ID_3838282336" MODIFIED="1380417004196" TEXT="M&#xe9;todos utilizados">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1380394681645" ID="ID_0584227347" MODIFIED="1380417004196" TEXT="Resultados">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1380394681645" ID="ID_6510484023" MODIFIED="1380417004196" TEXT="Conclus&#xe3;o">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380394681645" ID="ID_1513417248" MODIFIED="1380417004196" TEXT="Coment&#xe1;rios sobre os resultados">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1380394681645" ID="ID_6744206142" MODIFIED="1380417004196" TEXT="Apresenta&#xe7;&#xe3;o de trabalhos futuros">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1380394681645" ID="ID_0238636834" MODIFIED="1380417004196" TEXT="Refer&#xea;ncias">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1380394681645" FOLDED="true" ID="ID_2784047514" MODIFIED="1386986475922" POSITION="left" TEXT="Integrantes">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1380394681645" ID="ID_6365680884" MODIFIED="1380417004198" TEXT="Alex">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1380394681645" ID="ID_1435838427" MODIFIED="1380417004199" TEXT="Carlo">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1380394681645" ID="ID_6183381332" MODIFIED="1380417004199" TEXT="Iago">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
<node COLOR="#0033ff" CREATED="1380394681645" FOLDED="true" HGAP="21" ID="ID_1115603200" MODIFIED="1387073775858" POSITION="right" TEXT="terefas" VSHIFT="-49">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1380396149012" ID="ID_181975353" MODIFIED="1380417004199" TEXT="pesquisa">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380394681645" ID="ID_8628356005" MODIFIED="1380417004199" TEXT="Conceitos">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380396009465" ID="ID_635236020" MODIFIED="1380417004199" TEXT="blog"/>
<node COLOR="#111111" CREATED="1380396015182" ID="ID_1685272469" MODIFIED="1380417004200" TEXT="SO">
<node COLOR="#111111" CREATED="1380396030377" ID="ID_417475334" MODIFIED="1380417004200" TEXT="pesquisa operacional"/>
</node>
<node COLOR="#111111" CREATED="1380396038294" ID="ID_44097472" MODIFIED="1380417004200" TEXT="voluntario"/>
<node COLOR="#111111" CREATED="1380396044952" ID="ID_553112422" MODIFIED="1380417004200" TEXT="guest post"/>
<node COLOR="#111111" CREATED="1380396052046" ID="ID_339191199" MODIFIED="1380417004200" TEXT="empresa virou casamento"/>
<node COLOR="#111111" CREATED="1380396169255" ID="ID_1569903174" MODIFIED="1380417004200" TEXT="marketing"/>
</node>
<node COLOR="#990000" CREATED="1380396198034" ID="ID_1723283222" MODIFIED="1380417004200" TEXT="m&#xe9;todos">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380396210660" ID="ID_119717912" MODIFIED="1380417004200" TEXT="pesquisa para RH"/>
<node COLOR="#111111" CREATED="1380396201635" ID="ID_1976775181" MODIFIED="1380417004200" TEXT="recrutamento"/>
<node COLOR="#111111" CREATED="1380396207974" ID="ID_1174361641" MODIFIED="1380417004200" TEXT="sele&#xe7;&#xe3;o"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1380396153352" ID="ID_1410085554" MODIFIED="1380417004200" TEXT="escrita">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380394681645" ID="ID_4363860455" MODIFIED="1380417004200" TEXT="Contextualiza&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_8734185633" MODIFIED="1380417004200" TEXT="Apresenta&#xe7;&#xe3;o do ambiente">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_8277556347" MODIFIED="1380417004200" TEXT="Apresenta&#xe7;&#xe3;o da empresa">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_6387160118" MODIFIED="1380417004200" TEXT="Rotina das blogueiras">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_5677306274" MODIFIED="1380417004200" TEXT="Marketing">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_0201802238" MODIFIED="1380417004200" TEXT="Como a empresa atrai clientes">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_8063155013" MODIFIED="1380417004201" TEXT="Trabalho volunt&#xe1;rio">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1380394531594" FOLDED="true" ID="ID_1398274716" MODIFIED="1387073773937" POSITION="left" TEXT="Organiza&#xe7;&#xe3;o do tema">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1380394531594" ID="ID_23755597" MODIFIED="1380417004203" TEXT="Problema">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380394681646" ID="ID_1611342005" MODIFIED="1380417004203" TEXT="Apresenta&#xe7;&#xe3;o do problema">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1380394531594" ID="ID_5284761" MODIFIED="1380417004203" TEXT="Formar perfis de blogueiras para produzir conte&#xfa;dos para a empresa">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1380394681646" ID="ID_0246141613" MODIFIED="1380417004203" TEXT="Aquisi&#xe7;&#xe3;o de novos trabalhadores">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1380394681646" ID="ID_1366712710" MODIFIED="1380417004203" TEXT="Conseguir pessoas para trabalho volunt&#xe1;rio">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1380394531594" ID="ID_1676722693" MODIFIED="1380417004203" TEXT="Fundamenta&#xe7;&#xe3;o te&#xf3;rica">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380394531594" ID="ID_1643293613" MODIFIED="1380417004203" TEXT="Informa&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1380394531595" ID="ID_144737300" MODIFIED="1380417004203" TEXT="Blogueiros">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1380394531595" ID="ID_633295470" MODIFIED="1380417004203" TEXT="So">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1380394531595" ID="ID_4820225321" MODIFIED="1380417004203" TEXT="Conceitos de volunt&#xe1;rios">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1380394531595" ID="ID_8524468554" MODIFIED="1380417004203" TEXT="Guest post">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1380394531595" ID="ID_1065786876" MODIFIED="1380417004204" TEXT="Experimentos">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380394673656" ID="ID_6502704825" MODIFIED="1386986404424" TEXT="Propor um processo cont&#xed;nuo de forma&#xe7;&#xe3;o do grupo de blogueiras ">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380394673656" ID="ID_2156452014" MODIFIED="1380417004204" TEXT="Visita a ueg para pesquisar poss&#xed;veis volunt&#xe1;rios">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1380394681646" ID="ID_3664771158" MODIFIED="1380417004204" TEXT="Simula&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_2785258017" MODIFIED="1380417004204" TEXT="Pesquisa com volunt&#xe1;rios">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1380394681646" ID="ID_2266341216" MODIFIED="1380417004204" TEXT="M&#xe9;todos para experimento">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_1458811381" MODIFIED="1386986425654" TEXT="Recrutar">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_5254144121" MODIFIED="1380417004204" TEXT="Definir &#xe1;rea de pesquisa">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_7286037541" MODIFIED="1380417004204" TEXT="Definir conjunto de caracter&#xed;sticas comuns das pessoas">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_5342447233" MODIFIED="1380417051775" TEXT="Sele&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_3523754278" MODIFIED="1380417053150" TEXT="Definir caracter&#xed;sticas">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_0855645746" MODIFIED="1380417004204" TEXT="Ruins">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_6072183676" MODIFIED="1380417004204" TEXT="Boas">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_1311074088" MODIFIED="1386986443965" TEXT="Rela&#xe7;&#xe3;o de valores">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_6662834770" MODIFIED="1380417004204" TEXT="Para empresa">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_2075088076" MODIFIED="1380417004204" TEXT="Marketing">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_2046604671" MODIFIED="1380417004204" TEXT="Para volunt&#xe1;rio">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_1351803761" MODIFIED="1380417004205" TEXT="Status">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_4527811473" MODIFIED="1380417004205" TEXT="Satisfa&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_3861871470" MODIFIED="1386986445490" TEXT="Feedback">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_5504058770" MODIFIED="1380417004205" TEXT="Pesquisa de satisfa&#xe7;&#xe3;o dos volunt&#xe1;rios">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380394681646" ID="ID_4057117670" MODIFIED="1380417004205" TEXT="Verificar ganhos para empresa">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1380394531595" ID="ID_1481338086" MODIFIED="1380417004205" TEXT="Refer&#xea;ncias">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380394531596" ID="ID_1413904837" MODIFIED="1380417004205" TEXT="Virou casamento">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380394531596" ID="ID_3321642770" MODIFIED="1380396434138" TEXT="escrever">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1380396435808" ID="ID_1837391181" MODIFIED="1380417004205" TEXT="criar"/>
<node COLOR="#111111" CREATED="1380396443417" ID="ID_1537738489" MODIFIED="1380417004205" TEXT="verificar artigo da ex do Carlo"/>
</node>
<node COLOR="#990000" CREATED="1380394531596" ID="ID_135996694" MODIFIED="1380417004205" TEXT="conceitos">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1380394531596" ID="ID_1459881252" MODIFIED="1380396467362" TEXT="pesquisar artigos">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1380394678284" FOLDED="true" ID="ID_3046608674" MODIFIED="1386941441885" POSITION="left" TEXT="Formato">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1380394678284" ID="ID_2162212126" MODIFIED="1380417004206" TEXT="Word e rtf">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1380395919888" ID="ID_673984640" MODIFIED="1380417004206" TEXT="latex">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1380395924811" ID="ID_351151600" MODIFIED="1380417004206" TEXT="se revista ou conferencia aceitar">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
</node>
</map>
