<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000034" CREATED="1380394686501" ID="ID_3831266433" MODIFIED="1380394709635" TEXT="Tarefa sobre sele&#xe7;&#xe3;o de hp">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#000034" CREATED="1380394686501" FOLDED="true" ID="ID_8126857015" MODIFIED="1380394719117" POSITION="right" TEXT="Sele&#xe7;&#xe3;o com equipe">
<edge COLOR="#309eff"/>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_2568758718" MODIFIED="1380394686501" TEXT="Time line">
<edge COLOR="#309eff"/>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_2103535146" MODIFIED="1380394686501" TEXT="Pr&#xe9; sele&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_2327224280" MODIFIED="1380394686501" TEXT="Prolongadas entrevistas">
<edge COLOR="#309eff"/>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_8645722404" MODIFIED="1380394686501" TEXT="Com gerentes">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_5768134361" MODIFIED="1380394686501" TEXT="1 ou 2 horas">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_1621308008" MODIFIED="1380394686501" TEXT="Entrevistador">
<edge COLOR="#309eff"/>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_5528354228" MODIFIED="1380394686501" TEXT="Busca emocional">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_0520453444" MODIFIED="1380394686501" TEXT="N&#xe3;o liga para conhecimento t&#xe9;cnico">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_1055706730" MODIFIED="1380394686501" TEXT="Busca capacidade de trabalho em grupo">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_0435728131" MODIFIED="1380394686501" TEXT="Caracter&#xed;sticas">
<edge COLOR="#309eff"/>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_8368313577" MODIFIED="1380394702895" TEXT="Motivo">
<edge COLOR="#309eff"/>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_1222702360" MODIFIED="1380394686501" TEXT="1">
<edge COLOR="#309eff"/>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_1822650443" MODIFIED="1380394686501" TEXT="Mostra interesse da empresa">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_1251782104" MODIFIED="1380394686501" TEXT="Explora a capacidade de equipe">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_2758233324" MODIFIED="1380394686501" TEXT="2">
<edge COLOR="#309eff"/>
<node COLOR="#000034" CREATED="1380394686501" ID="ID_1415608406" MODIFIED="1380394686501" TEXT="Gerentes mostram import&#xe2;ncia de selecionar">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#000034" CREATED="1380394686502" ID="ID_3104642345" MODIFIED="1380394686502" TEXT="Prepara o entrevistado para ter sucesso">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#000034" CREATED="1380394686502" ID="ID_2308657235" MODIFIED="1380394686502" TEXT="Psic&#xf3;logos ensinam como desenvolver agilidades">
<edge COLOR="#309eff"/>
<node COLOR="#000034" CREATED="1380394686502" ID="ID_2680273674" MODIFIED="1380394686502" TEXT="Para entrevistar">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
</node>
</map>
