<map version="0.9.0" NAME="Organiza&#231;&#227;o para artigo" DATE="1380069657000" FILE="a900b539-93e3-4c04-a03c-b655926340bc.mm" MAPID="LLHcFe8hRWCuOCOi7bn7eQ==" BGCOLOR="#F2F3F7">
<node ID="ID_5661823263" TEXT="Organiza&#231;&#227;o para artigo" COLOR="#000034" TSGSTYLE="NodeMasterGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_1610513708" TEXT="Tema" POSITION="right" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2834027534" TEXT="Cloudcomputing" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6100582413" TEXT="T&#237;tulo" POSITION="left" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2228763046" TEXT="Escalonamento vertical e horizontal" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2410127381" TEXT="Resumo" POSITION="right" COLOR="#000034" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2748682573" TEXT="Parte" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0350328333" TEXT="Contextualiza&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8884843518" TEXT="Apresenta&#231;&#227;o do problema" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7112832630" TEXT="Apresenta&#231;&#227;o das t&#233;cnicas" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8058672348" TEXT="Trabalhos futuros" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_0156561430" TEXT="Assunto" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1655675878" TEXT="Contextualiza&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0511362762" TEXT="Servi&#231;os de cloud" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5535611842" TEXT="Aplica&#231;&#245;es existentes" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0721735056" TEXT="Utilizam cloud" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_8233605540" TEXT="Gerenciamento da cloud" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0034324347" TEXT="Consolida&#231;&#227;o de servidores" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2486481415" TEXT="Apresenta&#231;&#227;o do problema" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5267384508" TEXT="Consolida&#231;&#227;o dos servidores" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4360180058" TEXT="Gerenciamento de recursos" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_0076223455" TEXT="Apresenta&#231;&#227;o das t&#233;cnicas" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_3136648536" TEXT="Escalonamento" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_0378461487" TEXT="Trabalhos futuros" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2341573344" TEXT="Plataforma" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0058536040" TEXT="Gerenciamento de cloud" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3027211381" TEXT="Escalonamento de acordo com estat&#237;sticas de uso" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
</node>
</map>
