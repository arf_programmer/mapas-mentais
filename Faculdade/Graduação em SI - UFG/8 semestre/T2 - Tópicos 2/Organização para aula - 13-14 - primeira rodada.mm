<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1379010059081" ID="ID_4066783411" MODIFIED="1379094388887" TEXT="Resumo de IA">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1379010059081" ID="ID_6531456103" MODIFIED="1379094309599" POSITION="right" TEXT="Livros">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1379010059081" ID="ID_3882546403" MODIFIED="1379094309600" TEXT="Wooldridge">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1379010059081" FOLDED="true" ID="ID_2154585124" MODIFIED="1379094309600" TEXT="Dedu&#xe7;&#xe3;o l&#xf3;gica">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
<node COLOR="#111111" CREATED="1379010059081" ID="ID_1440228547" MODIFIED="1379010059081" TEXT="Por racioc&#xed;nio">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#990000" CREATED="1379010059082" ID="ID_3407645707" MODIFIED="1379094309601" TEXT="Teorema de previs&#xe3;o">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="full-1"/>
</node>
<node COLOR="#990000" CREATED="1379010059082" ID="ID_7156825007" MODIFIED="1379094309602" TEXT="Exemplo de agente">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1379010059082" FOLDED="true" ID="ID_8465012633" MODIFIED="1379010059082" TEXT="Rhalph">
<edge COLOR="#309eff"/>
<icon BUILTIN="full-1"/>
<node COLOR="#111111" CREATED="1379010059082" ID="ID_8868565367" MODIFIED="1379010059082" TEXT="Um rob&#xf4;">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1379010059082" ID="ID_3664828235" MODIFIED="1379010059082" TEXT="Problema para projetar">
<edge COLOR="#309eff"/>
<icon BUILTIN="full-2"/>
</node>
<node COLOR="#111111" CREATED="1379010059082" ID="ID_4257038202" MODIFIED="1379010059082" TEXT="Deliberate agent">
<edge COLOR="#309eff"/>
<icon BUILTIN="full-2"/>
</node>
<node COLOR="#111111" CREATED="1379010059082" ID="ID_1786776705" MODIFIED="1379010059082" TEXT="Agents of vacuum">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059082" ID="ID_3583600740" MODIFIED="1379010059082" TEXT="Caracter&#xed;sticas do agente">
<edge COLOR="#309eff"/>
<icon BUILTIN="full-3"/>
</node>
<node COLOR="#111111" CREATED="1379010059082" ID="ID_2324458071" MODIFIED="1379010059082" TEXT="A&#xe7;&#xf5;es que o agente faz">
<edge COLOR="#309eff"/>
<icon BUILTIN="full-3"/>
<node COLOR="#111111" CREATED="1379010059082" ID="ID_1748257522" MODIFIED="1379010059082" TEXT="movimenta&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059082" ID="ID_816039092" MODIFIED="1379010059082" TEXT="mudar dire&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1379010059083" ID="ID_1387025237" MODIFIED="1379010059083" TEXT="limpeza">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059083" ID="ID_606151760" MODIFIED="1379010059083" TEXT="sugar">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1379010059083" ID="ID_1279548231" MODIFIED="1379010059083" TEXT="ligar">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1379010059083" ID="ID_3161012522" MODIFIED="1379010059083" TEXT="Aprendizado do agente">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059083" ID="ID_8417858153" MODIFIED="1379010059083" TEXT="Passos ou exemplo para ele aprender">
<edge COLOR="#309eff"/>
<icon BUILTIN="full-4"/>
<node COLOR="#111111" CREATED="1379010059083" ID="ID_84383013" MODIFIED="1379010059083" TEXT="passos simples">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059083" ID="ID_593031743" MODIFIED="1379010059083" TEXT="movimentar sobre todos os quadrantes">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1379010059083" ID="ID_1697498294" MODIFIED="1379010059083" TEXT="se sujo, limpar">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1379010059083" ID="ID_1506668515" MODIFIED="1379010059083" TEXT="fun&#xe7;&#xe3;o next">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1379010059083" ID="ID_0737746856" MODIFIED="1379010059083" TEXT="Explica&#xe7;&#xe3;o dos passos">
<edge COLOR="#309eff"/>
<icon BUILTIN="full-4"/>
<node COLOR="#111111" CREATED="1379010059083" ID="ID_454338909" MODIFIED="1379010059083" TEXT="verificar se a regra principal est&#xe1; sendo disparada">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059084" ID="ID_646100020" MODIFIED="1379010059084" TEXT="se sujo, limpa">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1379010059084" ID="ID_194455993" MODIFIED="1379010059084" TEXT="movimenta&#xe7;&#xe3;o 0,0 para 2,2 e volta para 0,0">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059084" ID="ID_900034252" MODIFIED="1379010059084" TEXT="junto com a next">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1379010059084" ID="ID_488538954" MODIFIED="1379010059084" TEXT="&#xe9; o necess&#xe1;rio para deseguinar o comportamento do robo">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1379010059084" ID="ID_4583140588" MODIFIED="1379010059084" TEXT="Conclus&#xe3;o sobre as regras de aprendizado">
<edge COLOR="#309eff"/>
<icon BUILTIN="full-4"/>
<node COLOR="#111111" CREATED="1379010059084" ID="ID_1425926430" MODIFIED="1379013372172" TEXT="suposi&#xe7;&#xe3;o ">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059084" ID="ID_178810905" MODIFIED="1379010059084" TEXT="regras p">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1379010059084" ID="ID_1804107184" MODIFIED="1379010059084" TEXT="DB a">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1379010059084" ID="ID_1567247270" MODIFIED="1379010059084" TEXT="robo DO (*)">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1379010059084" ID="ID_1396054322" MODIFIED="1379013946232" TEXT="se ambiente muda entre t1 e t2">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059085" ID="ID_7525735180" MODIFIED="1379010059085" TEXT="Muito r&#xe1;pido">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059085" ID="ID_5555435782" MODIFIED="1379010059085" TEXT="A a&#xe7;&#xe3;o n&#xe3;o ser&#xe1; ideal">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1379010059085" ID="ID_5878182483" MODIFIED="1379010059085" TEXT="Se o ambiente muda instantaneamente">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059085" ID="ID_8052480827" MODIFIED="1379010059085" TEXT="O problema pode ser ignorado">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1379010059085" ID="ID_0677567872" MODIFIED="1379010059085" TEXT="Ambiente que muda muito r&#xe1;pido">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059085" ID="ID_0014727352" MODIFIED="1379010059085" TEXT="N&#xe3;o h&#xe1; como calcular a racionalidade">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1379010059085" ID="ID_8637738037" MODIFIED="1379010059085" TEXT="Muda mais r&#xe1;pido que o agente faz sua decis&#xe3;o de a&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1379010059085" ID="ID_6067032283" MODIFIED="1379010059085" TEXT="Problemas">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059085" ID="ID_2117786814" MODIFIED="1379010059085" TEXT="Percep&#xe7;&#xe3;o do ambiente">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059085" ID="ID_0020775210" MODIFIED="1379010059085" TEXT="Como ele mapeou o ambiente">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1379010059085" ID="ID_1776634506" MODIFIED="1379010059085" TEXT="Diferentes ambientes n&#xe3;o podem ser mapeados com linguagem">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059086" ID="ID_2520041383" MODIFIED="1379010059086" TEXT="Informa&#xe7;&#xe3;o tempor&#xe1;ria">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1379010165792" ID="ID_1528758509" MODIFIED="1379094309605" TEXT="desvantagens">
<node COLOR="#111111" CREATED="1379010956545" ID="ID_349885453" MODIFIED="1379094309605" TEXT="a complexidade do teorema de "/>
</node>
<node COLOR="#111111" CREATED="1379010807553" ID="ID_571419300" MODIFIED="1379094309605" TEXT="caracteristicas">
<node COLOR="#111111" CREATED="1379010811729" ID="ID_1610794376" MODIFIED="1379094309605" TEXT="abordagem elegante"/>
<node COLOR="#111111" CREATED="1379010834577" ID="ID_630956406" MODIFIED="1379094309605" TEXT="semantica limpa e simples"/>
</node>
<node COLOR="#111111" CREATED="1379010059084" ID="ID_794233812" MODIFIED="1379010059084" TEXT="racionalidade calculista">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node COLOR="#00b439" CREATED="1379010059086" ID="ID_8878236828" MODIFIED="1379094309606" TEXT="Genhard">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1379010059086" ID="ID_1015398515" MODIFIED="1379094309606" TEXT="sesao 1.4.1">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1379010059086" ID="ID_651320542" MODIFIED="1379010059086" TEXT="dedu&#xe7;&#xe3;o l&#xf3;gica">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059086" ID="ID_323127424" MODIFIED="1379010059086" TEXT="programada">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1379010059086" ID="ID_1523312498" MODIFIED="1379010059086" TEXT="atrav&#xe9;s de banco de dados">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059086" ID="ID_152758615" MODIFIED="1379010059086" TEXT="com informa&#xe7;&#xf5;es sobre o ambiente">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1379010059086" ID="ID_507755334" MODIFIED="1379010059086" TEXT="teorema de previs&#xe3;o">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1379010059086" ID="ID_838286593" MODIFIED="1379010059086" TEXT="diberate agents">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1379010059086" ID="ID_469591423" MODIFIED="1379010059086" TEXT="regras de dedu&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
<node COLOR="#111111" CREATED="1379010059086" ID="ID_1517698821" MODIFIED="1379010059086" TEXT="algoritmo">
<edge COLOR="#309eff"/>
</node>
</node>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1379010059087" FOLDED="true" ID="ID_2687687643" MODIFIED="1379094309607" POSITION="left" TEXT="Divis&#xe3;o para cada integrante">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1379010059087" FOLDED="true" ID="ID_353913119" MODIFIED="1379094309607" TEXT="de acordo com o livro Wooldridge">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1379010059087" ID="ID_1088651162" MODIFIED="1379094309607" TEXT="William">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1379010059087" ID="ID_3841253660" MODIFIED="1379010059087" TEXT="In&#xed;cio do cap&#xed;tulo 3">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1379010059087" ID="ID_7141351258" MODIFIED="1379010059087" TEXT="Somente primeira p&#xe1;gina">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1379010059087" ID="ID_6854070128" MODIFIED="1379010059087" TEXT="Leia um pouco de tudo e faca uma boa introdu&#xe7;&#xe3;o do assunto">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1379010059087" ID="ID_7047725670" MODIFIED="1379010059087" TEXT="Sei que voc&#xea; &#xe9; capaz de resumir. ">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1379010059087" ID="ID_3088284056" MODIFIED="1379094309607" TEXT="Bruno">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1379010059088" FOLDED="true" ID="ID_1580800130" MODIFIED="1379010059088" TEXT="P&#xe1;gina 48 apresentando os dois problemas de projeto">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1379010059088" ID="ID_4700850032" MODIFIED="1379010059088" TEXT="Transduction problem">
<edge COLOR="#309eff"/>
</node>
<node COLOR="#111111" CREATED="1379010059088" ID="ID_1221047165" MODIFIED="1379010059088" TEXT="Representation problem">
<edge COLOR="#309eff"/>
</node>
</node>
<node COLOR="#111111" CREATED="1379010059088" ID="ID_4823641187" MODIFIED="1379010059088" TEXT="At&#xe9; a p&#xe1;gina 51">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1379010059088" ID="ID_3377728140" MODIFIED="1379094309608" TEXT="Sebasti&#xe3;o">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1379010059088" ID="ID_0085775362" MODIFIED="1379010059088" TEXT="P&#xe1;gina 51">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1379010059088" ID="ID_0368164688" MODIFIED="1379010059088" TEXT="Apresenta&#xe7;&#xe3;o do rob&#xf4; de vacuum ">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1379010059088" ID="ID_8580631445" MODIFIED="1379010059088" TEXT="At&#xe9; p&#xe1;gina 53">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1379010059088" ID="ID_5305355246" MODIFIED="1379094309608" TEXT="Alex">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1379010059088" ID="ID_4888605428" MODIFIED="1379010059088" TEXT="O resto">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
</node>
</map>
