<map version="0.9.0" NAME="T2-" DATE="1383606763000" FILE="e2793e3a-5d4a-4af2-9773-bdd52fd53ddd.mm" MAPID="TP2NG0LURlaMkujZlYBlaA==" BGCOLOR="#F2F3F7">
<node ID="ID_1525527356" TEXT="T2-" COLOR="#000000" TSGSTYLE="NodeMasterGraphic">
<font NAME="SansSerif" SIZE="20" />
<edge COLOR="#309EFF" />
<node ID="ID_5582361302" TEXT="Agentes" POSITION="left" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#309EFF" />
<node ID="ID_2208841384" TEXT="Caracter&#237;sticas" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_5540886587" TEXT="Age em ambiente" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0653545621" TEXT="Comunica com outro agente" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5852265670" TEXT="Movido por conjunto de tend&#234;ncias #objetivo" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6276267820" TEXT="Possui recurso pr&#243;prio" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5433841202" TEXT="Tem percep&#231;&#227;o" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6170688844" TEXT="Capta estados do ambiente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1552608507" TEXT="Representa o ambiente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5226857028" TEXT="Pode se reproduzir" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="4iu6804dqelo743liicnfphqla" TEXT="Dif&#237;cil ser omnisciente" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="50vp8vhtk3vceejnadp388d2ra" TEXT="Racionalidade" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="7dkcr1va6apoldagp7ss3f9frv" TEXT="Criada com" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="05pu0gt0ua909tosa9l43oqe6s" TEXT="Aprendizado" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="36p7rd5u0gmd1kdt6j8rnj8u3v" TEXT="Percep&#231;&#245;es" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="2jfoq9nsq3h5bc8pa5p95r86jj" TEXT="Autonomia" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="77p4f9ao65pcqa99i3ml4f576h" TEXT="Ambiente" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="3c4mipjscar9mk1kqjss61o38l" TEXT="Caracter&#237;sticas" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="7d8hbvi7hentqbfbbmfhhlt2vg" TEXT="Peas" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="09f8mhkrbon75durlleuf1bqla" TEXT="Perfirmance" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="09u3ptdsvfa6p7kcnmlmrpehmc" TEXT="Environment" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="1l2kik0698d9saesheuusvooei" TEXT="Actuators" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="32ono9r8uqv9cpkhjta9f8i4oa" TEXT="Sensors" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
<node ID="ID_5316416048" TEXT="Tipos de agentes" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_6607432880" TEXT="Simples" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3034407264" TEXT="An&#225;logo ao reflexo humano" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1562177480" TEXT="A&#231;&#227;o aleat&#243;rios" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8410516442" TEXT="Ambientes simples" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2336605877" TEXT="Objetivo" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0740300681" TEXT="Prever pr&#243;ximos estados" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5825077526" TEXT="Jogo de dama" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3838328325" TEXT="Prev&#234; a pr&#243;xima jogada" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_2268213274" TEXT="Sabe como o mundo se comporta" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6240310254" TEXT="Utilidade" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6226311421" TEXT="Sabe como o mundo se comporta" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7567304410" TEXT="Sabe se &#233; &#250;til no ambiente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2274088025" TEXT="Agente que aprende" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7585266630" TEXT="Agente aut&#244;nomo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8882406764" TEXT="Mede a evolu&#231;&#227;o de resultados de a&#231;&#245;es" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5543827180" TEXT="Sabe se esta perto ou atingiu o objetivo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_6635086647" TEXT="Reativo" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6027080646" TEXT="Ar condicionado" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_1148747605" TEXT="IA" POSITION="left" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#309EFF" />
<node ID="ID_4432772057" TEXT="Categorias" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_5166355055" TEXT="Pensar" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8447218387" TEXT="Como humano" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5208680731" TEXT="N&#227;o se compara o racioc&#237;nio da m&#225;quina com humano" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_0763635237" TEXT="Racionalmente" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2133344101" TEXT="As vezes n&#227;o necessita de racioc&#237;nio" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1005361722" TEXT="Arist&#243;teles" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7833814711" TEXT="Silogismo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_6333716712" TEXT="Atuar" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7711764048" TEXT="Como humano" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2346765853" TEXT="Racionalmente" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7251126837" TEXT="Para atingir melhor resultado" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_3300313358" TEXT="Allan Turing" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_8787562018" TEXT="Teste" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4233324077" TEXT="O programa conversa" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3643074230" TEXT="Humano verifica se &#233; humano ou m&#225;quina" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_5418518578" TEXT="Influ&#234;ncia" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_7650384632" TEXT="Filosofia" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3825461125" TEXT="Positivismo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_7173221154" TEXT="Matem&#225;tica" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5225876251" TEXT="L&#243;gica booleana" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6212235442" TEXT="Formaliza&#231;&#227;o de l&#243;gica" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4586143611" TEXT="Estudo de algoritmo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3435642087" TEXT="Teorema de incerteza" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8757770100" TEXT="Crescimento exponencial" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0274300641" TEXT="Estat&#237;stica" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5467378730" TEXT="Economia" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3588302068" TEXT="Teoria dos jogos" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2785214387" TEXT="Procurar o fora do comum" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3337568478" TEXT="Teoria da utilidade" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3438307035" TEXT="Teoria da probabilidade" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4307502071" TEXT="Pesquisa operacional" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7571288518" TEXT="Otimiza&#231;&#227;o uso de recurso" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_2360131230" TEXT="Neuroci&#234;ncia" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3528581683" TEXT="Grafos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_7072752765" TEXT="Eng computa&#231;&#227;o" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0324286440" TEXT="Ajudou devido" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5532435040" TEXT="Evolu&#231;&#227;o dos recursos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_4040003041" TEXT="Lingu&#237;stica" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7202014240" TEXT="Google" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5605043773" TEXT="Saber contextualizar" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_5383245111" TEXT="Hist&#243;ria" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_3523640373" TEXT="Time line" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1031514111" TEXT="1 neur&#244;nio" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8375374540" TEXT="Sistemas de s&#237;mbolos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7766487701" TEXT="Realizar problemas complexos para ajudar os humanos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_8127852603" TEXT="Tend&#234;ncia" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1205210680" TEXT="Ubiquidade" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2865511043" TEXT="Interconex&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2240834506" TEXT="Inteligente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1610817374" TEXT="Elementos do curso" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4543748545" TEXT="Multi agentes" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_0582182467" TEXT="Intelig&#234;ncia" POSITION="right" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#309EFF" />
<node ID="ID_8657322755" TEXT="&#193;reas" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5200542000" TEXT="Capacidades" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_0124015015" TEXT="Aprender coisa nova" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6467532400" TEXT="Atrav&#233;s dos pais" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7224105304" TEXT="Criatividade" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1746307101" TEXT="Adapta&#231;&#227;o ao ambiente" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4247061557" TEXT="Racioc&#237;nio" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6280302035" TEXT="Fazer an&#225;lise" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4338867477" TEXT="Avaliar e julgar" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3111513673" TEXT="Criar seu ambiente" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_8421504666" TEXT="Gosto" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_0203306063" TEXT="Leituras sugeridas" POSITION="right" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#309EFF" />
<arrowlink ARROW_ID="ID_8628064473" DESTINATION="ID_4851645506" STARTINCLINATION="47;-109;" ENDINCLINATION="-193;204;" STARTARROW="None" ENDARROW="Default" />
</node>
<node ID="ID_4851645506" TEXT="Livro de Howard" POSITION="right" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#309EFF" />
<node ID="ID_0841655227" TEXT="Intelig&#234;ncia" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_3041424832" TEXT="Est&#237;mulo" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2010072486" TEXT="Habilidades" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6816867830" TEXT="L&#243;gica matem&#225;tica" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2732672554" TEXT="Espacial e lingu&#237;stica" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8644451874" TEXT="Corporal cinest&#233;sica" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8347402202" TEXT="Musical" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3174483337" TEXT="Interpessoal" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0763288016" TEXT="Intrapessoal" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8345237387" TEXT="Capacidade de saber o estado do corpo e mente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_7365375480" TEXT="Natural&#237;stica" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_3660742804" TEXT="Cr&#237;tica" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_3008340202" TEXT="Conceito espec&#237;fico para um tipo de situa&#231;&#227;o" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5481472202" TEXT="Confus&#227;o entre" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2350783054" TEXT="Aptid&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4233101814" TEXT="Intelig&#234;ncia" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1744134007" TEXT="Habilidade" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_1354023832" TEXT="Resumos de apresenta&#231;&#245;es - 1 rodada" POSITION="left" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#309EFF" />
<node ID="ID_1845142723" TEXT="Viviam" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_2167701333" TEXT="Inten&#231;&#227;o" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4031558872" TEXT="Pessoa" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1480003685" TEXT="Representa a a&#231;&#227;o com mais intensidade" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8280371037" TEXT="Jogar bola" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7016732013" TEXT="Tra&#231;a uma reta e vai tentar" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_6108271351" TEXT="Meio e fim" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1274434852" TEXT="Como decidir o meio para atingir o fim" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4605887211" TEXT="Algoritmo de strips" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4111763041" TEXT="Agente" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5826134316" TEXT="Precisa de" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7032483255" TEXT="Entrada" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5430382843" TEXT="Meta" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5470007825" TEXT="A&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6185350422" TEXT="Sa&#237;da" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6315823762" TEXT="Devolve ao ambiente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
<node ID="ID_4477745653" TEXT="Bocks words " COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_4186687743" TEXT="Implementa&#231;&#227;o de um agente" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3582077760" TEXT="C&#243;digo" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1035315773" TEXT="Estrutura em loop" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0172816630" TEXT="Problema" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3088061475" TEXT="Caracter&#237;sticas" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3366716122" TEXT="Observa o ambiente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1274568540" TEXT="Determina o planos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_8023757037" TEXT="Caracterizava cr&#237;ticas " COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3634225426" TEXT="Quanto tempo dura para fazer uma inten&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3141785810" TEXT="Para quando n&#227;o tem mais coisas no plano" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_8262053121" TEXT="Inten&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3375515213" TEXT="Reconsiderar inten&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5331348343" TEXT="Exemplo" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3508086283" TEXT="Caso de empregada" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2177624213" TEXT="Homer" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6503120203" TEXT="Estrat&#233;gia de compromisso" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8672751488" TEXT="Blind&#10;" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5171556110" TEXT="Single minded" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6233780510" TEXT="Open minded" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3023826627" TEXT="Tentando at&#233; chegar no limite" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node ID="ID_1680952078" TEXT="cleiviane" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_0642085158" TEXT="Arquitetura abstratas" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5706063580" TEXT="Arquitetura concreta" COLOR="#990000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4785435148" TEXT="Arquitetura em camadas" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7452237260" TEXT="Camadas horizontais" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7707241374" TEXT="S&#227;o v&#225;rias camadas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0601212656" TEXT="Uma sobre a outra" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2726268044" TEXT="Fornecem uma a&#231;&#227;o para o agente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5534405366" TEXT="Cada comportamento" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1730410781" TEXT="Uma camada" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1056770086" TEXT="Arquitetura" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3240516621" TEXT="Duas passagens" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1783344411" TEXT="Informa&#231;&#227;o sobe" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4014387330" TEXT="Desce para" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_4680350766" TEXT="Camadas horizontais" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0370632268" TEXT="Mediador" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1017264125" TEXT="Decide qual camada" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0362002636" TEXT="Problema" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3171244857" TEXT="Levar em considera&#231;&#227;o as intera&#231;&#245;es entre as camadas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7367845844" TEXT="Ruim para arquitetura de v&#225;rias camadas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_1504111201" TEXT="Vertical" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0833616724" TEXT="Uma passagem" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0555246503" TEXT="Gera a&#231;&#227;o na &#250;ltima" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6037330627" TEXT="Duas passagens" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_3464346505" TEXT="Turing" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2066714868" TEXT="Teste" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3680125171" TEXT="Verifica capacidade de intelig&#234;ncia" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4587386044" TEXT="3 elementos" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6354163380" TEXT="Homem" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8814271264" TEXT="M&#225;quina" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3615265702" TEXT="M&#225;quina" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6775183145" TEXT="Eliza" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7666104427" TEXT="Examinava perguntas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3860420043" TEXT="Respondia por contexto" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8638825512" TEXT="N&#227;o teve efic&#225;cia" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2688370373" TEXT="Passava perguntas em branco" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_7340182614" TEXT="Parry" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2640783616" TEXT="Simulou personalidade" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3103753545" TEXT="Conversou com a eliza" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0247258733" TEXT="Respondia por contexto" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2873840251" TEXT="Pessoa com paralisia paran&#243;ica" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_0424201228" TEXT="Respondeu 48 %" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_0037821638" TEXT="Arquitetura interrap" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7382578383" TEXT="Cada camada" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1528121761" TEXT="Tem base de conhecimento" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_4641182805" TEXT="Camada baixa" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6801503188" TEXT="Comportamento" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_4237052848" TEXT="Meio" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8752152626" TEXT="Conhecimento" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_8623616585" TEXT="Alta" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0073831408" TEXT="Coopera&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3380072282" TEXT="Intera&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2107537585" TEXT="Ativa&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0468808705" TEXT="Execu&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_197263955" TEXT="Alexandre" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_1486067711" TEXT="Arquitetura reativa" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7031066452" TEXT="Hist&#243;rico" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2808245414" TEXT="Introdu&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4302524617" TEXT="Formigas" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8541576170" TEXT="Complexidade em grupo" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4338806501" TEXT="Diversas atividades em grupo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_5287177247" TEXT="Caracter&#237;sticas" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4820077471" TEXT="Mudan&#231;a" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1404434102" TEXT="N&#227;o h&#225; hist&#243;rico" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1234845116" TEXT="N&#227;o reserva&#231;&#227;o do ambiente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5080003383" TEXT="N&#227;o h&#225; rela&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2170770817" TEXT="Cada um tem sua tarefa no ambiente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0676214528" TEXT="Grande n&#250;mero de agentes" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0177875322" TEXT="Novo conceito de inteligente" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4011677802" TEXT="Resultado da a&#231;&#227;o feita no ambiente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4076118472" TEXT="Cada formiga faz tem uma parte de todo o trabalho" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_5585561238" TEXT="Books" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7347247288" TEXT="Contextualiza&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8111351658" TEXT="Intelig&#234;ncia no mundo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_0110477708" TEXT="Acaso no ambiente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6847172623" TEXT="Comportamento" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8705372118" TEXT="Tarefa" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_0326033573" TEXT="Hierarquia" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<arrowlink ARROW_ID="ID_5186364207" DESTINATION="ID_6847172623" STARTINCLINATION="0;-150;" ENDINCLINATION="0;-150;" STARTARROW="None" ENDARROW="Default" />
<node ID="ID_3843213008" TEXT="Cada um em uma camada" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1401710405" TEXT="O mais baixo que ser&#225; pegado" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_5763541767" TEXT="Sensores tratam" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1036052105" TEXT="Ele AGI" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3116665378" TEXT="Exemplo" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0373638285" TEXT="Steels" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6544874552" TEXT="Objetivo" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2702388433" TEXT="Coletar pedra preciosa" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6622528748" TEXT="Ve&#237;culos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5824325682" TEXT="Ambiente" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6803200566" TEXT="Terreno cheio de obst&#225;culos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5686773811" TEXT="Gradiente" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2725701025" TEXT="Nave" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7215484666" TEXT="Faz sinal de r&#225;dio" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_8432453455" TEXT="Agente detecta o sinal" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1057383640" TEXT="Acha a nave por quantidade de sinal" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_7384735482" TEXT="A&#231;&#245;es" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7704327702" TEXT="Pegar" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0308867184" TEXT="Largar" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5543320160" TEXT="Caminho radioativo" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4031680236" TEXT="Leva at&#233; o monte de pedra" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1375215763" TEXT="&#201; feito pelo agente que levou a pedra para nave" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5616710054" TEXT="Comportamentos" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3578376778" TEXT="V&#225;rios testes" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
<node ID="ID_2677458576" TEXT="Eco resolu&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6630662524" TEXT="Organiza&#231;&#227;o de agentes" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4686838805" TEXT="Porque est&#225; lutando por lugar" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0183141041" TEXT="Com outro agente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_4357237200" TEXT="Exemplo" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2788626424" TEXT="Jogo" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7237677765" TEXT="Pe&#231;a toma o lugar do outro" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_4503680700" TEXT="Caracter&#237;stica" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6304682466" TEXT="Estado interno" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0480140002" TEXT="Busca da satisfa&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3323420230" TEXT="Estar satisfeito" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0281553685" TEXT="Insatisfeito" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3520051743" TEXT="Estar em fuga" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_8113422701" TEXT="Objetivo" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1770780381" TEXT="Estar satisfeito" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_7360701131" TEXT="Percep&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1530443675" TEXT="Formar uma pilha de m&#233;todos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6058301381" TEXT="Agentes" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7416211623" TEXT="Baseados em comportamentos de animais" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_1144124408" TEXT="Agre" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0343058312" TEXT="Insatisfa&#231;&#227;o com ia" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_0666613246" TEXT="Kaelbing" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3148331033" TEXT="Percep&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4421687406" TEXT="A&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5750876213" TEXT="Rooller" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2301256461" TEXT="Sem&#226;ntica" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6000010565" TEXT="Bit" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5102787781" TEXT="Estado" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5738548734" TEXT="Pode ser usado" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3363852527" TEXT="Entrada" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8571663070" TEXT="Sa&#237;da" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_8271140114" TEXT="Fatos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5681084167" TEXT="Funciona em simples circuito" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_4008867002" TEXT="Maes" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7306838627" TEXT="Agent network arquitetura" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1740305612" TEXT="Os agentes necessitam" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0306467784" TEXT="A&#231;&#245;es predefinidas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2753556865" TEXT="Complexo" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8426664817" TEXT="Devido ao n&#250;mero de camadas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0670035042" TEXT="Mais que 10 complexidade exponencial" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_4368245783" TEXT="Ambiente de desenvolvimento" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7116188310" TEXT="Sieme" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2167168448" TEXT="Swar" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_1171006251" TEXT="Requer gnu" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_7223670331" TEXT="Simula" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5514043651" TEXT="Fins did&#225;ticos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2038852470" TEXT="F&#225;cil &#250;" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3708551876" TEXT="Agentes reativos" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0538535087" TEXT="Comunicando entre si" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
<node ID="ID_1671344005" TEXT="Artigo em Portugas" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2787887606" TEXT="Nas refer&#234;ncias" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_935140942" TEXT="Alex" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_6531456103" TEXT="Livros" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3882546403" TEXT="Wan" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2154585124" TEXT="Dedu&#231;&#227;o l&#243;gica" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-1" />
<node ID="ID_1440228547" TEXT="Por racioc&#237;nio" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3407645707" TEXT="Teorema de previs&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-1" />
</node>
<node ID="ID_7156825007" TEXT="Exemplo de agente" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8465012633" TEXT="Rhalph" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-1" />
<node ID="ID_8868565367" TEXT="Um rob&#244;" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3664828235" TEXT="Problema para projetar" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-2" />
</node>
<node ID="ID_4257038202" TEXT="Deliberate agent" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-2" />
</node>
<node ID="ID_1786776705" TEXT="Agents of vacuum" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3583600740" TEXT="Caracter&#237;sticas do agente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-2" />
</node>
<node ID="ID_2324458071" TEXT="A&#231;&#245;es que o agente faz" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-3" />
</node>
<node ID="ID_3161012522" TEXT="Aprendizado do agente" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8417858153" TEXT="Passos ou exemplo para ele aprender" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-3" />
</node>
<node ID="ID_0737746856" TEXT="Explica&#231;&#227;o dos passos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-4" />
</node>
<node ID="ID_4583140588" TEXT="Cria&#231;&#227;o das regras de aprendizado" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-4" />
</node>
</node>
</node>
</node>
</node>
<node ID="ID_8878236828" TEXT="Gen" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_2426467641" TEXT="Apresenta&#231;&#245;es-rodada 2" POSITION="right" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6815255237" TEXT="Aula 1-Intera&#231;&#227;o entre multiagentes agentes" COLOR="#1b9701" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_6157283826" TEXT="Ambientes" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_2722640754" TEXT="Necessidade de mais de um agente" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
</node>
<node ID="ID_1458463213" TEXT="Agentes" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_4038484508" TEXT="Sofre influ&#234;ncia" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_2854166261" TEXT="Ter controle" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
<arrowlink ARROW_ID="ID_7211051502" DESTINATION="ID_6157283826" STARTINCLINATION="0;-150;" ENDINCLINATION="0;-150;" STARTARROW="None" ENDARROW="Default" />
</node>
</node>
</node>
<node ID="ID_0065432138" TEXT="Conjunto &#244;mega" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_7448755311" TEXT="Resultados" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
<node ID="ID_0802753221" TEXT="Estados do ambiente" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
</node>
<node ID="ID_2368058004" TEXT="Resultado" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_2063582756" TEXT="Traz" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_8836346032" TEXT="Ordena&#231;&#227;o" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
</node>
</node>
<node ID="ID_6115833305" TEXT="Fun&#231;&#227;o de utilidade" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_8447205303" TEXT="Ex" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_6652046311" TEXT="Dinheiro" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
</node>
<node ID="ID_1107114002" TEXT="N&#227;o &#233; LINEAR" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
</node>
<node ID="ID_6733800207" TEXT="Encontro de multiagentes" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_3673014680" TEXT="Conjunto" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
<node ID="ID_0206661182" TEXT="Resultado" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
</node>
<node ID="ID_3773047851" TEXT="Estrat&#233;gia" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_7827328117" TEXT="Dominantes" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_4843414532" TEXT="Problema" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_5524430675" TEXT="Deve exclui " COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_6640005133" TEXT="Fracamente dominantes" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
</node>
</node>
<node ID="ID_8146375825" TEXT="A&#231;&#245;es por" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_4505214688" TEXT="Conjuntos" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
</node>
</node>
<node ID="ID_2650735122" TEXT="Equil&#237;brio de nash" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_5327713701" TEXT="Depende do cen&#225;rio que se esta" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
<node ID="ID_3153077537" TEXT="Verifica a intera&#231;&#227;o entre os agentes" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
<node ID="ID_5363225470" TEXT="Tende" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_3058661467" TEXT="O que faca melhor para ele" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
<node ID="ID_1608414744" TEXT="Pensa na a&#231;&#227;o do outro" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
</node>
<node ID="ID_5304203652" TEXT="Ex" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_8383418085" TEXT="Times precisando de empate" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#34B1FD" />
<node ID="ID_3106502575" TEXT="Os dois n&#227;o ir&#227;o fazer gol" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#34B1FD" />
</node>
</node>
</node>
</node>
</node>
</node>
</node>
<node ID="ID_2378301456" TEXT="Jade" POSITION="left" COLOR="#000000" TSGSTYLE="NodeBasicGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6378361502" TEXT="Java" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5242422753" TEXT="Caracter&#237;sticas" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7146607788" TEXT="Teoria" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7562474370" TEXT="Comunica&#231;&#227;o" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5867817563" TEXT="Fipa" COLOR="#000000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8021426304" TEXT="Funda&#231;&#227;o" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5308264001" TEXT="Objetivo" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4473335255" TEXT="Determinar como os agentes devem ser desenhados" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_8721713072" TEXT="Criou as camadas de comunica&#231;&#227;o" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4680440444" TEXT="Transporte" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2664418533" TEXT="Codifica&#231;&#227;o" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5623524771" TEXT="Iso" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1457754581" TEXT="Xml" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1840228188" TEXT="Mensagem" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8836300314" TEXT="Fila acl" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_8788784278" TEXT="Ontologia" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1255323310" TEXT="Express&#227;o de conte&#250;do" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7133118814" TEXT="Fipa sl" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6877562135" TEXT="A&#231;&#245;es comunicativas" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6243266661" TEXT="Inform" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7458185756" TEXT="Request" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7748608360" TEXT="Agree" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_0242057221" TEXT="Protocolo de intera&#231;&#227;o" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6335427341" TEXT="Regulamenta&#231;&#227;o" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
<node ID="ID_4741052883" TEXT="Arquiteturas" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4850528818" TEXT="Reativas" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4048536746" TEXT="H&#237;bridas" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5074212563" TEXT="Deliberativas" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_7422753157" TEXT="Aplica&#231;&#227;o" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1775010154" TEXT="Coisas simples" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7327277087" TEXT="A coisas complexas" COLOR="#000000" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
</map>
