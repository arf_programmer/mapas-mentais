<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1390044372763" ID="ID_1738007225" LINK="estudo%20de%20MD.mm" MODIFIED="1390411958701" TEXT="cole&#xe7;&#xe3;o shaum">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1390411946503" ID="ID_516567110" MODIFIED="1390411958685" POSITION="left" TEXT="indice">
<edge STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1390044474113" ID="ID_822262289" MODIFIED="1390411958688" TEXT="teoria dos conjuntos">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044489931" ID="ID_1442353627" MODIFIED="1390411958688" TEXT="rela&#xe7;&#xf5;es">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044495854" ID="ID_1463002284" MODIFIED="1390411958690" TEXT="fun&#xe7;oes e algor&#xed;tmos">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044507988" ID="ID_65709781" MODIFIED="1390411958691" TEXT="l&#xf3;gica e calculo proposicional">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044530942" ID="ID_1809549651" MODIFIED="1390411958693" TEXT="vetores e matrizes">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044542456" ID="ID_1405876033" MODIFIED="1390411958693" TEXT="contagem">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044547258" ID="ID_799810114" MODIFIED="1390411958694" TEXT="teoria das probabilidades">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044556882" ID="ID_1616643942" MODIFIED="1390411958694" TEXT="teoria dos grafos">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044563984" ID="ID_1200247370" MODIFIED="1390411958695" TEXT="grafos orientados">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044571738" ID="ID_864755942" MODIFIED="1390411958696" TEXT="arvores bin&#xe1;rias">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044579722" ID="ID_1695415900" MODIFIED="1390411958696" TEXT="propriedades dos inteiros">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044588874" ID="ID_1729533836" MODIFIED="1390411958698" TEXT="sistemas alg&#xe9;bricos">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044597769" ID="ID_732176215" MODIFIED="1390411958699" TEXT="linguagens, gram&#xe1;tica e m&#xe1;quinas">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044632483" ID="ID_1445860043" MODIFIED="1390411958700" TEXT="conjunto ordenados e reticulados">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044661237" ID="ID_386329185" MODIFIED="1390411958700" TEXT="algebra booleana">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
<node COLOR="#00b439" CREATED="1390044672051" ID="ID_898484267" MODIFIED="1390411958700" TEXT="rela&#xe7;&#xf5;es de recorrencia">
<edge STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
</node>
</node>
</node>
</map>
