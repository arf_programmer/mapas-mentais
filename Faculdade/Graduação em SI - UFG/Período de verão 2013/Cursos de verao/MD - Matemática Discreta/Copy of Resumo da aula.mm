<map version="0.9.0" NAME="Resumo" DATE="1392287627105" FILE="214a3b80-89ef-482f-82c2-32570bb2207d.mm" MAPID="88vPDe2fRTW7ngl1c71g5A==" BGCOLOR="#F2F3F7">
<node ID="ID_6143586028" TEXT="Resumo" COLOR="#000000" TSGSTYLE="NodeMasterGraphic">
<font NAME="SansSerif" SIZE="20" />
<edge COLOR="#309EFF" />
<node ID="ID_6381373848" TEXT="&#205;ndex" POSITION="left" COLOR="#0033ff" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="18" />
<edge COLOR="#309EFF" />
<node ID="ID_8160034235" TEXT="Proposi&#231;&#245;es, representa&#231;&#245;es e tautologias" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_7843426847" TEXT="Proposi&#231;&#227;o ou declara&#231;&#227;o" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8418278402" TEXT="&#201; senten&#231;a booleana" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2727040877" TEXT="Representada por letras mai&#250;sculas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1527321670" TEXT="Tipo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4078328006" TEXT="Simples" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7771264713" TEXT="Valores de uma proposi&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3187251543" TEXT="Composta" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7265051285" TEXT="Combina&#231;&#227;o de duas proposi&#231;&#245;es" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7586145186" TEXT="Atrav&#233;s de um conectivo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<arrowlink ARROW_ID="ID_4605616821" DESTINATION="ID_4373532287" STARTINCLINATION="0;-150;" ENDINCLINATION="0;-150;" STARTARROW="None" ENDARROW="Default" />
</node>
</node>
</node>
</node>
</node>
<node ID="ID_3148623040" TEXT="F&#243;rmula bem formada" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_8483016723" TEXT="&#201; uma express&#227;o v&#225;lida dentro das regras de sintaxe" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8106857740" TEXT="Wff" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6801135360" TEXT="Well formed formule" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3251556676" TEXT="Ordem de preced&#234;ncia" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_2007057406" TEXT="Par&#234;nteses e colchetes" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4854858610" TEXT="Com dois conectivos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_3453380156" TEXT="Segue da esquerda para direita" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_8331400564" TEXT="Sempre cont&#233;m conectivo principal" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5431272415" TEXT="&#218;ltimo a ser resolvido" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_4373532287" TEXT="Conectivos" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6506552701" TEXT="Operadores associados a uma proposi&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2257227376" TEXT="Tipos" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6463285254" TEXT="Conjun&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5704484576" TEXT="Primeiro operador matem&#225;tico" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8785061842" TEXT="multiplica&#231;&#227;o na matem&#225;tica" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_0070675747" TEXT="Disjun&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4048044063" TEXT="Soma na matem&#225;tica" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5724622810" TEXT="Condicional ou implica&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8712335637" TEXT="Se ent&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6334053317" TEXT="Duas preposi&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0661775377" TEXT="A e B" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6870780406" TEXT="A &#233; antecedente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2076184427" TEXT="B &#233; consequente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_1555068510" TEXT="Falso somente em um caso" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<richcontent TYPE="NOTE">
<pre>A verdadeiro e B falso</pre>
</richcontent>
</node>
</node>
<node ID="ID_6202013358" TEXT="Bicondicional" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_3574033414" TEXT="Duas implica&#231;&#245;es e uma conjun&#231;&#227;o delas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4475543277" TEXT="A &#8212;&gt; B ^ B -&gt; A" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6418584088" TEXT="S&#237;mbolo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2570275334" TEXT="A &lt;-&gt; B" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_1407536051" TEXT="Nega&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4761458278" TEXT="Inverso da preposi&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2204024015" TEXT="S&#237;mbolos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0223587637" TEXT="aspa simples - '" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1777462749" TEXT="til - ~" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_445573305" TEXT="duas barras para baixo - &#172;&#172;" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_2033070743" TEXT="Equival&#234;ncia" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4656086754" TEXT="As duas proposi&#231;&#227;o &#233; igual" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4518173614" TEXT="Resulta em verdadeiro" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6612074567" TEXT="S&#237;mbolo" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1152704448" TEXT="&lt;=&gt;" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_7685657101" TEXT="Categoria" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_3352816876" TEXT="Bin&#225;rio" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1133004620" TEXT="Duas preposi&#231;&#245;es" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3125834242" TEXT="Un&#225;rio" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0818305461" TEXT="Uma preposi&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_7804603325" TEXT="Valores l&#243;gicos" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0060762752" TEXT="Resultado de uma proposi&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1773346235" TEXT="Tabela verdade" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_295736962" TEXT="simbolos utilizados" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5460404263" TEXT="Com uso de 0 e 1" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1685214611" TEXT="Liga&#231;&#227;o com operadores matem&#225;tico" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6357774740" TEXT="V ou f" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_4788804547" TEXT="Quantidade de linhas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5676515737" TEXT="2 elevado a quantidade de preposi&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1391001979" TEXT="2^n" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
<node ID="ID_3070207375" TEXT="Express&#245;es" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5540386743" TEXT="Tautologia" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4720802145" TEXT="Sempre verdadeiro" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1646376843" TEXT="Contradi&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6216385207" TEXT="Sempre falso" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_8783255330" TEXT="L&#243;gica proposicional" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<richcontent TYPE="NOTE">
<pre>Exerc&#237;cios realizados no app fii note</pre>
</richcontent>
<node ID="ID_5540078043" TEXT="Regras de equival&#234;ncia" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_419493610" TEXT="tipos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6084435516" TEXT="Comutatividade" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1565331524" TEXT="Associatividade" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3163548654" TEXT="De morgan" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3376150732" TEXT="Condicional" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6881081243" TEXT="Dupla nega&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4370187155" TEXT="Defini&#231;&#227;o de equival&#234;ncia" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1468599632" TEXT="s&#227;o bidirecionais" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_652415245" TEXT="tanto um lado chega no outro quanto o outro lado" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_8177266614" TEXT="Regras de infer&#234;ncia" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<richcontent TYPE="NOTE">
<pre>At&#233; p&#225;g 27</pre>
</richcontent>
<node ID="ID_5650300361" TEXT="Tipos" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0855235612" TEXT="Modos pones" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6414607727" TEXT="Modos tolens" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2484471330" TEXT="Conjun&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1847715774" TEXT="Simplifica&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0616227882" TEXT="Adi&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1205974523" TEXT="sh" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1593899123" TEXT="sd" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_993762978" TEXT="pd" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3451281373" TEXT="N&#227;o s&#227;o bidirecionais" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8158266742" TEXT="Funciona da esquerda para direita" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_3733224823" TEXT="Dedu&#231;&#227;o" COLOR="#990000" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_6514387454" TEXT="Argumento" COLOR="#111111" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7763175622" TEXT="formato" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1366381643" TEXT="P1 ^ p2 ^ pn -&gt; Q" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_8863470560" TEXT="Resolu&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4885568565" TEXT="Liste as hip&#243;teses" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-1" />
<node ID="ID_297090644" TEXT="cada proprosi&#231;&#227;o separada por uma conjun&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3474385143" TEXT="Tente utilizar MP primeiramente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-2" />
</node>
<node ID="ID_3406088522" TEXT="Simplifique focando na hip&#243;tese que cont&#233;m o resultado do argumento" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<icon BUILTIN="full-3" />
<node ID="ID_873789788" TEXT="foque em Q" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<arrowlink ARROW_ID="ID_6077167824" DESTINATION="ID_1366381643" STARTINCLINATION="441;0;" ENDINCLINATION="441;0;" STARTARROW="None" ENDARROW="Default" />
</node>
</node>
</node>
<node ID="ID_0851458171" TEXT="Argumentos com implica&#231;&#227;o como resultado" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2685771207" TEXT="Simplifique colocando o antecedente como mais uma proposi&#231;&#227;o do argumento" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1142622588" TEXT="p1 ^ pn -&gt; A-&gt;B" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1246030856" TEXT="p1 ^ pn ^ A -&gt; B" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_541624599" TEXT="seguindo o formato de um argumento" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_5055085410" TEXT="Sugest&#245;es" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6761380130" TEXT="Use MP primeiramente" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8732434473" TEXT="Utilize de Morgan em nega&#231;&#245;es" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6007185365" TEXT="Pois s&#227;o dif&#237;ceis de serem usadas" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1724806625" TEXT="construa novas hipoteses com a adi&#231;&#227;o" COLOR="#111111" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_1710360221" TEXT="Demostra&#231;&#227;o" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_8262350145" TEXT="Tipo" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0676047821" TEXT="Contraposi&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4380461061" TEXT="Inverte proposi&#231;&#245;es e as nega" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2103081208" TEXT="Prova" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_4315813073" TEXT="Direta" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2237214007" TEXT="Aplicando regras de infer&#234;ncia" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5730064421" TEXT="Rec&#237;proca" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4523856041" TEXT="Inverte a implica&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_8840263873" TEXT="Absurdo ou indireta" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7255116427" TEXT="A hip&#243;tese &#233; verdadeira e a nega&#231;&#227;o da conclus&#227;o tamb&#233;m verdadeira" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1048622674" TEXT="Na prova deve chegar em uma contradi&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0871175113" TEXT="Que simbolicamente &#233; 0" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_3315778235" TEXT="Exaust&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1654412185" TEXT="Para n&#250;mero finito e n&#250;mero de casos pequeno" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_0157415606" TEXT="Resolu&#231;&#227;o" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4865665244" TEXT="Aplicar regras de infer&#234;ncia, equival&#234;ncia" COLOR="#000034" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_4214538413" TEXT="Conjuntos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_0131223070" TEXT="Conjunto" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7555573228" TEXT="Tipos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8338366578" TEXT="Conjunto finito" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7278001760" TEXT="Conjunto infinito" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5328772781" TEXT="Nota&#231;&#227;o" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8488318111" TEXT="Predicado un&#225;rio" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1836171815" TEXT="Padr&#227;o para conjuntos conhecidos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7755851214" TEXT="N" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6651155585" TEXT="Inteiros positivos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_4303182380" TEXT="Z" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8270787437" TEXT="Inteiros" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_8508550558" TEXT="Q" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2143034566" TEXT="Racionais" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2407586836" TEXT="R" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4565671584" TEXT="Reais" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2577527440" TEXT="C" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_1671760417" TEXT="Zero cortado" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_1536631678" TEXT="Relacionamento" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5714314074" TEXT="Exist&#234;ncia" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8583534162" TEXT="Subconjunto" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3553634387" TEXT="Subconjunto pr&#243;prio" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6202361766" TEXT="Tem um elemento quem n&#227;o cont&#233;m no outro conjunto" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_2580574104" TEXT="Heran&#231;a" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_7503671505" TEXT="Propriedade de um conjunto que &#233; subconjunto de outro, ele ter&#225; as propriedades herdadas" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1780501464" TEXT="Como uma classe de POO que herda de outra classe" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_4610018501" TEXT="Partes de conjuntos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4463375768" TEXT="Formado por" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1507064802" TEXT="Conjunto vazio" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7273320661" TEXT="Os subconjuntos unit&#225;rios" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7566601474" TEXT="Cada elemento como subconjunto" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3151283330" TEXT="Conjuntos por par" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5023446474" TEXT="Elementos juntos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6057425321" TEXT="Todos elementos combinados" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6374287015" TEXT="Conjunto com todos subconjuntos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7164567686" TEXT="Com todos elementos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_7884371765" TEXT="Opera&#231;&#245;es bin&#225;rias" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7870813284" TEXT="Para ser poss&#237;vel em um conjunto" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7827500020" TEXT="O resultado da opera&#231;&#227;o deve conter" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1317266535" TEXT="Um elemento do conjunto" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3132676374" TEXT="Elemento &#250;nico" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_2743462153" TEXT="Deve conter dois elementos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_7077588258" TEXT="Opera&#231;&#245;es un&#225;ria" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6471607043" TEXT="Aplicada a um elemento" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_1062741561" TEXT="Conjunto universo" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2163141140" TEXT="Opera&#231;&#245;es de conjuntos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1224306855" TEXT="Uni&#227;o" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4363742247" TEXT="Interse&#231;&#227;o" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8826628713" TEXT="Diagrama de venn" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_3837131713" TEXT="Dentro dele cont&#233;m todos conjuntos as avaliados" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2131816252" TEXT="Produto cartesiano" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7025703528" TEXT="N&#227;o &#233; opera&#231;&#227;o bin&#225;ria" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_4445874156" TEXT="Prova identidade de conjuntos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8387166147" TEXT="M&#233;todos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4267762115" TEXT="Diagrama de venn" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_3804752866" TEXT="Direta- subconjunto de um e do outro" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8808434608" TEXT="Substituir por x &#8364; Aos conjuntos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8466257864" TEXT="Ir substituindo os conectivos por portugu&#234;s" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_8161288136" TEXT="Aplicar regras de equival&#234;ncia ou infer&#234;ncia" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0424001722" TEXT="Chegar no resultado do outro lado" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4625035118" TEXT="E depois fazer ao contr&#225;rio. " COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6213440443" TEXT="Dual" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1852300454" TEXT="Inverte os conectivos e os conjuntos finais" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
</node>
<node ID="ID_2180686080" TEXT="Rela&#231;&#245;es" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_5576232881" TEXT="Rela&#231;&#245;es bin&#225;rias" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2753533800" TEXT="Propriedades de rela&#231;&#245;es" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_4342285565" TEXT="Reflexiva" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6240520017" TEXT="Se todos x que pertence a S, tem o par ordenado x,x" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5473181440" TEXT="Simetria" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5725253845" TEXT="Se existe x,y e y,x " COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5427021401" TEXT="Transitiva" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_3577507747" TEXT="Se x,y e y,z e -&gt; x,z" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_3364601540" TEXT="Rela&#231;&#227;o de equival&#234;ncia" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0563185870" TEXT="Parti&#231;&#227;o de um conjunto" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4074111610" TEXT="Criar sub classes" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_8414136711" TEXT="Ordens" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_1470650744" TEXT="Ordem parcial" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5461350323" TEXT="Deve ser" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7100208208" TEXT="Ante sim&#233;trica" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0847578463" TEXT="Se reflexiva" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5810451522" TEXT="Transitiva" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2055541145" TEXT="Diagrama de hasse" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5727203024" TEXT="Mostra rela&#231;&#227;o de pares" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7540032206" TEXT="Parte do c&#237;rculo abaixo" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6314387380" TEXT="Significa primeira componente" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_3435242475" TEXT="Cada rela&#231;&#227;o &#233; uma linha entre os c&#237;rculos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_0587175073" TEXT="Ordem total" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_0853347572" TEXT="Quando tem todos elementos relacionados com outros" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_5232821004" TEXT="Forma uma reta no diagrama de hasse" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<arrowlink ARROW_ID="ID_0514085281" DESTINATION="ID_2055541145" STARTINCLINATION="0;-150;" ENDINCLINATION="0;-150;" STARTARROW="None" ENDARROW="Default" />
</node>
</node>
<node ID="ID_0050132016" TEXT="Elementos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7633858381" TEXT="M&#225;ximo" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_2854387668" TEXT="M&#237;nimo" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_7754314402" TEXT="Maximal" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4627646222" TEXT="Minimal" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
<node ID="ID_5352407771" TEXT="Fun&#231;&#227;o" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_0570838073" TEXT="Dom&#237;nio" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4828187551" TEXT="Conjunto s" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4484333602" TEXT="Todos elementos devem possuir somente uma imagem no contra dom&#237;nio" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2581674003" TEXT="Contradom&#237;nio" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8782384780" TEXT="Conjunto t" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2316541317" TEXT="Conceitos" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5365031010" TEXT="Igual" COLOR="#00b439" TSGSTYLE="NodeTextGraphic" FOLDED="true">
<edge COLOR="#309EFF" />
<node ID="ID_5014418752" TEXT="Tem dom&#237;nios e contra dom&#237;nios iguais e pares iguais" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_7736311660" TEXT="Imagem" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4304528200" TEXT="Tipos de fic&#231;&#245;es" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5168305480" TEXT="Sobrejetora" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_8428743517" TEXT="Possui todos elementos no contra dom&#237;nio" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_4184144263" TEXT="E se dom&#237;nio &#233; igual a contradom&#237;nio" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_4623005051" TEXT="Injetoras" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_5532184550" TEXT="Possui somente um elemento no dom&#237;nio" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_6475276641" TEXT="Um elemento no contra dom&#237;nio n&#227;o possui duas imagens inversas" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_0637663511" TEXT="Bijetoras" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_1000537666" TEXT="&#201; uma fun&#231;&#227;o sobrejetora e injetora" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_6228744338" TEXT="Composta" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_4815385617" TEXT="Uma fun&#231;&#227;o &#233; o contradom&#237;nio de outro contradom&#237;nio de outra fun&#231;&#227;o" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5081661433" TEXT="Inversa" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7380163280" TEXT="" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</node>
<node ID="ID_7468376456" TEXT="Indu&#231;&#227;o" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_7042348405" TEXT="Primeiro princ&#237;pio" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_7205027530" TEXT="Base da indu&#231;&#227;o" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6042023074" TEXT="Primeira proposi&#231;&#227;o verdade" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_5421232308" TEXT="P(k) -&gt; P(k+1)" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
<node ID="ID_0817330324" TEXT="Primeira verdade" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_0735243306" TEXT="Primeiro degrau &#233; ating&#237;vel" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_2688682584" TEXT="Passo indutivo" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_2743165443" TEXT="Prova o k+1" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
<node ID="ID_6480565338" TEXT="Usando o o anterior ao k" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
<node ID="ID_2558572324" TEXT="Segundo princ&#237;pio" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
<node ID="ID_4020232502" TEXT="Recorr&#234;ncia" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<font NAME="SansSerif" SIZE="16" />
<edge COLOR="#309EFF" />
<node ID="ID_1468182240" TEXT="" COLOR="#00b439" TSGSTYLE="NodeTextGraphic">
<edge COLOR="#309EFF" />
</node>
</node>
</node>
</node>
</map>
