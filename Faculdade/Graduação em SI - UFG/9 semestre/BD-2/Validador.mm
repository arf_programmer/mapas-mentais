<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1403221564212" ID="ID_1138077687" MODIFIED="1403221595439" TEXT="Validador">
<node CREATED="1403221664237" ID="ID_1294656508" MODIFIED="1403221667155" POSITION="right" TEXT="tarefas">
<node CREATED="1403221667496" ID="ID_386534478" MODIFIED="1403221667496" TEXT=""/>
</node>
<node CREATED="1403221669829" ID="ID_1771201555" MODIFIED="1403233518550" POSITION="right" TEXT="triggers">
<node CREATED="1403221691774" ID="ID_631897498" MODIFIED="1403221691774" TEXT="defini&#xe7;&#xe3;o de cpf de funcion&#xe1;rio para 11 posi&#xe7;&#xf5;es;">
<node CREATED="1403222707701" ID="ID_1582129047" MODIFIED="1403222741795" TEXT="criar trigger para validar">
<node CREATED="1403222742155" ID="ID_944009113" MODIFIED="1403222745353" TEXT="insert"/>
<node CREATED="1403222746372" ID="ID_1020363634" MODIFIED="1403222748950" TEXT="update"/>
</node>
</node>
<node CREATED="1403221691781" ID="ID_1799992523" MODIFIED="1403227396619" TEXT="CPF de funcion&#xe1;rio deve ser v&#xe1;lido (conforme c&#xe1;lculos dos d&#xed;gitos verificadores);">
<node CREATED="1403222791575" ID="ID_1940840872" MODIFIED="1403222800433" TEXT="trigger com ">
<node CREATED="1403222800688" ID="ID_115089515" MODIFIED="1403222802965" TEXT="insert"/>
<node CREATED="1403222803204" ID="ID_108173992" MODIFIED="1403222806792" TEXT="update"/>
<node CREATED="1403222807039" ID="ID_1270318529" MODIFIED="1403222808702" TEXT="ambas">
<node CREATED="1403222808964" ID="ID_1294076501" MODIFIED="1403222825943" TEXT="usar algoritmo para verificar tam do cpf"/>
</node>
</node>
</node>
<node CREATED="1403221691778" ID="ID_1332772025" MODIFIED="1403221691778" TEXT="gravar automaticamente o hist&#xf3;rico de ger&#xea;ncia de um departamento (criar nova tabela);">
<node CREATED="1403222757959" ID="ID_610460491" MODIFIED="1403222765847" TEXT="criar trigger">
<node CREATED="1403222766132" ID="ID_1092555111" MODIFIED="1403222772512" TEXT="todas operacoes">
<node CREATED="1403222775185" ID="ID_915722131" MODIFIED="1403222782847" TEXT="inserir dado em uma tabela Log"/>
</node>
</node>
</node>
<node CREATED="1403221691782" ID="ID_1029277474" MODIFIED="1403221691782" TEXT="sal&#xe1;rio de supervisor deve ser superior ao sal&#xe1;rio de TODOS os seus supervisionados;">
<node CREATED="1403222858247" ID="ID_532405688" MODIFIED="1403222862910" TEXT="validar ">
<node CREATED="1403222863166" ID="ID_1036651045" MODIFIED="1403222867006" TEXT="update"/>
<node CREATED="1403222867430" ID="ID_1200735888" MODIFIED="1403222870903" TEXT="insert"/>
<node CREATED="1403223731318" ID="ID_1316051471" MODIFIED="1403223745225" TEXT="salario de supervisor e supervisionado"/>
</node>
</node>
<node CREATED="1403221691787" ID="ID_1077822293" MODIFIED="1403221691787" TEXT="funcion&#xe1;rio deve trabalhar somente em projetos que s&#xe3;o controlados pelo departamento em que ele trabalha;">
<node CREATED="1403222858247" ID="ID_622916992" MODIFIED="1403222862910" TEXT="validar ">
<node CREATED="1403222863166" ID="ID_1659397035" MODIFIED="1403222867006" TEXT="update"/>
<node CREATED="1403222867430" ID="ID_159103376" MODIFIED="1403222870903" TEXT="insert"/>
<node CREATED="1403223708401" ID="ID_921793007" MODIFIED="1403223724569" TEXT="departamento de funcionario"/>
</node>
</node>
<node CREATED="1403233517713" ID="ID_327663196" MODIFIED="1403233542125">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      a quantidade de horas semanais que um empregado trabalha em projetos n&#227;o pode ser superior a 40 horas.
    </p>
  </body>
</html>
</richcontent>
<node CREATED="1403233578538" ID="ID_379220663" MODIFIED="1403233581572" TEXT="validar">
<node CREATED="1403233581935" ID="ID_977630540" MODIFIED="1403233584859" TEXT="insert"/>
<node CREATED="1403233585591" ID="ID_1843730718" MODIFIED="1403233587388" TEXT="update"/>
<node CREATED="1403233587681" ID="ID_1740678115" MODIFIED="1403233594099" TEXT="trabalha_em"/>
</node>
</node>
</node>
</node>
</map>
