<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#000000" CREATED="1407717360257" ID="ID_2518613878" MODIFIED="1407717711344" TEXT="Estudo para prova">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="20"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties"/>
<node COLOR="#0033ff" CREATED="1407717360257" ID="ID_0144522151" MODIFIED="1411136207205" POSITION="right" TEXT="Conte&#xfa;do total">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1407717360258" FOLDED="true" ID="ID_3822025722" MODIFIED="1411136214864" TEXT="Matem&#xe1;tica">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717360258" ID="ID_5825722371" MODIFIED="1407717711233" TEXT="&#xc1;lgebra LINEAR">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360258" ID="ID_1856535120" MODIFIED="1407717711234" TEXT="An&#xe1;lise combinat&#xf3;ria">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360258" ID="ID_4583053738" MODIFIED="1407717711234" TEXT="C&#xe1;lculo diferencial ou integral">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360258" ID="ID_6684365486" MODIFIED="1407717711235" TEXT="Geometria anal&#xed;tica">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360258" ID="ID_3075030613" MODIFIED="1407717711236" TEXT="L&#xf3;gica matem&#xe1;tica">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360259" ID="ID_2506444176" MODIFIED="1407717711237" TEXT="Matem&#xe1;tica discreta">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360259" ID="ID_3832468788" MODIFIED="1407717711238" TEXT="Probabilidade e estat&#xed;stica">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360259" FOLDED="true" ID="ID_0355356310" MODIFIED="1411136212679" TEXT="Fundamentos de computa&#xe7;&#xe3;o">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717360259" ID="ID_1063117018" MODIFIED="1407717711244" TEXT="An&#xe1;lise de algor&#xed;timos">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360259" ID="ID_7527378221" MODIFIED="1407717711245" TEXT="Algor&#xed;timos e estrutura de dados">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360260" ID="ID_5108816423" MODIFIED="1407717711246" TEXT="Arquitetura e organiza&#xe7;&#xe3;o de computadores">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360260" ID="ID_3312544541" MODIFIED="1407717711247" TEXT="Circuito digitais">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360260" ID="ID_4333851076" MODIFIED="1407717711248" TEXT="Linguagem de programa&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360260" ID="ID_4207210781" MODIFIED="1407717711249" TEXT="Linguagens formais aut&#xf4;matos e computabilidade">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360260" ID="ID_0512301041" MODIFIED="1407717711250" TEXT="Organiza&#xe7;&#xe3;o de arquivos e dados">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360260" ID="ID_0426748708" MODIFIED="1407717711250" TEXT="Sistemas operacionais">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360260" ID="ID_6373108267" MODIFIED="1407717711251" TEXT="T&#xe9;cnicas de programa&#xe7;&#xe3;o">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360261" ID="ID_7321417362" MODIFIED="1407717711253" TEXT="Teoria dos grafos">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360261" FOLDED="true" ID="ID_5483751461" MODIFIED="1407717711257" TEXT="Tecnologia da computa&#xe7;&#xe3;o">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717360261" ID="ID_8745345880" MODIFIED="1407717711258" TEXT="Banco de dados">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360261" ID="ID_4268705743" MODIFIED="1407717711259" TEXT="Compiladores">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360261" ID="ID_1730315011" MODIFIED="1407717711260" TEXT="Computa&#xe7;&#xe3;o gr&#xe1;fica">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360262" ID="ID_2887565561" MODIFIED="1407717711261" TEXT="Engenharia de software">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360262" ID="ID_0647121716" MODIFIED="1407717711262" TEXT="Intelig&#xea;ncia artificial">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360262" ID="ID_1274410376" MODIFIED="1407717711263" TEXT="Processamento de imagens">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360262" ID="ID_8723347853" MODIFIED="1407717711264" TEXT="Redes de computadores">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717360262" ID="ID_4102024536" MODIFIED="1407717711265" TEXT="Sistemas distribu&#xed;dos">
<edge COLOR="#309eff"/>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1407717360259" FOLDED="true" ID="ID_554253855" MODIFIED="1411175347416" POSITION="right" TEXT="Fundamentos de computa&#xe7;&#xe3;o">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<icon BUILTIN="messagebox_warning"/>
<node COLOR="#00b439" CREATED="1407717360259" ID="ID_170090151" MODIFIED="1411175229990" TEXT="An&#xe1;lise de algor&#xed;timos">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1407717493000" ID="ID_1663831902" MODIFIED="1407717711297" TEXT="Medidas de Complexidade, An&#xe1;lise Assint&#xf3;tica de Limites de Complexidade, T&#xe9;cnicas de Prova de  Cotas Inferiores. Nota&#xe7;&#xe3;o &#x201c;Big O&#x201d;, &#x201c;Little o&#x201d;, &#x201c;Omega&#x201d; e &#x201c;Theta&#x201d;. Medidas Emp&#xed;ricas de Performance.  O  Uso  de  Rela&#xe7;&#xf5;es  de  Recorr&#xea;ncia  para  An&#xe1;lise  de  Algoritmos  Recursivos.  An&#xe1;lise  de  Algoritmos  Iterativos e Recursivos.">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360259" ID="ID_1047658653" MODIFIED="1411175235103" TEXT="Algor&#xed;timos e estrutura de dados">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1407717502412" ID="ID_1941057110" MODIFIED="1407717711300" TEXT="Metodologia de Desenvolvimento de Algoritmos. Tiposde Dados B&#xe1;sicos e Estruturados. Comandos  de  uma  Linguagem  de  Programa&#xe7;&#xe3;o.  Recursividade:  Conceito  e  Implementa&#xe7;&#xe3;o.  Modularidade  e  Abstra&#xe7;&#xe3;o.  Estrat&#xe9;gias de  Depura&#xe7;&#xe3;o. Cadeias e  Processamento de Cadeias.  Estruturas de Dados  Lineares  e  suas  Generaliza&#xe7;&#xf5;es:  Listas  Ordenadas,  Listas  Encadeadas,  Pilhas  e  Filas.  &#xc1;rvores  e  suas  Generaliza&#xe7;&#xf5;es:  &#xc1;rvores  Bin&#xe1;rias,  &#xc1;rvores  de  Busca  e  &#xc1;rvores  Balanceadas.  Tabelas  Hash.  Algoritmos para Pesquisa e Ordena&#xe7;&#xe3;o. Algoritmos para &#x201c;Garbage Collection&#x201d;. T&#xe9;cnicas de Projeto de  Algoritmos:  M&#xe9;todo  da  For&#xe7;a  Bruta,  Pesquisa  Exaustiva,  Algoritmo  Guloso,  Dividir  e  Conquistar,  &#x201c;Backtracking&#x201d; e Heur&#xed;sticas. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360260" ID="ID_1001915331" MODIFIED="1407717711302" TEXT="Arquitetura e organiza&#xe7;&#xe3;o de computadores">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717511734" ID="ID_586516325" MODIFIED="1407717711303" TEXT="Organiza&#xe7;&#xe3;o de Computadores: Mem&#xf3;rias, Unidades Centrais de Processamento, Entrada e Sa&#xed;da.  Linguagens  de  Montagem.  Modos  de  Endere&#xe7;amento,  Conjunto  de  Instru&#xe7;&#xf5;es.  Mecanismos  de  Interrup&#xe7;&#xe3;o  e  de  Exce&#xe7;&#xe3;o.  Barramento,  Comunica&#xe7;&#xf5;es, Interfaces  e  Perif&#xe9;ricos.  Organiza&#xe7;&#xe3;o  de  Mem&#xf3;ria. Mem&#xf3;ria Auxiliar. Arquiteturas RISC e CISC. Pipeline. Paralelismo de Baixa Granularidade.  Processadores Superescalares e Superpipeline. Multiprocessadores. Multicomputadores. Arquiteturas  Paralelas e n&#xe3;o Convencionais. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360260" ID="ID_1977384590" MODIFIED="1411175249564" TEXT="Circuito digitais">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1407717521192" ID="ID_1630384022" MODIFIED="1407717711305" TEXT="Sistemas  de  Numera&#xe7;&#xe3;o  e  C&#xf3;digos.  Aritm&#xe9;tica  Bin&#xe1;ria.  Representa&#xe7;&#xe3;o  e  Manipula&#xe7;&#xe3;o  de  Circuitos  Combinat&#xf3;rios.  Minimiza&#xe7;&#xe3;o  e  Otimiza&#xe7;&#xe3;o  de  Fun&#xe7;&#xf5;es  Combinat&#xf3;rias.  Projeto  de  Circuitos  Combinat&#xf3;rios. An&#xe1;lise e S&#xed;ntese de Componentes Sequenciais e de Mem&#xf3;ria. Projeto de Circuitos  Sequenciais.  Modelo  de  M&#xe1;quinas  de  Estado  Finito  (FSM).  Circuitos  Sequenciais  S&#xed;ncronos  e  Ass&#xed;ncronos. Componentes de Armazenamento. Projeto de Sistemas Digitais: Hier&#xe1;rquico e Modular.  Princ&#xed;pios e T&#xe9;cnicas de Projeto. Conceitos de Controle e de Tempo. Fam&#xed;lias L&#xf3;gicas. Dispositivos  L&#xf3;gicos Program&#xe1;veis (PLD). ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360260" ID="ID_817008423" MODIFIED="1407717711307" TEXT="Linguagem de programa&#xe7;&#xe3;o">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717531813" ID="ID_1090601596" MODIFIED="1407717711308" TEXT="Conceitos.  Paradigmas  de  Linguagens  de  Programa&#xe7;&#xe3;o. Sem&#xe2;ntica  Formal.  Teoria  dos  Tipos:  Sistemas de Tipos, Polimorfismo. Verifica&#xe7;&#xe3;o e Infer&#xea;ncia de Tipos.">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360260" ID="ID_992084345" MODIFIED="1407717711309" TEXT="Linguagens formais aut&#xf4;matos e computabilidade">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717540707" ID="ID_1380698233" MODIFIED="1407717711310" TEXT="Gram&#xe1;ticas.  Linguagens  Regulares,  Livres-de-Contexto  e  Sens&#xed;veis-ao-Contexto.  Tipos  de  Reconhecedores. Opera&#xe7;&#xf5;es com Linguagens. Propriedades das Linguagens. Aut&#xf4;matos de Estados  Finitos Determin&#xed;stico e  n&#xe3;o Determin&#xed;stico. Aut&#xf4;matos de  Pilha.  M&#xe1;quina  de  Turing. Hierarquia de  Chomsky. Fun&#xe7;&#xf5;es Recursivas. Tese de Church. Problemas Indecid&#xed;veis. Teorema da Incompletude  de  Godel.  Classes  de  Problemas  P,  NP,  NP  Completo  e NP-Dif&#xed;cil.  M&#xe9;todos  de  Redu&#xe7;&#xe3;o  de  Problemas. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360260" ID="ID_906533031" MODIFIED="1407717711312" TEXT="Organiza&#xe7;&#xe3;o de arquivos e dados">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717555041" ID="ID_1200818505" MODIFIED="1407717711313" TEXT="Organiza&#xe7;&#xe3;o,  Estrutura  e  Opera&#xe7;&#xe3;o  de  Arquivos.  Diret&#xf3;rios:  Conte&#xfa;do  e  Estrutura.  Arquivos  do  Sistema e Sistema de Arquivos Virtuais. T&#xe9;cnicas dePesquisa. Dados e Metadados. Representa&#xe7;&#xe3;o  Digital  e  Anal&#xf3;gica.  Algoritmos  de  Codifica&#xe7;&#xe3;o  e  Decodifica&#xe7;&#xe3;o.  Compress&#xe3;o  de  Dados,  &#xc1;udio,  Imagem e V&#xed;deo. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360260" ID="ID_846401389" MODIFIED="1411175312707" TEXT="Sistemas operacionais">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1407717563970" ID="ID_1355873720" MODIFIED="1407717711315" TEXT="Conceito  de  Processo.  Ger&#xea;ncia  de  Processos/Processador.  Comunica&#xe7;&#xe3;o,  Concorr&#xea;ncia  e  Sincroniza&#xe7;&#xe3;o de Processos. Gerenciamento de Mem&#xf3;ria: Mem&#xf3;ria Virtual, Pagina&#xe7;&#xe3;o, Segmenta&#xe7;&#xe3;o  e &#x201c;Swap&#x201d;. Gerenciamento  de Arquivos. Gerenciamento  de Dispositivos de Entrada/Sa&#xed;da.  Aloca&#xe7;&#xe3;o  de Recursos.">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360260" ID="ID_1449047943" MODIFIED="1407717711317" TEXT="T&#xe9;cnicas de programa&#xe7;&#xe3;o">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717575257" ID="ID_497090676" MODIFIED="1407717711318" TEXT="Desenvolvimento  de  algoritmos.  Tipos  de  dados  b&#xe1;sicos  e  estruturados.  Comandos  de  uma  Linguagem  de  programa&#xe7;&#xe3;o.  Metodologia  de  desenvolvimento  de  programas.  Modularidade  e  abstra&#xe7;&#xe3;o. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360261" ID="ID_421821625" MODIFIED="1411175328425" TEXT="Teoria dos grafos">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1407717583977" ID="ID_299284953" MODIFIED="1407717711320" TEXT="Grafos  orientados  e  n&#xe3;o-orientados.  Caminhos.  Planaridade.  Conectividade.  Colora&#xe7;&#xe3;o.  Grafos  Infinitos. Algoritmos em grafos. Problemas intrat&#xe1;veis. Busca em Largura e Profundidade. Algoritmos  do Menor Caminho. &#xc1;rvore Geradora. Ordena&#xe7;&#xe3;o Topol&#xf3;gica.">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1407717360258" ID="ID_1240633655" MODIFIED="1409866189872" POSITION="left" TEXT="Matem&#xe1;tica">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1407717360258" ID="ID_145232080" MODIFIED="1411475474427" TEXT="&#xc1;lgebra LINEAR">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717402113" ID="ID_358417471" MODIFIED="1411174804058">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Sistemas de Equa&#231;&#245;es Lineares
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#111111" CREATED="1407717947739" ID="ID_1924778183" MODIFIED="1407717960477">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      m&#233;todo de elimina&#231;&#227;o de Gauss para sistemas lineares.&#160;
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1407717800030" FOLDED="true" ID="ID_1783765589" MODIFIED="1411475512085" TEXT="Espa&#xe7;os  vetoriais.  ">
<font NAME="SansSerif" SIZE="14"/>
<node COLOR="#111111" CREATED="1407717819408" ID="ID_89167539" MODIFIED="1407717927970" TEXT="Subespa&#xe7;os.  ">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1407717825563" ID="ID_1516158305" MODIFIED="1407717927973" TEXT="Bases.  ">
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1407717829394" ID="ID_1235168129" MODIFIED="1407717927975" TEXT="Somas  Diretas.  ">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1407717835108" ID="ID_1472091479" MODIFIED="1411174816158" TEXT="Introdu&#xe7;&#xe3;o  &#xe0;  Programa&#xe7;&#xe3;o  Linear.  ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#111111" CREATED="1407717843748" ID="ID_355558187" MODIFIED="1407717971546" TEXT="Transforma&#xe7;&#xf5;es  Lineares  e  Matrizes. ">
<font NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#990000" CREATED="1407717850246" ID="ID_1535128127" MODIFIED="1407717855317" TEXT=" Autovalores  e  Autovetores.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717855318" ID="ID_1030493028" MODIFIED="1407717860677" TEXT="Diagonaliza&#xe7;&#xe3;o.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717860678" ID="ID_1360258955" MODIFIED="1407717865104" TEXT="Espa&#xe7;os  com  Produto  Interno.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717865105" ID="ID_1677352543" MODIFIED="1407717869143" TEXT="Bases  Ortonormais.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717869145" ID="ID_803556018" MODIFIED="1407717875496" TEXT="Proje&#xe7;&#xf5;es  Ortogonais.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717875497" ID="ID_1263553280" MODIFIED="1407717879921" TEXT="Movimentos R&#xed;gidos.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717879923" ID="ID_473638583" MODIFIED="1407717885255" TEXT="M&#xe9;todo  dos  M&#xed;nimos  Quadrados.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717885256" ID="ID_1529564605" MODIFIED="1407717893078" TEXT="Transforma&#xe7;&#xf5;es  em  Espa&#xe7;os  com  Produto  Interno. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717893079" ID="ID_1601709354" MODIFIED="1411174830735" TEXT=" O  Teorema  da  Representa&#xe7;&#xe3;o  para  Fun&#xe7;&#xf5;es  Lineares.  ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407717899736" ID="ID_428949341" MODIFIED="1407717904880" TEXT="Adjunta  de  uma  Transforma&#xe7;&#xe3;o  Linear.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717904881" ID="ID_254449940" MODIFIED="1407717911184" TEXT="Operadores  Sim&#xe9;tricos,  Unit&#xe1;rios,  Ortogonais  e  Normais.">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407717911185" ID="ID_413466661" MODIFIED="1407717911187" TEXT=" O Teorema Espectral. Formas Can&#xf4;nicas. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360258" FOLDED="true" ID="ID_1537903924" MODIFIED="1411174881226" TEXT="An&#xe1;lise combinat&#xf3;ria">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717415325" ID="ID_945784697" MODIFIED="1411174864248" TEXT="Distribui&#xe7;&#xe3;o.  ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718025466" ID="ID_1829452332" MODIFIED="1411174864253" TEXT="Permuta&#xe7;&#xf5;es.  ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718028649" ID="ID_1729185074" MODIFIED="1411174864252" TEXT="Combina&#xe7;&#xf5;es.  ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718032122" ID="ID_542332585" MODIFIED="1407718038048" TEXT="Fun&#xe7;&#xf5;es  Geradoras  Ordin&#xe1;rias  e  Exponenciais.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718038050" ID="ID_1375156235" MODIFIED="1407718042338" TEXT="Princ&#xed;pio  de  Inclus&#xe3;o  e  Exclus&#xe3;o.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718042339" ID="ID_38211624" MODIFIED="1411174872247" TEXT="Enumera&#xe7;&#xe3;o  de  Parti&#xe7;&#xf5;es,  Grafos,  &#xc1;rvores  e  Redes.  ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718046907" ID="ID_1872100781" MODIFIED="1411174876378">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Enumera&#231;&#227;o&#160;&#160;por&#160;&#160;Recurs&#227;o.
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718053949" ID="ID_195141317" MODIFIED="1407718053958">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Permuta&#231;&#245;es com Posi&#231;&#245;es Restritas.&#160;
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360258" FOLDED="true" ID="ID_376467463" MODIFIED="1411174972296" TEXT="C&#xe1;lculo diferencial ou integral">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Ver com a Priscila
    </p>
  </body>
</html></richcontent>
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="help"/>
<node COLOR="#990000" CREATED="1407717426521" ID="ID_378972193" MODIFIED="1411174889238" TEXT="Limites  de  Fun&#xe7;&#xf5;es  e  de  Sequ&#xea;ncias.  ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718062461" ID="ID_83149857" MODIFIED="1407718069986" TEXT="Fun&#xe7;&#xf5;es  Reais  de  uma  Vari&#xe1;vel:  Continuidade  e  Diferenciabilidade. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718069987" ID="ID_175707032" MODIFIED="1411174894483" TEXT="M&#xe1;ximos e M&#xed;nimos. ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718077416" ID="ID_354250383" MODIFIED="1407718082591" TEXT="F&#xf3;rmula de  Taylor e Aproxima&#xe7;&#xe3;o de  Fun&#xe7;&#xf5;es. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718082592" ID="ID_474291628" MODIFIED="1407718088351" TEXT="M&#xe9;todo de  Newton para o C&#xe1;lculo de Ra&#xed;zes e de M&#xe1;ximos e M&#xed;nimos.">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718088353" ID="ID_938412740" MODIFIED="1407718096561" TEXT=" Integra&#xe7;&#xe3;o de Fun&#xe7;&#xf5;es Reais de uma  Vari&#xe1;vel.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718096562" ID="ID_818244849" MODIFIED="1407718101069" TEXT="M&#xe9;todos  de  Integra&#xe7;&#xe3;o.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718101071" ID="ID_1004365378" MODIFIED="1407718104754" TEXT="Integra&#xe7;&#xe3;o  Aproximada.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718104755" ID="ID_897302205" MODIFIED="1407718111475" TEXT="Regras  dos  Trap&#xe9;zios,  de  Simpson  e  Generalizadas. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718111476" ID="ID_1495115982" MODIFIED="1407718116667" TEXT="Fun&#xe7;&#xf5;es de V&#xe1;rias Vari&#xe1;veis: Continuidade e Diferenciabilidade. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718116669" ID="ID_1518738327" MODIFIED="1407718124084" TEXT="Gradiente. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718124085" ID="ID_1899066416" MODIFIED="1407718127650" TEXT="M&#xe1;ximos  e M&#xed;nimos. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718127651" ID="ID_629798676" MODIFIED="1407718131986" TEXT="Multiplicadores de Lagrange. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718131987" ID="ID_1683639134" MODIFIED="1407718135877" TEXT="Transforma&#xe7;&#xf5;es. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718135878" ID="ID_898695364" MODIFIED="1407718139461" TEXT="Matrizes Jacobianas. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718139463" ID="ID_1905039097" MODIFIED="1407718143246" TEXT="Teorema da Fun&#xe7;&#xe3;o  Inversa.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718143248" ID="ID_966922301" MODIFIED="1407718148231" TEXT="Diferencia&#xe7;&#xe3;o  Impl&#xed;cita.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718148233" ID="ID_1640982948" MODIFIED="1407718153488" TEXT="Integra&#xe7;&#xe3;o  de  Fun&#xe7;&#xf5;es  de  V&#xe1;rias  Vari&#xe1;veis.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718153489" ID="ID_364934786" MODIFIED="1407718161777">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Mudan&#231;as&#160;&#160;de&#160;&#160;Coordenadas em Integrais.
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718161782" ID="ID_623203408" MODIFIED="1407718161790">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Integral de Linha.
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360258" FOLDED="true" ID="ID_685784290" MODIFIED="1411174993016" TEXT="Geometria anal&#xed;tica">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717437094" ID="ID_788589882" MODIFIED="1411174990151" TEXT="Matrizes.  ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718170159" ID="ID_1030165714" MODIFIED="1411174990157" TEXT="Sistemas  de  Equa&#xe7;&#xf5;es  Lineares.  ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718174584" ID="ID_867513458" MODIFIED="1411174990157" TEXT="Vetores.  ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718178505" ID="ID_967406440" MODIFIED="1411174990156" TEXT="Produtos:  escalar,  vetorial  e  misto.  ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718182477" ID="ID_638766980" MODIFIED="1411174990156" TEXT="&#xc1;lgebra  Vetorial. ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718186558" ID="ID_1392469743" MODIFIED="1411174990155" TEXT="Reta no plano e no espa&#xe7;o. ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718189886" ID="ID_1375353668" MODIFIED="1411174990154" TEXT="Planos. ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718194166" ID="ID_498205358" MODIFIED="1411174990154" TEXT="Posi&#xe7;&#xf5;es Relativas, Interse&#xe7;&#xf5;es, Dist&#xe2;ncias e &#xc2;ngulos. ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718207240" ID="ID_772432044" MODIFIED="1411174990153">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      &#160;C&#237;rculo e Esfera.
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718214959" ID="ID_997356204" MODIFIED="1411174990152">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Coordenadas Polares, Cil&#237;ndricas e Esf&#233;ricas.
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360258" FOLDED="true" ID="ID_1617373904" MODIFIED="1411175035391" TEXT="L&#xf3;gica matem&#xe1;tica">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717444642" ID="ID_1918101597" MODIFIED="1411175025557" TEXT="L&#xf3;gica  Proposicional  e  de  Predicados.  ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718223694" ID="ID_1505270025" MODIFIED="1407718228854" TEXT="Linguagem  Proposicional  e  de  Primeira  Ordem.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718228856" ID="ID_232014993" MODIFIED="1407718233254" TEXT="Sistemas  Dedutivos. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718233255" ID="ID_746287257" MODIFIED="1411175013206" TEXT="Tabelas Verdade e Estruturas de PrimeiraOrdem. ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718237008" ID="ID_400299002" MODIFIED="1407718241734" TEXT="Rela&#xe7;&#xf5;es de Consequ&#xea;ncia. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718241735" ID="ID_1482976445" MODIFIED="1407718247196" TEXT="Corretude.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718247197" ID="ID_813534871" MODIFIED="1407718250996" TEXT="Completude.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718250996" ID="ID_1465275782" MODIFIED="1407718254180" TEXT="Compacidade.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718254181" ID="ID_635588230" MODIFIED="1407718264173" TEXT="Lowemhein-Skolem.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718264174" ID="ID_1157566992" MODIFIED="1407718270448">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Decidibilidade.&#160;&#160;
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718270453" ID="ID_1545531518" MODIFIED="1407718276063">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Prova&#160;&#160;Autom&#225;tica&#160;&#160;de&#160;&#160;Teoremas.&#160;&#160;
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718276068" ID="ID_1135890328" MODIFIED="1407718276076">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      L&#243;gicas n&#227;o cl&#225;ssicas.&#160;
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360259" FOLDED="true" ID="ID_39667056" MODIFIED="1411175084878" TEXT="Matem&#xe1;tica discreta">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717454664" ID="ID_647740450" MODIFIED="1411175042497" TEXT="Itera&#xe7;&#xe3;o, Indu&#xe7;&#xe3;o e Recurs&#xe3;o. ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718624389" ID="ID_1654970239" MODIFIED="1407718628596" TEXT="Conjuntos e &#xc1;lgebra de Conjuntos como uma Teoria Axiom&#xe1;tica. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718628597" ID="ID_1353782530" MODIFIED="1407718633452" TEXT="Par  Ordenado.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718633454" ID="ID_1468942550" MODIFIED="1407718636677" TEXT="Fun&#xe7;&#xf5;es.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718636678" ID="ID_440432396" MODIFIED="1407718647460" TEXT="Fun&#xe7;&#xf5;es  e  Formas  Booleanas,  &#xc1;lgebra  Booleana,  Minimiza&#xe7;&#xe3;o  de  Fun&#xe7;&#xf5;es  Booleanas.">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718647462" ID="ID_471611765" MODIFIED="1411175056554" TEXT=" Rela&#xe7;&#xf5;es sobre Conjuntos, Rela&#xe7;&#xf5;es de Equival&#xea;ncia e Ordem.">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718652905" ID="ID_1457620247" MODIFIED="1407718658303" TEXT=" Reticulados, Mon&#xf3;ides,  Grupos,  An&#xe9;is.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718658304" ID="ID_238211240" MODIFIED="1411175069952" TEXT="Teoria  dos  C&#xf3;digos,  Canal  Bin&#xe1;rio,  Canal  Sim&#xe9;trico,  C&#xf3;digo  de  Blocos,  Matrizes  Geradoras e Verificadoras, C&#xf3;digos de Grupo, C&#xf3;digos de Hamming. ">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
<node COLOR="#990000" CREATED="1407718672717" ID="ID_529798237" MODIFIED="1411175081096" TEXT="Teoria dos Dom&#xed;nios: Ordens  Parciais Completas, Continuidade, Ponto Fixo, Dom&#xed;nios, Espa&#xe7;o das Fun&#xe7;&#xf5;es.">
<font NAME="SansSerif" SIZE="14"/>
<icon BUILTIN="button_ok"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360259" FOLDED="true" ID="ID_1062335300" MODIFIED="1411175128755" TEXT="Probabilidade e estat&#xed;stica">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717464569" ID="ID_77123848" MODIFIED="1407718687267" TEXT="Eventos. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718687268" ID="ID_983368248" MODIFIED="1407718690563" TEXT="Experimentos Aleat&#xf3;rios. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718690564" ID="ID_1332310100" MODIFIED="1407718694235" TEXT="An&#xe1;lise Explorat&#xf3;ria de Dados. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718694237" ID="ID_1556394715" MODIFIED="1407718697875" TEXT="Descri&#xe7;&#xe3;o Estat&#xed;stica dos Dados.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718697876" ID="ID_467803057" MODIFIED="1407718701468" TEXT="Espa&#xe7;os Amostrais.">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718701469" ID="ID_1672117542" MODIFIED="1407718705165" TEXT=" Probabilidades em Espa&#xe7;os Amostrais Discretos. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718705166" ID="ID_1908428665" MODIFIED="1407718711295" TEXT="Distribui&#xe7;&#xf5;es de Probabilidades  de  Vari&#xe1;veis  Aleat&#xf3;rias  Unidimensionais  e  Bidimensionais.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718711300" ID="ID_1075397721" MODIFIED="1407718715150" TEXT="Esperan&#xe7;a  Matem&#xe1;tica.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718715151" ID="ID_187929780" MODIFIED="1407718718614" TEXT="Vari&#xe2;ncia  e  Coeficientes  de  Correla&#xe7;&#xe3;o.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718718615" ID="ID_189875611" MODIFIED="1407718722303" TEXT="Aproxima&#xe7;&#xe3;o  Normal.  ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718722304" ID="ID_1520795702" MODIFIED="1407718727704" TEXT="Estima&#xe7;&#xe3;o  Pontual  e  por  Intervalo. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718727705" ID="ID_1558525651" MODIFIED="1407718731016" TEXT=" Teste  de  Hip&#xf3;teses para M&#xe9;dias. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718731017" ID="ID_1463001087" MODIFIED="1407718738427">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Testes do Qui-Quadrado.
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718738432" ID="ID_1575048331" MODIFIED="1407718744194">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Testes de Compara&#231;&#245;es de M&#233;dias.
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
<node COLOR="#990000" CREATED="1407718744199" ID="ID_578897782" MODIFIED="1407718744206">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Regress&#227;o e&#160;&#160;Correla&#231;&#227;o.&#160;
    </p>
  </body>
</html></richcontent>
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
<node COLOR="#0033ff" CREATED="1407717360261" ID="ID_255182159" MODIFIED="1411175669168" POSITION="left" TEXT="Tecnologia da computa&#xe7;&#xe3;o">
<edge COLOR="#309eff" STYLE="sharp_bezier" WIDTH="8"/>
<font NAME="SansSerif" SIZE="18"/>
<node COLOR="#00b439" CREATED="1407717360261" ID="ID_1614237547" MODIFIED="1411175723713" TEXT="Banco de dados">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<icon BUILTIN="yes"/>
<node COLOR="#990000" CREATED="1407717618592" ID="ID_1310615585" MODIFIED="1407717711326" TEXT="Modelo de Dados. Modelagem e Projeto de Banco de Dados. Sistemas de Gerenciamento de Bancos  de  Dados  (SGBD):  Arquitetura,  Seguran&#xe7;a,  Integridade,  Concorr&#xea;ncia,  Recupera&#xe7;&#xe3;o  ap&#xf3;s  Falha,  Gerenciamento de Transa&#xe7;&#xf5;es. Linguagens de Consulta. Bancos de Dados Distribu&#xed;dos. Minera&#xe7;&#xe3;o  de Dados. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360261" ID="ID_1252078728" MODIFIED="1407717711328" TEXT="Compiladores">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717626722" ID="ID_1279523619" MODIFIED="1407717711329" TEXT="Compiladores  e  Interpretadores.  An&#xe1;lise  L&#xe9;xica  e  Sint&#xe1;tica.  Tabelas  de  S&#xed;mbolos.  Esquemas  de  Tradu&#xe7;&#xe3;o.  Ambientes  de  Tempo  de  Execu&#xe7;&#xe3;o.  Representa&#xe7;&#xe3;o  Intermedi&#xe1;ria.  An&#xe1;lise  Sem&#xe2;ntica.  Gera&#xe7;&#xe3;o de C&#xf3;digo. Otimiza&#xe7;&#xe3;o de C&#xf3;digo. Bibliotecas e Compila&#xe7;&#xe3;o em Separado. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360261" ID="ID_274680699" MODIFIED="1411175728827" TEXT="Computa&#xe7;&#xe3;o gr&#xe1;fica">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<icon BUILTIN="yes"/>
<node COLOR="#990000" CREATED="1407717635637" ID="ID_1430774424" MODIFIED="1407717711331" TEXT="Transforma&#xe7;&#xf5;es Geom&#xe9;tricas em Duas e Tr&#xea;s Dimens&#xf5;es: Coordenadas Homog&#xea;neas e Matrizes de  Transforma&#xe7;&#xe3;o. Transforma&#xe7;&#xe3;o  entre  Sistemas  de  Coordenadas  2D  e  Recorte.  Transforma&#xe7;&#xf5;es  de  Proje&#xe7;&#xe3;o Paralela e Perspectiva. C&#xe2;mera Virtual. Transforma&#xe7;&#xe3;o entre Sistemas de Coordenadas 3D.  Defini&#xe7;&#xe3;o  de  Objetos  e  Cenas  Tridimensionais:  Modelos  Poliedrais  e  Malhas  de  Pol&#xed;gonos.  O  Processo  de  &#x201c;Rendering&#x201d;:  Fontes  de  Luz,  Remo&#xe7;&#xe3;o  de  Linhas  e  Superf&#xed;cies  Ocultas,  Modelos  de  Tonaliza&#xe7;&#xe3;o (&#x201c;Shading&#x201d;). Aplica&#xe7;&#xe3;o de Texturas. O problema do Serrilhado (&#x201c;Aliasing&#x201d;) e T&#xe9;cnicas de  Anti-Serrilhado (&#x201c;Antialiasing&#x201d;). Visualiza&#xe7;&#xe3;o. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360262" ID="ID_323883713" MODIFIED="1411175165725" TEXT="Engenharia de software">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1407717653580" ID="ID_1228978275" MODIFIED="1407717711334" TEXT="Processo  de  Desenvolvimento  de  Software.  Ciclo  de  Vida  de  Desenvolvimento  de  Software.  Qualidade de Software. T&#xe9;cnicas de Planejamento e Gerenciamento de Software. Gerenciamento de  Configura&#xe7;&#xe3;o de Software. Engenharia de Requisitos.M&#xe9;todos de An&#xe1;lise e de Projeto de Software.  Garantia  de  Qualidade  de  Software.  Verifica&#xe7;&#xe3;o,  Valida&#xe7;&#xe3;o  e  Teste.  Manuten&#xe7;&#xe3;o.  Documenta&#xe7;&#xe3;o.  Padr&#xf5;es  de  Desenvolvimento.  Reuso.  Engenharia  Reversa.  Reengenharia.  Ambientes  de  Desenvolvimento de Software. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360262" ID="ID_89607469" MODIFIED="1411175172570" TEXT="Intelig&#xea;ncia artificial">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<node COLOR="#990000" CREATED="1407717664359" ID="ID_69235865" MODIFIED="1407717711336" TEXT="Linguagens Simb&#xf3;licas. Programa&#xe7;&#xe3;o em L&#xf3;gica. Resolu&#xe7;&#xe3;o de Problemas como Busca. Estrat&#xe9;gias  de Busca, Busca Cega e Busca Heur&#xed;stica. Hill climbing, best first, simulated annealing e Algoritmo  A*.  Busca  como  Maximiza&#xe7;&#xe3;o  de  Fun&#xe7;&#xe3;o.  Grafos  And/Or.  Esquemas  para  Representa&#xe7;&#xe3;o  do  Conhecimento:  L&#xf3;gicos,  em  Rede,  Estruturados,  Procedurais.  Sistemas  de  Produ&#xe7;&#xe3;o  com  Encadeamento para a Frente e Encadeamento para tr&#xe1;s. Racioc&#xed;nio N&#xe3;o-Monot&#xf4;nico. Formalismos  para  a  Representa&#xe7;&#xe3;o  de  Conhecimento  Incerto.  A  Regra  de  Bayes.  Conjuntos  e  L&#xf3;gica  Fuzzy.  Aprendizado  de  M&#xe1;quina.  Aprendizado  Indutivo.  &#xc1;rvores  de  Decis&#xe3;o,  Redes  Neurais  e  Algoritmos  Gen&#xe9;ticos.  Sistemas  Especialistas.  Processamento  de Linguagem  Natural.  Agentes  Inteligentes.  Rob&#xf3;tica. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360262" ID="ID_1845429815" MODIFIED="1407717711338" TEXT="Processamento de imagens">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<node COLOR="#990000" CREATED="1407717678023" ID="ID_1578286237" MODIFIED="1407717711339" TEXT="Introdu&#xe7;&#xe3;o  aos  Filtros  Digitais.  M&#xe9;todos  de  Espa&#xe7;o  de  Estados.  No&#xe7;&#xf5;es  de  Percep&#xe7;&#xe3;o  Visual  Humana. Amostragem e Quantiza&#xe7;&#xe3;o de Imagens. Transformadas de Imagens. Realce. Filtragem e  Restaura&#xe7;&#xe3;o. Reconstru&#xe7;&#xe3;o Tomogr&#xe1;fica de Imagens. Codifica&#xe7;&#xe3;o. An&#xe1;lise de Imagens e No&#xe7;&#xf5;es de  Vis&#xe3;o Computacional. Reconhecimento de Padr&#xf5;es. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360262" ID="ID_1597564794" MODIFIED="1411175842146" TEXT="Redes de computadores">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<icon BUILTIN="yes"/>
<node COLOR="#990000" CREATED="1407717685661" ID="ID_956233126" MODIFIED="1407717711341" TEXT="Tipos de Enlace, C&#xf3;digos, Modos e Meios de Transmiss&#xe3;o. Protocolos e Servi&#xe7;os de Comunica&#xe7;&#xe3;o.  Terminologia, Topologias, Modelos de Arquitetura e Aplica&#xe7;&#xf5;es. Especifica&#xe7;&#xe3;o de Protocolos. Internet  e Intranets. Interconex&#xe3;o de Redes. Redes de Banda Larga. Seguran&#xe7;a e Autentica&#xe7;&#xe3;o. Avalia&#xe7;&#xe3;o de  Desempenho. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
<node COLOR="#00b439" CREATED="1407717360262" ID="ID_376493998" MODIFIED="1411175887466" TEXT="Sistemas distribu&#xed;dos">
<edge COLOR="#309eff" STYLE="bezier" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="16"/>
<icon BUILTIN="button_ok"/>
<icon BUILTIN="yes"/>
<node COLOR="#990000" CREATED="1407717697785" ID="ID_12530568" MODIFIED="1407717711344" TEXT="Problemas  B&#xe1;sicos  em  Computa&#xe7;&#xe3;o  Distribu&#xed;da:  Coordena&#xe7;&#xe3;o  e  Sincroniza&#xe7;&#xe3;o  de  Processos,  Exclus&#xe3;o  M&#xfa;tua,  Difus&#xe3;o  de  Mensagens.  Compartilhamento  de  Informa&#xe7;&#xe3;o:  Controle  de  Concorr&#xea;ncia,  Transa&#xe7;&#xf5;es  Distribu&#xed;das.  Comunica&#xe7;&#xe3;o  entre  Processos.  Toler&#xe2;ncia  a  Falhas.  Sistemas  Operacionais  Distribu&#xed;dos:  Sistemas  de  Arquivos,  Servidores  de  Nomes,  Mem&#xf3;ria  Compartilhada, Seguran&#xe7;a. ">
<font NAME="SansSerif" SIZE="14"/>
</node>
</node>
</node>
</node>
</map>
